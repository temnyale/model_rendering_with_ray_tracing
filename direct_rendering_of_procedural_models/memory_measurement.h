/**
 * Data structure that simplifies the process of memory measurement.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#ifndef MEMORY_MEASUREMENT
#define MEMORY_MEASUREMENT

#include <iostream>
#include <cuda_runtime_api.h>

namespace drpm
{
	enum class InformationUnit
	{
		BYTE,
		MEGABYTE
	};

	inline std::string information_unit_to_string(InformationUnit information_unit)
	{
		switch (information_unit)
		{
			case InformationUnit::BYTE:
			{
				return "B";
			}

			case InformationUnit::MEGABYTE:
			{
				return "MB";
			}
		}

		throw "Incorrect information unit";
	}

	inline size_t bytes_to_megabytes(size_t value_in_bytes)
	{
		return value_in_bytes / 1024.f / 1024.f;
	}

	inline size_t convert_from_bytes(size_t value_in_bytes, InformationUnit required_information_unit)
	{
		switch (required_information_unit)
		{
			case InformationUnit::MEGABYTE:
			{
				return bytes_to_megabytes(value_in_bytes);
			} 
		}

		throw "Incorrect information unit";
	}

	inline void measure_memory(size_t& free, size_t& used, size_t& total, InformationUnit information_unit)
	{
		free = 0;
		used = 0;
		total = 0;

		int num_gpus;
		cudaGetDeviceCount(&num_gpus);

		for (int gpu_id = 0; gpu_id < num_gpus; gpu_id++)
		{
			cudaSetDevice(gpu_id);

			int id;
			cudaGetDevice(&id);

			size_t _free, _total;
			cudaMemGetInfo(&_free, &_total);			

			if (information_unit != InformationUnit::BYTE)
			{
				_free = convert_from_bytes(_free, information_unit);
				_total = convert_from_bytes(_total, information_unit);
			}

			used += _total - _free;
			free += _free;
			total += _total;
		}
	}

	class GraphicsCardMemoryMeasurer
	{
	private:
		InformationUnit information_unit;

		size_t total = 0;
		size_t free_s = 0;
		size_t free_e = 0;

	public:
		GraphicsCardMemoryMeasurer(InformationUnit _information_unit = InformationUnit::BYTE) : information_unit(_information_unit)
		{

		}

		void set_information_unit(InformationUnit _information_unit)
		{
			information_unit = _information_unit;
		}

		void start()
		{
			size_t used;
			measure_memory(free_s, used, total, information_unit);
		}

		void end()
		{
			size_t used;
			measure_memory(free_e, used, total, information_unit);
		}

		size_t used_s()
		{
			return total - free_s;
		}

		size_t used_e()
		{
			return total - free_e;
		}

		size_t used()
		{
			return used_e() - used_s();
		}

		size_t free()
		{
			return total - used();
		}

		void print()
		{
			std::cout << "\n";
			std::cout << "\n";
			std::cout << "===============================================================================\n";
			std::cout << "\n";
			std::cout << "\n";
			std::cout << "Memory information:\n";
			std::cout << "      Total:    " << total << " " << information_unit_to_string(information_unit) << "\n";
			std::cout << "      Used:     " << used() << " " << information_unit_to_string(information_unit) << "\n";
			std::cout << "      Free:     " << free() << " " << information_unit_to_string(information_unit) << "\n";
			std::cout << "\n";
			std::cout << "\n";
			std::cout << "===============================================================================\n";
			std::cout << "\n";
			std::cout << "\n";
		}
	};

	class SceneMemoryMeasurer
	{
	private:
		InformationUnit information_unit;

		size_t _traditional_gas_memory = 0;
		size_t _traditional_sbt_memory = 0;

		size_t _procedural_gas_memory = 0;
		size_t _procedural_sbt_memory = 0;

	public:
		SceneMemoryMeasurer(InformationUnit _information_unit = InformationUnit::BYTE) : information_unit(_information_unit)
		{

		}

		void set_information_unit(InformationUnit _information_unit)
		{
			information_unit = _information_unit;
		}

		void set_traditional_gas_memory(size_t __gas_memory)
		{
			_traditional_gas_memory = __gas_memory;
		}

		void set_procedural_gas_memory(size_t __gas_memory)
		{
			_procedural_gas_memory = __gas_memory;
		}

		void set_traditional_sbt_memory(size_t __sbt_memory)
		{
			_traditional_sbt_memory = __sbt_memory;
		}

		void set_procedural_sbt_memory(size_t __sbt_memory)
		{
			_procedural_sbt_memory = __sbt_memory;
		}

		size_t required_memory()
		{
			size_t ret 
				= _traditional_gas_memory + _traditional_sbt_memory
				+ _procedural_gas_memory + _procedural_sbt_memory;

			if (information_unit != InformationUnit::BYTE)
				ret = convert_from_bytes(ret, information_unit);

			return ret;
		}

		size_t traditional_gas_memory()
		{
			size_t ret = _traditional_gas_memory;

			if (information_unit != InformationUnit::BYTE)
				ret = convert_from_bytes(ret, information_unit);

			return ret;
		}

		size_t procedural_gas_memory()
		{
			size_t ret = _procedural_gas_memory;

			if (information_unit != InformationUnit::BYTE)
				ret = convert_from_bytes(ret, information_unit);

			return ret;
		}

		size_t gas_memory()
		{
			return traditional_gas_memory() + procedural_gas_memory();
		}

		size_t traditional_sbt_memory()
		{
			size_t ret = _traditional_sbt_memory;

			if (information_unit != InformationUnit::BYTE)
				ret = convert_from_bytes(ret, information_unit);

			return ret;
		}

		size_t procedural_sbt_memory()
		{
			size_t ret = _procedural_sbt_memory;

			if (information_unit != InformationUnit::BYTE)
				ret = convert_from_bytes(ret, information_unit);

			return ret;
		}

		size_t sbt_memory()
		{
			return traditional_sbt_memory() + procedural_sbt_memory();
		}

		void print()
		{
			std::cout << "\n";
			std::cout << "\n";
			std::cout << "===============================================================================\n";
			std::cout << "\n";
			std::cout << "\n";
			std::cout << "Scene memory information:\n";
			std::cout << "      GAS:                   " << gas_memory() << " " << information_unit_to_string(information_unit) << "\n";
			std::cout << "      Procedural GAS:        " << procedural_gas_memory() << " " << information_unit_to_string(information_unit) << "\n";
			std::cout << "      Traditional GAS:       " << traditional_gas_memory() << " " << information_unit_to_string(information_unit) << "\n";
			std::cout << "      SBT:                   " << sbt_memory() << " " << information_unit_to_string(information_unit) << "\n";
			std::cout << "      Procedural SBT:        " << procedural_sbt_memory() << " " << information_unit_to_string(information_unit) << "\n";
			std::cout << "      Traditional SBT:       " << traditional_sbt_memory() << " " << information_unit_to_string(information_unit) << "\n";
			std::cout << "      Total:                 " << required_memory() << " " << information_unit_to_string(information_unit) << "\n";
			std::cout << "\n";
			std::cout << "\n";
			std::cout << "===============================================================================\n";
			std::cout << "\n";
			std::cout << "\n";
		}
	};
}

#endif // !MEMORY_MEASUREMENT
