/**
 * Data structure to create textures.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <vector>
#include <vector_types.h>

namespace drpm
{
	class TextureCreator
	{
	public:
		static void create_black_texture(std::vector<float4>& buffer, unsigned int width, unsigned int height);
		static void create_white_texture(std::vector<float4>& buffer, unsigned int width, unsigned int height);
		static void create_pseudo_night_sky_texture(std::vector<float4>& buffer, unsigned int width, unsigned int height, float star_density, int seed = 0);
		static void create_black_white_texture(std::vector<float4>& buffer, unsigned int width, unsigned int height, float white_density, int seed = 0);
		// Converts from an array of float4s to an array of floats.
		static void float4_to_float(std::vector<float>& new_texture_buffer, std::vector<float4> texture_buffer, unsigned int width, unsigned int height);
	};
}
