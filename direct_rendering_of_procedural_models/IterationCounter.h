/**
 * Data structure that simplifies the process of counting.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <iostream>

namespace drpm
{
	class IterationCounter
	{
	private:
		bool _running;
		int _total_number_of_iterations;
		int _current_iteration;

	public:
		IterationCounter() : _running(false), _total_number_of_iterations(1), _current_iteration(0)
		{ }

		void set_total_number_of_iterations(int total_number_of_iterations)
		{
			_total_number_of_iterations = total_number_of_iterations;
		}

		int get_total_number_of_iterations()
		{
			return _total_number_of_iterations;
		}

		void start()
		{
			_current_iteration = 0;

			_running = true;
		}

		bool iterate()
		{
			if (_current_iteration + 1 > _total_number_of_iterations)
			{
				_running = false;
				return true;
			}

			_current_iteration++;

			return false;
		}

		bool is_running()
		{
			return _running;
		}

		void print()
		{
			std::cout << "Iteration no. " << _current_iteration << " of " << _total_number_of_iterations << "\n";
		}

		int number_of_iterations_to_end()
		{
			return _total_number_of_iterations - _current_iteration;
		}
	};
}