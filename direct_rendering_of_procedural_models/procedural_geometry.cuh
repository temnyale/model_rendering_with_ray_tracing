/**
 * Functions to create the geometry on demand.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <optix.h>
#include <optix_device.h>
#include <cuda/helpers.h>

#include "rendering.h"
#include "math_utils.h"
#include "common_geometry.h"

namespace drpm
{
	extern "C" __constant__ Parameters OPTIX_PARAMETERS_VAR;

	__forceinline__ __device__ void aabb_to_max_min_aabbs(const OptixAabb aabb, float3& aabb_min, float3& aabb_max)
	{
		aabb_min = make_float3(aabb.minX, aabb.minY, aabb.minZ);
		aabb_max = make_float3(aabb.maxX, aabb.maxY, aabb.maxZ);
	}

	__forceinline__ __device__ void aabb_to_points(
		const OptixAabb aabb,
		float3& a, float3& b, float3& c, float3& d, float3& e, float3& f, float3& g, float3& h
	)
	{
		a = make_float3(aabb.minX, aabb.minY, aabb.minZ);
		b = make_float3(aabb.maxX, aabb.minY, aabb.minZ);
		c = make_float3(aabb.maxX, aabb.minY, aabb.maxZ);
		d = make_float3(aabb.minX, aabb.minY, aabb.maxZ);
		e = make_float3(aabb.minX, aabb.maxY, aabb.minZ);
		f = make_float3(aabb.maxX, aabb.maxY, aabb.minZ);
		g = make_float3(aabb.maxX, aabb.maxY, aabb.maxZ);
		h = make_float3(aabb.minX, aabb.maxY, aabb.maxZ);
	}

	__forceinline__ __device__ void prepare_data_for_intersection_report(
		const float3& ray_origin, const float3& ray_dir,
		const Quadrangle& wall, const float& t, const Material* material,
		float3& intersection_point, float& u, float& v, float3& wall_normal
	)
	{
		intersection_point = ray_origin + ray_dir * t;

		if (material != nullptr && material->has_texture)
			wall_UV_map(u, v, wall, intersection_point, material->texture.t_h, material->texture.t_v);
		else
			wall_UV_map(u, v, wall, intersection_point);

		wall_normal = wall.get_normal();
	}

	__forceinline__ __device__ void report_intersection(
		float& t, float3& normal, float& u, float& v, const SceneObjectPart object_part
	)
	{
		int object_part_as_int = static_cast<int>(object_part);
		optixReportIntersection(
			t,
			0,
			float3_as_args(normal),
			reinterpret_cast<unsigned int&>(u),
			reinterpret_cast<unsigned int&>(v),
			reinterpret_cast<unsigned int&>(object_part_as_int)
		);
	}

	__forceinline__ __device__ void report_intersection_with_id(
		float& t, float3& normal, float& u, float& v, const SceneObjectPart object_part, const int window_id
	)
	{
		int object_part_as_int = static_cast<int>(object_part);
		int window_id_ = window_id;
		optixReportIntersection(
			t,
			0,
			float3_as_args(normal),
			reinterpret_cast<unsigned int&>(u),
			reinterpret_cast<unsigned int&>(v),
			reinterpret_cast<unsigned int&>(object_part_as_int),
			reinterpret_cast<unsigned int&>(window_id_)
		);
	}

	__forceinline__ __device__ void split_wall_u(
		const Quadrangle& wall_to_split,
		Quadrangle& left_part, float left_part_width,
		Quadrangle& central_part,
		Quadrangle& right_part, float right_part_width)
	{
		float3 horizontal_dir = wall_to_split.get_horizontal_direction();

		central_part.a = wall_to_split.a + horizontal_dir * left_part_width;
		central_part.b = wall_to_split.b - horizontal_dir * right_part_width;
		central_part.c = wall_to_split.c - horizontal_dir * right_part_width;
		central_part.d = wall_to_split.d + horizontal_dir * left_part_width;

		left_part = wall_to_split;
		left_part.b = central_part.a;
		left_part.c = central_part.d;

		right_part = wall_to_split;
		right_part.a = central_part.b;
		right_part.d = central_part.c;
	}

	__forceinline__ __device__ void split_wall_u(
		const Quadrangle& wall_to_split,
		Quadrangle& left_part, float left_part_width,
		Quadrangle& right_part)
	{
		float3 horizontal_dir = wall_to_split.get_horizontal_direction();

		left_part = wall_to_split;
		left_part.b = wall_to_split.a + horizontal_dir * left_part_width;
		left_part.c = wall_to_split.d + horizontal_dir * left_part_width;

		right_part = wall_to_split;
		right_part.a = left_part.b;
		right_part.d = left_part.c;
	}

	__forceinline__ __device__ void split_wall_v(
		const Quadrangle& wall_to_split,
		Quadrangle& upper_part, float upper_part_width,
		Quadrangle& central_part,
		Quadrangle& lower_part, float lower_part_width)
	{
		float3 vertical_dir = wall_to_split.get_vertical_direction();

		central_part.a = wall_to_split.a + vertical_dir * lower_part_width;
		central_part.b = wall_to_split.b + vertical_dir * lower_part_width;
		central_part.c = wall_to_split.c - vertical_dir * upper_part_width;
		central_part.d = wall_to_split.d - vertical_dir * upper_part_width;

		upper_part = wall_to_split;
		upper_part.a = central_part.d;
		upper_part.b = central_part.c;

		lower_part = wall_to_split;
		lower_part.d = central_part.a;
		lower_part.c = central_part.b;
	}

	__forceinline__ __device__ void split_wall_v(
		const Quadrangle& wall_to_split,
		Quadrangle& upper_part, float upper_part_width,
		Quadrangle& lower_part)
	{
		float3 vertical_dir = wall_to_split.get_vertical_direction();

		upper_part = wall_to_split;
		upper_part.a = wall_to_split.d - vertical_dir * upper_part_width;
		upper_part.b = wall_to_split.c - vertical_dir * upper_part_width;

		lower_part = wall_to_split;
		lower_part.d = upper_part.a;
		lower_part.c = upper_part.b;
	}

	__forceinline__ __device__ float3 get_lamp_pillar_bottom_disc_center(OptixAabb lamp_aabb)
	{
		float2 lamp_bottom_disc_center2d = make_float2((lamp_aabb.maxX + lamp_aabb.minX) / 2.0f, (lamp_aabb.maxZ + lamp_aabb.minZ) / 2.0f);
		return make_float3(lamp_bottom_disc_center2d.x, lamp_aabb.minY, lamp_bottom_disc_center2d.y);
	}

	__forceinline__ __device__ float calculate_lamp_glass_sphere_h(const float glass_sphere_radius, const float pillar_radius)
	{
		const float glass_sphere_radius2 = glass_sphere_radius * glass_sphere_radius;
		const float pillar_radius2 = pillar_radius * pillar_radius;

		return sqrtf(glass_sphere_radius2 - pillar_radius2);
	}

	__forceinline__ __device__ float calculate_lamp_pillar_max_y(const float lamp_max_y, const float glass_sphere_radius, const float glass_sphere_h)
	{
		return lamp_max_y - (glass_sphere_radius + glass_sphere_h);
	}

	__forceinline__ __device__ float3 calculate_lamp_bulb_center(const float pillar_max_y, const float3 bottom_disc_center, const float emissive_sphere_radius)
	{
		return make_float3(bottom_disc_center.x, pillar_max_y + emissive_sphere_radius, bottom_disc_center.z);
	}

	__forceinline__ __device__ float3 calculate_lamp_glass_sphere_center(const float max_y, const float3 bottom_disc_center, const float glass_sphere_radius)
	{
		return make_float3(bottom_disc_center.x, max_y - glass_sphere_radius, bottom_disc_center.z);
	}

	__forceinline__ __device__ bool calculate_start_and_end_floor_bounds_2_parts(
		const Quadrangle& wall, const float3& ray_origin, const float3& ray_dir,
		RegularBuildingFacadeParameters& facade_parameters, const int number_of_floors_except_shop, bool no_gaps,
		int& start_i, int& end_i
	)
	{
		bool upper_intersection = true, lower_intersection = true;

		Quadrangle wall_part_without_top_and_bottom_gaps;
		Quadrangle tmp1, tmp2;

		start_i = 0;
		end_i = number_of_floors_except_shop;

		float bottom_gap = no_gaps ? 0.0f : facade_parameters.bottom_gap + (facade_parameters.shop_at_first_floor ? facade_parameters.shop_floor_height : 0.0f);
		float top_gap = no_gaps ? 0.0f : facade_parameters.top_gap;

		if (bottom_gap > 0.0001f)
			split_wall_v(
				wall,
				tmp1,
				wall.get_height() - bottom_gap,
				tmp2
			);
		else
			tmp1 = wall;

		if (top_gap > 0.0001f)
			split_wall_v(tmp1, tmp2, top_gap, wall_part_without_top_and_bottom_gaps);
		else
			wall_part_without_top_and_bottom_gaps = tmp1;

		Quadrangle upper_part, lower_part;
		float upper_part_size;
		if (number_of_floors_except_shop % 2 == 0)
		{
			upper_part_size = wall_part_without_top_and_bottom_gaps.get_height() / 2.0f;
		}
		else
		{
			upper_part_size = (wall_part_without_top_and_bottom_gaps.get_height() / 2.0f) - facade_parameters.floor_height;
		}
		split_wall_v(wall_part_without_top_and_bottom_gaps, upper_part, upper_part_size, lower_part);

		BoundingQuadrangles bounding_quads_for_upper_part;
		bounding_quads_for_upper_part.initialize_bounding_quadrangles(upper_part, facade_parameters.max_extrusion, facade_parameters.max_intrusion);
		if (!bounding_quads_for_upper_part.does_ray_intersect_bounding_quadrangles(ray_origin, ray_dir))
		{
			end_i = static_cast<float>(number_of_floors_except_shop + 1) / 2.0f;
			upper_intersection = false;
		}

		BoundingQuadrangles bounding_quads_for_lower_part;
		bounding_quads_for_lower_part.initialize_bounding_quadrangles(lower_part, facade_parameters.max_extrusion, facade_parameters.max_intrusion);
		if (!bounding_quads_for_lower_part.does_ray_intersect_bounding_quadrangles(ray_origin, ray_dir))
		{
			start_i = static_cast<float>(number_of_floors_except_shop) / 2.0f;
			lower_intersection = false;
		}

		start_i = max(start_i - 1, 0);
		end_i = min(end_i + 1, number_of_floors_except_shop);

		return lower_intersection || upper_intersection;
	}

	__forceinline__ __device__ bool calculate_start_and_end_floor_bounds_4_parts(
		const Quadrangle& wall, const float3& ray_origin, const float3& ray_dir,
		RegularBuildingFacadeParameters& facade_parameters, int number_of_floors_except_shop,
		int& start_i, int& end_i
	)
	{
		bool upper_intersection = false, lower_intersection = false;

		Quadrangle wall_part_without_top_and_bottom_gaps;
		Quadrangle tmp1, tmp2;

		start_i = 0;
		end_i = number_of_floors_except_shop;

		split_wall_v(
			wall,
			tmp1,
			wall.get_height() - (facade_parameters.bottom_gap + (facade_parameters.shop_at_first_floor ? facade_parameters.shop_floor_height : 0.0f)),
			tmp2
		);
		split_wall_v(tmp1, tmp2, facade_parameters.top_gap, wall_part_without_top_and_bottom_gaps);

		Quadrangle upper_part, lower_part;
		float upper_part_size;
		if (number_of_floors_except_shop % 2 == 0)
		{
			upper_part_size = wall_part_without_top_and_bottom_gaps.get_height() / 2.0f;
		}
		else
		{
			upper_part_size = (wall_part_without_top_and_bottom_gaps.get_height() / 2.0f) - facade_parameters.floor_height;
		}
		split_wall_v(wall_part_without_top_and_bottom_gaps, upper_part, upper_part_size, lower_part);

		int upper_part_number_of_windows = (number_of_floors_except_shop) / 2;
		int start_i_upper_part, end_i_upper_part;
		upper_intersection = calculate_start_and_end_floor_bounds_2_parts(
			upper_part,
			ray_origin,
			ray_dir,
			facade_parameters,
			upper_part_number_of_windows,
			true,
			start_i_upper_part,
			end_i_upper_part
		);
		int lower_part_number_of_windows = (number_of_floors_except_shop + 1) / 2;
		int start_i_lower_part, end_i_lower_part;
		lower_intersection = calculate_start_and_end_floor_bounds_2_parts(
			lower_part,
			ray_origin,
			ray_dir,
			facade_parameters,
			lower_part_number_of_windows,
			true,
			start_i_lower_part,
			end_i_lower_part
		);
		start_i_upper_part += lower_part_number_of_windows;
		end_i_upper_part += lower_part_number_of_windows;

		if (lower_intersection && upper_intersection)
		{
			start_i = start_i_lower_part;
			end_i = end_i_upper_part;
		}
		else if (lower_intersection && !upper_intersection)
		{
			start_i = start_i_lower_part;
			end_i = end_i_lower_part;
		}
		else if (!lower_intersection && upper_intersection)
		{
			start_i = start_i_upper_part;
			end_i = end_i_upper_part;
		}

		start_i = max(start_i - 1, 0);
		end_i = min(end_i + 1, number_of_floors_except_shop);

		return lower_intersection || upper_intersection;
	}

	__forceinline__ __device__ bool calculate_start_and_end_floor_bounds(
		const Quadrangle& wall, const float3& ray_origin, const float3& ray_dir,
		RegularBuildingFacadeParameters& facade_parameters,
		int& start_i, int& end_i
	)
	{
		const int number_of_floors_for_4_parts = 16;

		if (facade_parameters.number_of_floors < number_of_floors_for_4_parts)
		{
			return calculate_start_and_end_floor_bounds_2_parts(wall, ray_origin, ray_dir, facade_parameters, facade_parameters.number_of_floors, false, start_i, end_i);
		}
		else
		{
			return calculate_start_and_end_floor_bounds_4_parts(wall, ray_origin, ray_dir, facade_parameters, facade_parameters.number_of_floors, start_i, end_i);
		}
	}

	__forceinline__ __device__ bool sample_plane_glass(
		const float3& ray_origin, const float3& ray_dir,
		const Quadrangle& window_quadrangle, const SceneObjectPart glass_type,
		const Material* plane_glass_material
	)
	{
		float hit = false;

		float t;
		float u, v;
		float3 intersection_point;
		float3 wall_n;
		if (does_ray_intersect_wall_from_any_side(window_quadrangle, ray_origin, ray_dir, t) == 1)
		{
			prepare_data_for_intersection_report(ray_origin, ray_dir, window_quadrangle, t, nullptr, intersection_point, u, v, wall_n);
			if (dot(wall_n, ray_dir) > 0.f)
				wall_n *= -1.f;
			report_intersection(t, wall_n, u, v, glass_type);

			hit = true;
		}

		return hit;
	}

	__forceinline__ __device__ bool sample_balcony(
		const float3& ray_origin, const float3& ray_dir, HitGroupData* hit_data,
		Quadrangle balcony_background_wall, RegularBuildingFacadeParameters& facade_parameters,
		const int balcony_id
	)
	{
		bool hit_inside = false;
		// If there is a hit from outside, it means the ray intersects another wall which is the window not any wall around it.
		bool hit_outside = false;

		float& intrusion = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_intrusion;

		float t;
		float3 intersection_point;
		float3 wall_n;
		float u, v;

		Quadrangle balcony_original_quadrangle_backup = balcony_background_wall;

		Quadrangle left, top, right, bottom;

		left = (balcony_background_wall.get_left(intrusion)).get_reversed();
		top = (balcony_background_wall.get_top(intrusion)).get_reversed();
		right = (balcony_background_wall.get_right(intrusion)).get_reversed();
		bottom = (balcony_background_wall.get_bottom(intrusion)).get_reversed();

		float3 balcony_quadrangle_n = balcony_background_wall.get_normal();

		balcony_background_wall = balcony_background_wall - balcony_quadrangle_n * intrusion;

		Quadrangle balcony_walls[4] =
		{
			left, top, right, bottom
		};

		for (auto& balcony_wall : balcony_walls)
		{
			int hit_with_balcony_wall = does_ray_intersect_wall_from_any_side(balcony_wall, ray_origin, ray_dir, t);

			if (hit_with_balcony_wall == 1)
			{
				prepare_data_for_intersection_report(ray_origin, ray_dir, balcony_wall, t, nullptr, intersection_point, u, v, wall_n);
				report_intersection_with_id(t, wall_n, u, v, SceneObjectPart::BALCONY_WALL, balcony_id);

				hit_inside = true;
			}
			else if (hit_with_balcony_wall == 2)
			{
				hit_outside = true;
			}
		}

		// Fence.
		{
			Quadrangle foreground, top, background;

			foreground.a = balcony_original_quadrangle_backup.a;
			foreground.b = balcony_original_quadrangle_backup.b;
			foreground.c 
				= foreground.b + balcony_original_quadrangle_backup.get_vertical_direction() * facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_wall_height;
			foreground.d 
				= foreground.a + balcony_original_quadrangle_backup.get_vertical_direction() * facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_wall_height;

			float& fence_intrusion = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_wall_thickness;

			top = foreground.get_top(fence_intrusion);
			background = foreground.get_opposite(fence_intrusion);

			Quadrangle fence_parts[3]
			{
				foreground, top, background
			};

			for (auto& fence_part : fence_parts)
			{
				if (does_ray_intersect_wall(fence_part, ray_origin, ray_dir, t))
				{
					prepare_data_for_intersection_report(ray_origin, ray_dir, fence_part, t, nullptr, intersection_point, u, v, wall_n);
					report_intersection_with_id(t, wall_n, u, v, SceneObjectPart::BALCONY_FENCE, balcony_id);

					hit_inside = true;
				}
			}
		}

		if (!hit_outside)
		{
			// Glass.
			{
				Quadrangle glass_quadrangle;

				float& fence_height = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_wall_height;
				float& balcony_height = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_sizes.y;
				float glass_height = balcony_height - fence_height;

				glass_quadrangle.d = balcony_original_quadrangle_backup.d;
				glass_quadrangle.c = balcony_original_quadrangle_backup.c;
				glass_quadrangle.a = glass_quadrangle.d - balcony_original_quadrangle_backup.get_vertical_direction() * glass_height;
				glass_quadrangle.b = glass_quadrangle.c - balcony_original_quadrangle_backup.get_vertical_direction() * glass_height;
				//glass_quadrangle = balcony_original_quadrangle_backup;
				hit_inside = max(
					hit_inside,
					sample_plane_glass(
						ray_origin,
						ray_dir,
						glass_quadrangle,
						SceneObjectPart::BALCONY_GLASS,
						nullptr
					)
				);
			}

			// The main wall.
			if (does_ray_intersect_wall(balcony_background_wall, ray_origin, ray_dir, t))
			{
				prepare_data_for_intersection_report(ray_origin, ray_dir, balcony_background_wall, t, nullptr, intersection_point, u, v, wall_n);
				report_intersection_with_id(t, wall_n, u, v, SceneObjectPart::BALCONY_WALL, balcony_id);
				hit_inside = true;
			}
		}

		return hit_outside ? false : hit_inside;
	}	

	__forceinline__ __device__ bool sample_window(
		const float3& ray_origin, const float3& ray_dir, HitGroupData* hit_data,
		Quadrangle window_quadrangle,
		WindowParameters window_parameters,
		const int window_id
	)
	{
		bool hit_inside = false;
		// If there is a hit from outside, it means the ray intersects another wall which is the window not any wall around it.
		bool hit_outside = false;

		float t;
		float3 intersection_point;
		float3 wall_n;
		float u, v;

		float total_intrusion = window_parameters.outside_intrusion + window_parameters.inside_intrusion;

		{
			Quadrangle left, top, right, bottom;

			left = (window_quadrangle.get_left(total_intrusion)).get_reversed();
			top = (window_quadrangle.get_top(total_intrusion)).get_reversed();
			right = (window_quadrangle.get_right(total_intrusion)).get_reversed();
			bottom = (window_quadrangle.get_bottom(total_intrusion)).get_reversed();

			Quadrangle window_walls[4] =
			{
				left, top, right, bottom
			};

			for (auto& window_wall : window_walls)
			{
				
				int hit_with_window_wall = does_ray_intersect_wall_from_any_side(window_wall, ray_origin, ray_dir, t);

				if (hit_with_window_wall == 1)
				{
					prepare_data_for_intersection_report(ray_origin, ray_dir, window_wall, t, &hit_data->material, intersection_point, u, v, wall_n);
					SceneObjectPart obj_part
						= distance_between_plane_and_point(window_quadrangle.a, window_quadrangle.get_normal(), intersection_point) < window_parameters.outside_intrusion
						? SceneObjectPart::BUILDING_WALL : SceneObjectPart::ROOM_WALL;
					report_intersection_with_id(t, wall_n, u, v, obj_part, window_id);

					hit_inside = true;
				}
				else if (hit_with_window_wall == 2)
				{
					hit_outside = true;
				}
			}
		}

		Quadrangle window_glass_quadrangle = window_quadrangle - window_quadrangle.get_normal() * window_parameters.outside_intrusion;
		Quadrangle background_quadrangle = window_glass_quadrangle - window_glass_quadrangle.get_normal() * window_parameters.inside_intrusion;

		if (!hit_outside)
		{
			if (!window_parameters.imposts)
			{
				// Glass.
				hit_inside = max(
					hit_inside,
					sample_plane_glass(
						ray_origin,
						ray_dir,
						window_glass_quadrangle,
						SceneObjectPart::WINDOW_GLASS,
						nullptr
					)
				);
			}
			else
			{
				Quadrangle quadrangles[16];
				get_quadrangles_for_window_glass_and_imposts(window_glass_quadrangle, window_parameters.impost_position, window_parameters.impost_width, window_parameters.impost_thickness, quadrangles);
				
				for (int i = 0; i < 4; i++)
				{
					hit_inside = max(
						hit_inside,
						sample_plane_glass(
							ray_origin,
							ray_dir,
							quadrangles[i],
							SceneObjectPart::WINDOW_GLASS,
							nullptr
						)
					);
				}

				for (int i = 4; i < 16; i++)
				{
					hit_inside = max(
						hit_inside,
						sample_plane_glass(
							ray_origin,
							ray_dir,
							quadrangles[i],
							SceneObjectPart::WINDOW_IMPOST,
							nullptr
						)
					);
				}
			}

			// The main wall.
			if (does_ray_intersect_wall(background_quadrangle, ray_origin, ray_dir, t))
			{
				prepare_data_for_intersection_report(ray_origin, ray_dir, background_quadrangle, t, nullptr, intersection_point, u, v, wall_n);
				report_intersection_with_id(t, wall_n, u, v, SceneObjectPart::ROOM_WALL, window_id);

				hit_inside = true;
			}
		}

		return hit_outside ? false : hit_inside;
	}

	__forceinline__ __device__ void sample_floor_with_simple_windows(
		const float3& ray_origin, const float3& ray_dir, HitGroupData* hit_data,
		const Quadrangle& wall, const int floor, RegularBuildingFacadeParameters& facade_parameters, bool& hit
	)
	{
		float3 vertical_dir = wall.get_vertical_direction();
		float3 horizontal_dir = wall.get_horizontal_direction();

		float first_floor_above_shop_height
			= facade_parameters.bottom_gap + facade_parameters.shop_floor_height * facade_parameters.shop_at_first_floor;

		float y = wall.a.y + first_floor_above_shop_height + facade_parameters.floor_height * floor;

		Quadrangle wall_part_for_floor = get_wall_part_for_floor(
			vertical_dir,
			make_float3(wall.a.x, y, wall.a.z),
			make_float3(wall.b.x, y, wall.b.z),
			facade_parameters.floor_height
		);

		BoundingQuadrangles bounding_quads_for_floor;
		bounding_quads_for_floor.initialize_bounding_quadrangles(
			wall_part_for_floor,
			0,
			facade_parameters.facade_slots.simple_window_parameters.window_parameters.outside_intrusion + facade_parameters.facade_slots.simple_window_parameters.window_parameters.inside_intrusion
		);

		if (bounding_quads_for_floor.does_ray_intersect_bounding_quadrangles(ray_origin, ray_dir))
		{
			for (int j = 0; j < facade_parameters.number_of_slots_horizontally; j++)
			{
				float dist_between_floor_bottom_and_window =
					(facade_parameters.floor_height - facade_parameters.facade_slots.simple_window_parameters.window_parameters.sizes.y) / 2.0f;

				float3 window_slot = get_facade_slot_start_point(
					horizontal_dir,
					make_float3(wall.a.x, y + dist_between_floor_bottom_and_window, wall.a.z),
					facade_parameters.facade_slots.simple_window_parameters.get_slot(),
					facade_parameters.facade_slots.simple_window_parameters.edge_gap,
					j
				);

				Quadrangle window_quadrangle = get_quadrangle_in_slot(
					horizontal_dir,
					vertical_dir,
					window_slot,
					facade_parameters.facade_slots.simple_window_parameters.window_parameters.sizes.x,
					facade_parameters.facade_slots.simple_window_parameters.window_parameters.sizes.y,
					facade_parameters.facade_slots.simple_window_parameters.gap_between_windows / 2.f
				);

				BoundingQuadrangles bounding_quads_for_window;
				bounding_quads_for_window.initialize_bounding_quadrangles(
					window_quadrangle,
					0,
					facade_parameters.facade_slots.simple_window_parameters.window_parameters.outside_intrusion + facade_parameters.facade_slots.simple_window_parameters.window_parameters.inside_intrusion
				);

				if (bounding_quads_for_window.does_ray_intersect_bounding_quadrangles(ray_origin, ray_dir))
				{
					int window_id = (floor * facade_parameters.number_of_slots_horizontally + j) * 1234 * floor;
					hit = max(
						sample_window(
							ray_origin,
							ray_dir,
							hit_data,
							window_quadrangle,
							facade_parameters.facade_slots.simple_window_parameters.window_parameters,
							window_id
						), hit);
				}
			}
		}
	}

	__forceinline__ __device__ void sample_floor_with_window_2_balconies_window(
		const float3& ray_origin, const float3& ray_dir, HitGroupData* hit_data,
		const Quadrangle& wall, const int floor, RegularBuildingFacadeParameters& facade_parameters, bool& hit
	)
	{
		const int common_id_factor = 4321;

		float3 vertical_dir = wall.get_vertical_direction();
		float3 horizontal_dir = wall.get_horizontal_direction();

		float first_floor_above_shop_height
			= facade_parameters.bottom_gap + facade_parameters.shop_floor_height * facade_parameters.shop_at_first_floor;

		float y = wall.a.y + first_floor_above_shop_height + facade_parameters.floor_height * floor;

		Quadrangle wall_part_for_floor = get_wall_part_for_floor(
			vertical_dir,
			make_float3(wall.a.x, y, wall.a.z),
			make_float3(wall.b.x, y, wall.b.z),
			facade_parameters.floor_height
		);

		BoundingQuadrangles bounding_quads_for_floor;
		bounding_quads_for_floor.initialize_bounding_quadrangles(wall_part_for_floor, 0, facade_parameters.max_intrusion);

		if (bounding_quads_for_floor.does_ray_intersect_bounding_quadrangles(ray_origin, ray_dir))
		{
			for (int j = 0; j < facade_parameters.number_of_slots_horizontally; j++)
			{
				float dist_between_floor_bottom_and_window =
					(facade_parameters.floor_height
						- facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.y)
					/ 2.0f;

				float dist_between_floor_bottom_and_balcony =
					(facade_parameters.floor_height
						- facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_sizes.y)
					/ 2.0f;

				float3 current_slot = get_facade_slot_start_point(
					horizontal_dir,
					make_float3(wall.a.x, y, wall.a.z),
					facade_parameters.facade_slots.window_2_balconies_window_parameters.get_slot(),
					facade_parameters.facade_slots.window_2_balconies_window_parameters.edge_gap,
					j
				);

				float3 current_slot_for_windows = current_slot + vertical_dir * dist_between_floor_bottom_and_window;
				float3 current_slot_for_balcony = current_slot + vertical_dir * dist_between_floor_bottom_and_balcony;

				// Windows. -----------------------------------------------------------------------------------------------------------------
				{
					Quadrangle window_quadrangles[2];
					{
						Quadrangle q = get_quadrangle_in_slot(
							horizontal_dir,
							vertical_dir,
							current_slot_for_windows,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.x,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.y,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_windows / 2.f
						);

						window_quadrangles[0].a = q.a;
						window_quadrangles[0].b = q.b;
						window_quadrangles[0].c = q.c;
						window_quadrangles[0].d = q.d;
					}
					{
						float3 _current_slot_for_windows = current_slot_for_windows + horizontal_dir * facade_parameters.facade_slots.window_2_balconies_window_parameters.get_slot();

						Quadrangle q = get_quadrangle_in_slot(
							-horizontal_dir,
							vertical_dir,
							_current_slot_for_windows,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.x,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.y,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_windows / 2.f
						);
						q = q.get_reversed();

						window_quadrangles[1].a = q.a;
						window_quadrangles[1].b = q.b;
						window_quadrangles[1].c = q.c;
						window_quadrangles[1].d = q.d;
					}

					int window_in_slot = common_id_factor;
					for (auto& window_quadrangle : window_quadrangles)
					{
						BoundingQuadrangles bounding_quads_for_window;
						bounding_quads_for_window.initialize_bounding_quadrangles(
							window_quadrangle,
							0,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.inside_intrusion
							+ facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.outside_intrusion
						);

						if (bounding_quads_for_window.does_ray_intersect_bounding_quadrangles(ray_origin, ray_dir))
						{
							int window_id = (floor * facade_parameters.number_of_slots_horizontally + j) * 1234 * floor * window_in_slot;
							hit = max(
								sample_window(
									ray_origin,
									ray_dir,
									hit_data,
									window_quadrangle,
									facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters,
									window_id
								), hit);
						}

						window_in_slot *= window_in_slot;
					}
				}

				// Balconies. ------------------------------------------------------------------------------------------------------------------------
				{
					Quadrangle balconies_quadrangles[2];
					{
						const float& gap_between_windows = facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_windows;
						const float& window_u = facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.x;
						const float& gap_between_window_and_balcony = facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_window_and_balcony;
						const float& balcony_u = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_sizes.x;
						const float& balcony_v = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_sizes.y;

						Quadrangle q = get_quadrangle_in_slot(
							horizontal_dir,
							vertical_dir,
							current_slot_for_balcony,
							balcony_u,
							balcony_v,
							(facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_windows / 2.f) + window_u + gap_between_window_and_balcony
						);

						balconies_quadrangles[0].a = q.a;
						balconies_quadrangles[0].b = q.b;
						balconies_quadrangles[0].c = q.c;
						balconies_quadrangles[0].d = q.d;
					}
					{
						const float& slot_size = facade_parameters.facade_slots.window_2_balconies_window_parameters.get_slot();
						const float& gap_between_windows = facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_windows;
						const float& window_u = facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.x;
						const float& gap_between_window_and_balcony = facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_window_and_balcony;
						const float& balcony_u = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_sizes.x;
						const float& balcony_v = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_sizes.y;

						Quadrangle q = get_quadrangle_in_slot(
							-horizontal_dir,
							vertical_dir,
							current_slot_for_balcony + horizontal_dir * slot_size,
							balcony_u,
							balcony_v,
							(facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_windows / 2.f) + window_u + gap_between_window_and_balcony
						);
						q = q.get_reversed();

						balconies_quadrangles[1].a = q.a;
						balconies_quadrangles[1].b = q.b;
						balconies_quadrangles[1].c = q.c;
						balconies_quadrangles[1].d = q.d;
					}

					int balcony_in_slot = common_id_factor;
					for (auto& balcony_quadrangle : balconies_quadrangles)
					{
						BoundingQuadrangles bounding_quads_for_window;
						bounding_quads_for_window.initialize_bounding_quadrangles(
							balcony_quadrangle,
							0,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_intrusion
						);

						if (bounding_quads_for_window.does_ray_intersect_bounding_quadrangles(ray_origin, ray_dir))
						{
							int balcony_id = (floor * facade_parameters.number_of_slots_horizontally + j) * 1234 * floor * balcony_in_slot;
							hit = max(
								sample_balcony(
									ray_origin,
									ray_dir,
									hit_data,
									balcony_quadrangle,
									facade_parameters,
									balcony_id
								), hit);
						}

						balcony_in_slot *= balcony_in_slot;
					}
				}
			}
		}
	}	

	__forceinline__ __device__ void sample_facade(
		const float3& ray_origin, const float3& ray_dir, HitGroupData* hit_data,
		const Quadrangle& wall, RegularBuildingFacadeParameters& facade_parameters
	)
	{
		float t;
		float u, v;
		float wall_width = length(wall.a - wall.b);
		float wall_height = length(wall.d - wall.a);
		float3 vertical_dir = wall.get_vertical_direction();
		float3 horizontal_dir = wall.get_horizontal_direction();

		bool hit = false;

		float3 wall_n = wall.get_normal();

		float3 intersection_point;

		if (facade_parameters.shop_at_first_floor)
		{
			Quadrangle wall_part_for_floor_with_shop = get_wall_part_for_floor(
				vertical_dir, 
				wall.a,
				wall.b,
				facade_parameters.shop_floor_height
			);

			BoundingQuadrangles bounding_quads_for_floor_with_shop;
			bounding_quads_for_floor_with_shop.initialize_bounding_quadrangles(
				wall_part_for_floor_with_shop,
				0,
				facade_parameters.shop_window_parameters.window_parameters.outside_intrusion + facade_parameters.shop_window_parameters.window_parameters.inside_intrusion
			);

			if (bounding_quads_for_floor_with_shop.does_ray_intersect_bounding_quadrangles(ray_origin, ray_dir))
			{
				for (int i = 0; i < facade_parameters.number_of_shop_windows_horizontally; i++)
				{
					float3 shop_window_slot = get_facade_slot_start_point(
						horizontal_dir,
						make_float3(wall.a.x, wall.a.y, wall.a.z),
						facade_parameters.shop_window_parameters.get_slot(),
						facade_parameters.shop_window_parameters.edge_gap,
						i
					);
					Quadrangle shop_window_quad = get_quadrangle_in_slot(
						horizontal_dir,
						vertical_dir,
						shop_window_slot,
						facade_parameters.shop_window_parameters.window_parameters.sizes.x,
						facade_parameters.shop_window_parameters.window_parameters.sizes.y,
						facade_parameters.shop_window_parameters.gap_between_windows / 2.f
					);

					hit = max(
						sample_window(
							ray_origin,
							ray_dir,
							hit_data,
							shop_window_quad,
							facade_parameters.shop_window_parameters.window_parameters,
							-1
						),
						hit);
				}
			}
		}

		// Check if the point is inside a window.
		{
			int start_i = 0, end_i = facade_parameters.number_of_floors;

			if (!calculate_start_and_end_floor_bounds(wall, ray_origin, ray_dir, facade_parameters, start_i, end_i))
			{
				start_i = 0;
				end_i = -1;
			}

			for (int i = start_i; i < end_i; i++)
			{
				switch (facade_parameters.facade_slot_type)
				{
					case FacadeSlotType::SIMPLE_WINDOWS:
					{
						sample_floor_with_simple_windows(ray_origin, ray_dir, hit_data, wall, i, facade_parameters, hit);
					} break;

					case FacadeSlotType::WINDOW_BALCONY_BALCONY_WINDOW:
					{
						sample_floor_with_window_2_balconies_window(ray_origin, ray_dir, hit_data, wall, i, facade_parameters, hit);
					} break;
				}
			}
		}

		// It's not inside any window.
		if (!hit && does_ray_intersect_wall(wall, ray_origin, ray_dir, t))
		{
			prepare_data_for_intersection_report(ray_origin, ray_dir, wall, t, &hit_data->material, intersection_point, u, v, wall_n);
			report_intersection(t, wall_n, u, v, SceneObjectPart::BUILDING_WALL);
		}
	}

	__forceinline__ __device__ void get_vertices(HitGroupData* hit_data, const int prim_idx, float3 vertices[3])
	{
		const int vert_idx_offset = prim_idx * 3;
		const float3 v0 = hit_data->traditional_geometry_hit_group_data.vertices[vert_idx_offset + 0];
		const float3 v1 = hit_data->traditional_geometry_hit_group_data.vertices[vert_idx_offset + 1];
		const float3 v2 = hit_data->traditional_geometry_hit_group_data.vertices[vert_idx_offset + 2];

		vertices[0] = v0;
		vertices[1] = v1;
		vertices[2] = v2;
	}

	__forceinline__ __device__ Quadrangle get_quadrangle_from_vertex_array(HitGroupData* hit_data, const int prim_idx)
	{
		const int vert_idx_offset = prim_idx * 3;
		float3 vertices[3];
		get_vertices(hit_data, prim_idx, vertices);
		const float3 v0 = vertices[0];
		const float3 v1 = vertices[1];
		const float3 v2 = vertices[2];
		const float3 v3 = prim_idx % 2 == 0 ? hit_data->traditional_geometry_hit_group_data.vertices[vert_idx_offset + 4] : hit_data->traditional_geometry_hit_group_data.vertices[vert_idx_offset - 2];
		Quadrangle quadrangle;
		if (prim_idx % 2 == 0)
		{
			quadrangle.a = v0;
			quadrangle.b = v1;
			quadrangle.c = v2;
			quadrangle.d = v3;
		}
		else
		{
			quadrangle.a = v2;
			quadrangle.b = v3;
			quadrangle.c = v0;
			quadrangle.d = v1;
		}
		return quadrangle;
	}	
}