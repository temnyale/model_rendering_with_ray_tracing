/**
 * Data structure that simplifies the process of image rendering.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <sutil\CUDAOutputBuffer.h>
#include <iomanip>

#include "IterationCounter.h"

namespace drpm
{
	void save_image(sutil::CUDAOutputBuffer<uchar4>& output_buffer);

	class ImageRenderingHandler
	{
	private:
		sutil::CUDAOutputBuffer<uchar4>* _output_buffer;
		bool _save_current_image;
		IterationCounter _iteration_counter;
		bool _finished;

	public:
		ImageRenderingHandler() : _output_buffer(nullptr), _save_current_image(false), _finished(false)
		{ }

		void initialize(sutil::CUDAOutputBuffer<uchar4>* output_buffer) 
		{
			_output_buffer = output_buffer;
		}

		void save_current_image()
		{
			_save_current_image = true;
		}

		void set_total_number_of_frames(int total_number_of_frames)
		{
			_iteration_counter.set_total_number_of_iterations(total_number_of_frames);
		}

		int get_total_number_of_frames()
		{
			return _iteration_counter.get_total_number_of_iterations();
		}

		void start_image_rendering()
		{
			_iteration_counter.start();
		}

		void update()
		{
			if (_finished)
				_finished = false;

			if (_save_current_image)
			{
				save_image(*_output_buffer);
				_save_current_image = false;
			}

			if (_iteration_counter.is_running())
			{
				if (_iteration_counter.iterate())
				{
					save_image(*_output_buffer);
					_finished = true;
				}
			}
		}

		bool is_rendering()
		{
			return _iteration_counter.is_running();
		}

		bool finished()
		{
			return _finished;
		}

		void print()
		{
			_iteration_counter.print();
		}

		int number_of_iterations_to_end()
		{
			return _iteration_counter.number_of_iterations_to_end();
		}
	};
}