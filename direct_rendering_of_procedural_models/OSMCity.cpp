﻿#include "OSMCity.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <regex>
#include <unordered_map>
#include <helpers.h>
#include <sutil\sutil.h>
#include <random>

#include "utils.h"
#include "math_utils.h"
#include "OptixAabb_ext.h"
#include "vector3.h"
#include "path_settings.h"


void drpm::OSMCity::create_block_of_lamps(float2 start_position, float2 lamp_left, float2 lamp_forward, float2 shift, int number, float min_y, float max_y)
{
	float3 lamp_left3d = make_float3(lamp_left.x, 0, lamp_left.y);
	float3 lamp_forward3d = make_float3(lamp_forward.x, 0, lamp_forward.y);
	float3 lamp_up3d = vector3::up() * (max_y - min_y);

	float2 slot_start = start_position;
	for (int lamp_i = 0; lamp_i < number; lamp_i++)
	{
		float2 lamp_center = slot_start;
		float3 lamp_corner3d = make_float3(lamp_center.x, min_y, lamp_center.y);

		float2 lamp_a = lamp_center - lamp_left / 2.f - lamp_forward / 2.f;
		float2 lamp_b = lamp_a + lamp_forward;
		float2 lamp_c = lamp_b + lamp_left;
		float2 lamp_d = lamp_c - lamp_forward;

		StreetLamp* street_lamp = new StreetLamp({lamp_a, lamp_b, lamp_c, lamp_d}, max_y, min_y);

		street_lamps.push_back(street_lamp);

		OptixAabb* collider_aabb = new OptixAabb();

		street_lamp->calculate_collider_aabb(*collider_aabb);
		street_lamps_and_colliders[street_lamp] = collider_aabb;

		slot_start += shift;
	}
}

void drpm::OSMCity::create_sidewalk_and_lamps(
	const float2 end_right, const float2 end_left, 
	const float2 start_right, const float2 start_left
)
{
	float min_sidewalk_length_to_place_lamp = drpm::lamp_pillar_diameter;
	float min_sidewalk_width_to_place_lamp = 2.0f * drpm::lamp_pillar_diameter;
	float lamp_diameter = (drpm::lamp_glass_sphere_diameter > drpm::lamp_pillar_diameter) ? drpm::lamp_glass_sphere_diameter : drpm::lamp_pillar_diameter;

	float2 mid_at_start = (start_right + start_left) / 2.0f;
	float2 mid_at_end = (end_right + end_left) / 2.0f;

	float2 block_forward = normalize(mid_at_end - mid_at_start);
	float2 block_left = make_float2(-block_forward.y, block_forward.x);

	bool left_is_closest_at_start = false;
	if (dot(normalize(mid_at_start - start_right), block_forward) >= 0.f)
		left_is_closest_at_start = true;

	bool left_is_closest_at_end = true;
	if (dot(normalize(end_left - mid_at_end), block_forward) >= 0.f)
		left_is_closest_at_end = false;

	float sidewalk_length = length((left_is_closest_at_start ? start_left : start_right) - (left_is_closest_at_end ? end_left : end_right));
	float sidewalk_width = std::min(get_projection(end_right - end_left, block_left), get_projection(start_right - start_left, block_left));
	float mid_proj = get_projection(start_right - start_left, block_left) / 2.f;

	float space_for_lamps = sidewalk_length - 2.0f * drpm::gap_between_road_edge_and_lamp;
	if (!isnan(space_for_lamps) && !isinf(space_for_lamps) 
		&& !isnan(sidewalk_length) && !isinf(sidewalk_length)
		&& !isnan(sidewalk_width) && !isinf(sidewalk_width)
		&& space_for_lamps > min_sidewalk_length_to_place_lamp && sidewalk_width > min_sidewalk_width_to_place_lamp)
	{
		Sidewalk* sidewalk = new Sidewalk({ start_right, end_right, end_left, start_left }, drpm::terrain_height + drpm::sidewalk_height, drpm::terrain_height);
		sidewalks.push_back(sidewalk);

		float lamp_slot_size = lamp_diameter + 2.0f * drpm::gap_around_lamp;
		float num_of_lamps = floorf(space_for_lamps / lamp_slot_size);
		float remaining_size = space_for_lamps - num_of_lamps * lamp_slot_size;
		
		if (num_of_lamps > 0)
		{
			float current_gap_around_lamp = drpm::gap_around_lamp + (remaining_size / num_of_lamps) / 2.f;
			lamp_slot_size = lamp_diameter + 2.0f * current_gap_around_lamp;

			float2 lamp_forward = block_forward * lamp_diameter;
			float2 lamp_left = block_left * lamp_diameter;
			float2 start_position =
				(left_is_closest_at_start ? start_left : start_right)
				+ block_forward * (drpm::gap_between_road_edge_and_lamp + current_gap_around_lamp + lamp_diameter / 2.f)
				+ (left_is_closest_at_start ? -1.f : 1.f) * normalize(lamp_left) * mid_proj;
			float2 shift = block_forward * lamp_slot_size;
			float new_slot_size = lamp_diameter + 2.0f * current_gap_around_lamp;

			create_block_of_lamps(
				start_position,
				lamp_left,
				lamp_forward,
				shift,
				num_of_lamps,
				drpm::terrain_height + drpm::sidewalk_height,
				drpm::lamp_height + drpm::sidewalk_height + drpm::terrain_height
			);
		}
	}
}

float drpm::OSMCity::calculate_building_average_height()
{
	float building_average_height = 0.f;

	for (auto& building : get_buildings())
		building_average_height += building->aabb.maxY - building->aabb.minY;

	building_average_height /= get_buildings().size();

	return building_average_height;
}

void drpm::OSMCity::create_street_lamps()
{
	for (auto edge_road : edge_roads)
	{
		// Left side.
		{
			float2 c_inner = edge_road->get_c_inner();
			float2 c_outer = edge_road->c_outer;
			float2 d_inner = edge_road->get_d_inner();
			float2 d_outer = edge_road->d_outer;

			create_sidewalk_and_lamps(c_inner, c_outer, d_inner, d_outer);
		}

		{
			float2 b_inner = edge_road->get_b_inner();
			float2 b_outer = edge_road->b_outer;
			float2 a_inner = edge_road->get_a_inner();
			float2 a_outer = edge_road->a_outer;

			create_sidewalk_and_lamps(b_outer, b_inner, a_outer, a_inner);
		}
	}
}

void drpm::OSMCity::read_file(const std::string path)
{
	{
		terrain_aabb.maxX = std::numeric_limits<float>::min();
		terrain_aabb.maxY = drpm::terrain_height;
		terrain_aabb.maxZ = std::numeric_limits<float>::min();

		terrain_aabb.minX = std::numeric_limits<float>::max();
		terrain_aabb.minY = 0.0f;
		terrain_aabb.minZ = std::numeric_limits<float>::max();
	}

	std::ifstream in(path, std::ios::in);
	if (!in)
	{
		std::cerr << "Cannot open " << path << std::endl;
		return;
	}

	std::string line;
	std::unordered_map<long long, Node*> map_of_nodes;
	std::unordered_map<long long, std::unordered_set<long>> map_of_neighbor_ids;

	// Read nodes.
	{
		getline(in, line);
		if (line.rfind("Nodes", 0) != 0)
		{
			std::cerr << "line.rfind(\"Nodes\", 0) != 0" << std::endl;
			return;
		}

		std::vector<std::string> splitted_line;

		splitted_line = utils::split_string(line, ' ');

		int number_of_nodes = std::atoi(splitted_line[1].c_str());

		for (int i = 0; i < number_of_nodes; i++)
		{
			long long id;
			float3 location;

			// Check the current line.
			{
				getline(in, line);
				if (line.rfind("Node", 0) != 0)
				{
					std::cerr << "line.rfind(\"Node\", 0) != 0" << std::endl;
					return;
				}
			}

			// Read the node's ID.
			{
				getline(in, line);
				if (line.rfind("ID", 0) != 0)
				{
					std::cerr << "line.rfind(\"ID\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');
				id = std::stoll(splitted_line[1].c_str());
			}

			// Read the node's location.
			{
				getline(in, line);
				if (line.rfind("Location", 0) != 0)
				{
					std::cerr << "line.rfind(\"Location\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');
				location = make_float3(std::stof(splitted_line[1]), drpm::terrain_height, std::stof(splitted_line[2]));
			}

			// Read the node's neighbors.
			{
				getline(in, line);
				if (line.rfind("Neighbors", 0) != 0)
				{
					std::cerr << "line.rfind(\"Neighbors\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');
				int number_of_neighbors = splitted_line.size() - 1;

				for (int j = 0; j < number_of_neighbors; j++)
				{
					map_of_neighbor_ids[id].insert(std::stoll(splitted_line[j + 1].c_str()));
				}
			}

			// Create an instance of the node and add into the set of nodes.
			{
				Node* node = new Node(id, location);
				nodes.push_back(node);
				map_of_nodes[node->get_id()] = node;

				terrain_aabb.minX = std::min(node->get_location().x, terrain_aabb.minX);
				terrain_aabb.minZ = std::min(node->get_location().z, terrain_aabb.minZ);

				terrain_aabb.maxX = std::max(node->get_location().x, terrain_aabb.maxX);
				terrain_aabb.maxZ = std::max(node->get_location().z, terrain_aabb.maxZ);
			}

			// Empty line.
			getline(in, line);
		}

		// Add neighbors to each node.
		for (auto node : nodes)
		{
			for (auto neighbor_id : map_of_neighbor_ids[node->get_id()])
			{
				Node* neighbor = map_of_nodes[neighbor_id];
				node->add_neighbor(neighbor);
			}
		}
	}

	// Skip two empty lines.
	getline(in, line);
	getline(in, line);

	// Read edges.
	{
		getline(in, line);
		if (line.rfind("Edges", 0) != 0)
		{
			std::cerr << "line.rfind(\"Edges\", 0) != 0" << std::endl;
			return;
		}

		std::vector<std::string> splitted_line;

		splitted_line = utils::split_string(line, ' ');

		int number_of_edges = std::atoi(splitted_line[1].c_str());

		for (int i = 0; i < number_of_edges; i++)
		{
			long long id;
			long long from, to;

			// Check the current line.
			{
				getline(in, line);
				if (line.rfind("Edge", 0) != 0)
				{
					std::cerr << "line.rfind(\"Edge\", 0) != 0" << std::endl;
					return;
				}
			}

			// Read the edges's ID.
			{
				getline(in, line);
				if (line.rfind("ID", 0) != 0)
				{
					std::cerr << "line.rfind(\"ID\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');
				id = std::stoll(splitted_line[1].c_str());
			}

			// Read the first node.
			{
				getline(in, line);
				if (line.rfind("From", 0) != 0)
				{
					std::cerr << "line.rfind(\"From\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');
				from = std::atol(splitted_line[1].c_str());
			}

			// Read the second node.
			{
				getline(in, line);
				if (line.rfind("To", 0) != 0)
				{
					std::cerr << "line.rfind(\"To\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');
				from = std::atol(splitted_line[1].c_str());
			}

			// Create an instance of the edge and add into the set of edges.
			{
				
			}

			// Empty line.
			getline(in, line);
		}
	}

	// Skip two empty lines.
	getline(in, line);
	getline(in, line);

	// Read buildings
	{
		getline(in, line);
		if (line.rfind("Buildings", 0) != 0)
		{
			std::cerr << "line.rfind(\"Buildings\", 0) != 0" << std::endl;
			return;
		}

		std::vector<std::string> splitted_line;

		splitted_line = utils::split_string(line, ' ');

		int number_of_buildings = std::atoi(splitted_line[1].c_str());

		std::mt19937 gen(drpm::global_seed);
		srand(drpm::global_seed);
		for (int i = 0; i < number_of_buildings; i++)
		{				
			Building* building;
			long long id;
			bool broken_building = false;

			// Check the current line.
			{
				getline(in, line);
				if (line.rfind("Building", 0) != 0)
				{
					std::cerr << "line.rfind(\"Building\", 0) != 0" << std::endl;
					return;
				}
			}

			// Read the building's ID.
			{
				getline(in, line);
				if (line.rfind("ID", 0) != 0)
				{
					std::cerr << "line.rfind(\"ID\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');
				id = std::stoll(splitted_line[1].c_str());
			}

			// Read the building's type.
			{
				getline(in, line);
				if (line.rfind("Type", 0) != 0)
				{
					std::cerr << "line.rfind(\"Type\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');
			}

			// Create an instance of the building and add into the set of buildings.
			{
				float height = building_min_height;

				std::lognormal_distribution<float> dis_height(1.0f, 4.0f);

				{
					float random_float;

					do
					{
						random_float = dis_height(gen);
					} while (random_float < 0.0f || random_float > 1.0f);

					height = random_float * (drpm::building_max_height - drpm::building_min_height) + drpm::building_min_height;
				}

				/*const float LO = building_min_height;
				const float HI = building_max_height;
				height = LO + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (HI - LO)));*/

				building = new Building(id, drpm::terrain_height, height + drpm::terrain_height);
			}

			// Read the building's nodes.
			{
				getline(in, line);
				if (line.rfind("Nodes", 0) != 0)
				{
					std::cerr << "line.rfind(\"Nodes\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');
				int number_of_nodes = splitted_line.size() - 1;

				for (int j = 0; j < number_of_nodes; j++)
				{
					long long node_id = std::stoll(splitted_line[j + 1].c_str());
					Node* node = map_of_nodes[node_id];
					if (node == nullptr)
					{
						std::cerr << "node == nullptr, node id: " << node_id << ", building id: " << building->id << "\n";
						broken_building = true;
						//return;
					}
					else
						building->vertices.push_back(make_float2(node->get_location().x, node->get_location().z));
				}

				if (length(building->vertices[0] - building->vertices[building->vertices.size() - 1]) > 0.001f)
				{
					building->vertices.push_back(building->vertices[0]);
				}

				building->calculate_aabb();

				if (!broken_building)
					buildings.push_back(building);
				else
					delete building;
			}			

			// Empty line.
			getline(in, line);
		}
	}	

	// Skip two empty lines.
	getline(in, line);
	getline(in, line);

	// Read edge roads.
	{
		getline(in, line);
		if (line.rfind("Edge roads", 0) != 0)
		{
			std::cerr << "line.rfind(\"Edge roads\", 0) != 0" << std::endl;
			return;
		}

		std::vector<std::string> splitted_line;

		splitted_line = utils::split_string(line, ' ');

		int number_of_edge_roads = std::atoi(splitted_line[2].c_str());

		for (int i = 0; i < number_of_edge_roads; i++)
		{
			std::string type;
			float2 a_inner, b_inner, c_inner, d_inner;
			float2 a_outer, b_outer, c_outer, d_outer;
			long long id;

			// Check the current line.
			{
				getline(in, line);
				if (line.rfind("Edge road", 0) != 0)
				{
					std::cerr << "line.rfind(\"Edge road\", 0) != 0" << std::endl;
					return;
				}
			}			

			// Read the edge road's edge ID.
			{
				getline(in, line);
				if (line.rfind("Edge ID", 0) != 0)
				{
					std::cerr << "line.rfind(\"Edge ID\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');
				id = std::stoll(splitted_line[2].c_str());
			}

			// Read the edge road's type.
			{
				getline(in, line);
				if (line.rfind("Type", 0) != 0)
				{
					std::cerr << "line.rfind(\"Type\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');
				type = splitted_line[1].c_str();
			}

			// Read the outer quadrangle.
			{
				getline(in, line);
				if (line.rfind("Outer quadrangle", 0) != 0)
				{
					std::cerr << "line.rfind(\"Outer quadrangle\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');

				a_outer = make_float2(std::stof(splitted_line[2]), std::stof(splitted_line[3]));
				b_outer = make_float2(std::stof(splitted_line[4]), std::stof(splitted_line[5]));
				c_outer = make_float2(std::stof(splitted_line[6]), std::stof(splitted_line[7]));
				d_outer = make_float2(std::stof(splitted_line[8]), std::stof(splitted_line[9]));
			}

			// Read the inner quadrangle.
			{
				getline(in, line);
				if (line.rfind("Inner quadrangle", 0) != 0)
				{
					std::cerr << "line.rfind(\"Inner quadrangle\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');

				a_inner = make_float2(std::stof(splitted_line[2]), std::stof(splitted_line[3]));
				b_inner = make_float2(std::stof(splitted_line[4]), std::stof(splitted_line[5]));
				c_inner = make_float2(std::stof(splitted_line[6]), std::stof(splitted_line[7]));
				d_inner = make_float2(std::stof(splitted_line[8]), std::stof(splitted_line[9]));
			}

			{
				EdgeRoad* edge_road = new EdgeRoad(
					type, 
					a_inner, b_inner, c_inner, d_inner, 
					a_outer, b_outer, c_outer, d_outer,
					drpm::terrain_height + drpm::road_height, drpm::terrain_height
				);			

				edge_roads.push_back(edge_road);

				terrain_aabb.minX = std::min(edge_road->aabb.minX, terrain_aabb.minX);
				terrain_aabb.minZ = std::min(edge_road->aabb.minZ, terrain_aabb.minZ);

				terrain_aabb.maxX = std::max(edge_road->aabb.maxX, terrain_aabb.maxX);
				terrain_aabb.maxZ = std::max(edge_road->aabb.maxZ, terrain_aabb.maxZ);
			}

			// Empty line.
			getline(in, line);
		}
	}

	// Skip two empty lines.
	getline(in, line);
	getline(in, line);

	// Read joint roads.
	{
		getline(in, line);
		if (line.rfind("Joint roads", 0) != 0)
		{
			std::cerr << "line.rfind(\"Joint roads\", 0) != 0" << std::endl;
			return;
		}

		std::vector<std::string> splitted_line;

		splitted_line = utils::split_string(line, ' ');

		int number_of_joint_roads = std::atoi(splitted_line[2].c_str());

		for (int i = 0; i < number_of_joint_roads; i++)
		{
			std::vector<float2> vertices;
			long long id;

			// Check the current line.
			{
				getline(in, line);
				if (line.rfind("Joint road", 0) != 0)
				{
					std::cerr << "line.rfind(\"Joint road\", 0) != 0" << std::endl;
					return;
				}
			}

			// Read the joint road's node ID.
			{
				getline(in, line);
				if (line.rfind("Node ID", 0) != 0)
				{
					std::cerr << "line.rfind(\"Node ID\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');
				id = std::stoll(splitted_line[2].c_str());
			}

			// Read the rectangle.
			{
				getline(in, line);
				if (line.rfind("Vertices", 0) != 0)
				{
					std::cerr << "line.rfind(\"Vertices\", 0) != 0" << std::endl;
					return;
				}

				splitted_line = utils::split_string(line, ' ');

				for (int j = 1; j + 1 < splitted_line.size(); j += 2)
				{
					vertices.push_back(make_float2(std::stof(splitted_line[j]), std::stof(splitted_line[j + 1])));

					//printf("%f %f\n", vertices[j - 1].x, vertices[j - 1].y);
				}
			}

			{
				JointRoad* joint_road = new JointRoad(vertices, drpm::terrain_height + drpm::road_height, drpm::terrain_height);
				
				joint_roads.push_back(joint_road);

				terrain_aabb.minX = std::min(joint_road->aabb.minX, terrain_aabb.minX);
				terrain_aabb.minZ = std::min(joint_road->aabb.minZ, terrain_aabb.minZ);

				terrain_aabb.maxX = std::max(joint_road->aabb.maxX, terrain_aabb.maxX);
				terrain_aabb.maxZ = std::max(joint_road->aabb.maxZ, terrain_aabb.maxZ);
			}

			// Empty line.
			getline(in, line);
		}
	}

	terrain_aabb.maxX += drpm::terrain_extension_x;
	terrain_aabb.minX -= drpm::terrain_extension_x;
	terrain_aabb.maxZ += drpm::terrain_extension_z;
	terrain_aabb.minZ -= drpm::terrain_extension_z;

	if (drpm::path_to_heightmap != "")
	{
		sutil::ImageBuffer image;
		std::string path = drpm::path_to_heightmap;
		image = sutil::loadImage(path.c_str());

		for (auto& building : buildings)
		{
			float3 average_position = make_float3(0.0f);
			for (auto vertex : building->vertices)
			{
				average_position += make_float3(vertex.x, 0.f, vertex.y);
			}
			average_position /= building->vertices.size();

			int u = map(average_position.x, terrain_aabb.minX, terrain_aabb.maxX, 0, image.width - 1);
			int v = map(average_position.z, terrain_aabb.minZ, terrain_aabb.maxZ, 0, image.height - 1);

			float value_from_texture = 1.0f;

			switch (image.pixel_format)
			{
				case sutil::BufferImageFormat::UNSIGNED_BYTE4:
				{
					uchar4* image_data = static_cast<uchar4*>(image.data);
					value_from_texture = static_cast<float>(image_data[v * image.height + u].x) / 255.0f;
				} break;
				
				case sutil::BufferImageFormat::FLOAT4:
				{
					float4* image_data = static_cast<float4*>(image.data);
					value_from_texture = image_data[v * image.height + u].x;
				} break;

				case sutil::BufferImageFormat::FLOAT3:
				{
					float3* image_data = static_cast<float3*>(image.data);
					value_from_texture = image_data[v * image.height + u].x;
				} break;
			}


			float height = value_from_texture * (building_max_height - building_min_height) + building_min_height;
			building->aabb.maxY = building->aabb.minY + height;
		}

		delete image.data;
	}

	create_street_lamps();
}

void drpm::OSMCity::print_scene_info()
{
	std::cout << "\n";
	std::cout << "\n";
	std::cout << "===================================================================================================================\n";
	std::cout << "\n";
	std::cout << "\n";
	std::cout << "Scene information:\n";
	std::cout << "      Number of buildings:            " << get_buildings().size() << "\n";
	std::cout << "      Building average height:        " << calculate_building_average_height() << " m" << "\n";
	std::cout << "      Number of edge roads:           " << get_edge_roads().size() << "\n";
	std::cout << "      Number of joint roads:          " << get_joint_roads().size() << "\n";
	std::cout << "      Number of sidewalks:            " << get_sidewalks().size() << "\n";
	std::cout << "      Number of street_lamps:         " << get_street_lamps().size() << "\n";
	std::cout << "      Scene size:                     " << get_terrain_aabb().maxX - get_terrain_aabb().minX << " m x " << get_terrain_aabb().maxZ - get_terrain_aabb().minZ << " m" << "\n";
	std::cout << "\n";
	std::cout << "\n";
	std::cout << "===================================================================================================================\n";
	std::cout << "\n";
	std::cout << "\n";
}
