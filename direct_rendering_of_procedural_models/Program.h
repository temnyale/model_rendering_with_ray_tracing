/**
 * Data structures to handle the application.
 * Some parts are taken from the Nvidia sample projects.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#ifndef __PROGRAM_H__
#define __PROGRAM_H__

#include <cuda_runtime.h>

#include <sutil/CUDAOutputBuffer.h>
#include <sutil/Exception.h>
#include <sutil/sutil.h>
#include <sutil/GLDisplay.h>
#include <sampleConfig.h>

#include <GLFW/glfw3.h>

#include "rendering.h"
#include "OptixAabb_ext.h"
#include "general_settings.h"
#include "OSMCity.h"
#include "memory_measurement.h"
#include "image_handling.h"
#include "TimeCounter.h"

#include <iomanip>
#include <iostream>
#include <string>
#include <helpers.h>
#include <vector>

#include <sutil\Camera.h>
#include <sutil\Trackball.h>

/// <summary>
/// drpm - Direct Rendering of Procedural Models.
/// </summary>
namespace drpm
{
	// --------------------------------------------------------------------------------------------------------
	// Structures containing various variables.

	static struct OptixState
	{
		// Parameters for OptiX.
		Parameters* d_params = nullptr;
		Parameters params = {};

		ProceduralGeometryGenerationParameters current_procedural_geometry_generation_parameters;		
		SkydomeParameters skydome_parameters;
		RayParameters current_ray_parameters;
		ProceduralGeometryGenerationParameters current_procedural_geometry_generation_parameters_backup;
		RayParameters current_ray_parameters_backup;
		ProceduralGeometryGenerationParameters image_rendering_scene_parameters;
		RayParameters image_rendering_ray_parameters;
		bool parameter_changed = false;

		// OptiX structures.
		OptixDeviceContext context = nullptr;
		OptixModule module = nullptr;
		OptixPipelineCompileOptions pipeline_compile_options = {};

		// Program groups.
		OptixProgramGroup raygen_prog_group = nullptr;
		OptixProgramGroup radiance_miss_prog_group = nullptr;
		OptixProgramGroup procedural_geometry_radiance_hit_prog_group = nullptr;
		OptixProgramGroup normal_geometry_radiance_hit_prog_group = nullptr;

		OptixPipeline pipeline = nullptr;

		// Shader binding table.
		OptixShaderBindingTable sbt = {};
		std::vector<HitGroupRecord> hitgroup_records;

		// Traversable handle for geomtry acceleration structures (GAS).
		OptixTraversableHandle procedural_geometry_gas_handle = 0;
		CUdeviceptr procedural_geometry_gas_buffer = 0;
		std::vector<CUdeviceptr> aabb_buffers;

		OptixTraversableHandle traditional_geometry_gas_handle = 0;
		CUdeviceptr traditional_geometry_gas_buffer = 0;  // Triangle AS memory
		std::vector<CUdeviceptr> vertex_buffers;

		OptixTraversableHandle hIAS = 0;
		CUdeviceptr deviceBufferIAS = 0;

		CUstream stream = 0;

		std::vector<void**> cuda_allocated_sbt_memory;
	};

	static struct TextureInstance
	{
		cudaTextureObject_t cuda_texture_object;
		cudaArray_t cuda_pixel_array;
	};

	static struct Scene
	{
		// Objects.
		OSMCity* osm_city;

		// Camera.
		bool camera_changed = true;
		sutil::Camera camera;

		std::vector<TextureInstance> skydome_textures;
		std::vector<TextureInstance> building_textures;
		TextureInstance grass_texture;

		std::vector<std::vector<TriangleData>> building_parts;
	};

	static struct UserInput
	{
		// User interaction.		
		sutil::Trackball trackball;
		int32_t mouse_button = -1;
	};

	static struct WindowState
	{
		unsigned int width = 500;
		unsigned int height = 500;
		bool window_resized = false;
		bool mouse_hovering_gui = false;
		bool minimized = false;
	};

	// --------------------------------------------------------------------------------------------------------

	// --------------------------------------------------------------------------------------------------------
	
	/// <summary>
	/// The main class. It's supposed to be used as a singleton. 
	/// </summary>
	class Program
	{
	private:
		static Program* instance;

		OptixState optix_state;
		Scene scene;
		UserInput user_input;
		WindowState window_state;
		ImageRenderingHandler image_rendering_handler;

		Program()
		{
			window_state.width = application_window_width;
			window_state.height = application_window_height;
		}

		// For rendering.
		void create_scene();
		void create_context();
		void build_traditional_GAS(SceneMemoryMeasurer& scene_memory_measurer);
		void build_procedural_GAS(SceneMemoryMeasurer& scene_memory_measurer);
		void build_IAS();
		void create_module();
		void create_program_groups();
		void create_pipeline();
		void create_SBT(SceneMemoryMeasurer& scene_memory_measurer);
		void initialize_launch_parameters();
		void launch_subframe(sutil::CUDAOutputBuffer<uchar4>& output_buffer);
		void display_subframe(sutil::CUDAOutputBuffer<uchar4>& output_buffer, sutil::GLDisplay& gl_display, GLFWwindow* window);
		void update_state(sutil::CUDAOutputBuffer<uchar4>& output_buffer);
		void create_skydome_textures();
		void create_building_materials();
		void create_asphalt_material();
		void create_street_lamp_bulb_material();
		void create_window_impost_material();
		void create_grass_material();
		void create_street_lamp_material();
		void create_glass_materials();
		void cleanup();
		void initialize_gui(GLFWwindow* window);
		void display_gui();
		void start_image_rendering();
		void stop_image_rendering();
		int get_number_of_objects_with_procedural_geometry()
		{
			int n = 0;

			if (!ignore_buildings && procedural_buildings)
				n += scene.osm_city->get_buildings().size();
			if (!ignore_edge_roads && procedural_edge_roads)
				n += scene.osm_city->get_edge_roads().size();
			if (!ignore_joint_roads && procedural_joint_roads)
				n += scene.osm_city->get_joint_roads().size();
			if (!ignore_sidewalks && procedural_sidewalks)
				n += scene.osm_city->get_sidewalks().size();
			if (!ignore_street_lamps)
			{
				n += scene.osm_city->get_street_lamps().size();
				n += scene.osm_city->get_street_lamps_and_colliders().size();
			}
			if (!ignore_terrain && procedural_terrain)
				n += 1;

			return n;
		}

		int get_number_of_objects_with_normal_geometry()
		{
			int n = 0;

			if (!ignore_buildings && !procedural_buildings)
				n += scene.osm_city->get_buildings().size();
			if (!ignore_edge_roads && !procedural_edge_roads)
				n += scene.osm_city->get_edge_roads().size();
			if (!ignore_joint_roads && !procedural_joint_roads)
				n += scene.osm_city->get_joint_roads().size();
			if (!ignore_sidewalks && !procedural_sidewalks)
				n += scene.osm_city->get_sidewalks().size();
			if (!ignore_terrain && !procedural_terrain)
				n += 1;

			return n;
		}

		// For the window.
		// Callbacks must be static methods.
		static void window_size_callback(GLFWwindow* window, int32_t res_x, int32_t res_y);
		static void window_iconify_callback(GLFWwindow* window, int32_t iconified);
		static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
		static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos);
		static void key_callback(GLFWwindow* window, int32_t key, int32_t scancode, int32_t action, int32_t mods);
		static void scroll_callback(GLFWwindow* window, double xscroll, double yscroll);

		static void context_log_cb(unsigned int level, const char* tag, const char* message, void* /*cbdata */)
		{
			std::cerr << "[" << std::setw(2) << level << "][" << std::setw(12) << tag << "]: "
				<< message << "\n";
		}

	public:
		~Program()
		{
			// The function cleanup() is called at the end of the function run(). 
			// So, there is nothing to clean.
		}

		/// <summary>
		/// If the instance doesn't exist, it creates one.
		/// </summary>
		/// <returns>true - no instance had existed and one was created; false - and instance exists.</returns>
		static bool initialize()
		{
			if (instance == nullptr)
			{
				instance = new Program();

				return true;
			}
			else
			{
				return false;
			}
		}

		static Program* get_instance()
		{
			return instance;
		}

		void run();
	};
}

#endif