/**
 * Extended OptixAabb that has more functions that simplify various things.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <optix.h>
#include <vector>
#include <sutil/vec_math.h>
#include "math_utils.h"

namespace drpm
{
	namespace OptixAabb_ext
	{
		__forceinline__ __device__ __host__ static float3 find_min(std::vector<float3> corners)
		{
			float3 min = make_float3(1e16, 1e16, 1e16);

			for (float3 corner : corners)
			{
				min.x = fminf(corner.x, min.x);
				min.y = fminf(corner.y, min.y);
				min.z = fminf(corner.z, min.z);
			}

			return min;
		}

		__forceinline__ __device__ __host__ static float3 find_max(std::vector<float3> corners)
		{
			float3 max = make_float3(-1e16, -1e16, -1e16);

			for (float3 corner : corners)
			{
				max.x = fmaxf(corner.x, max.x);
				max.y = fmaxf(corner.y, max.y);
				max.z = fmaxf(corner.z, max.z);
			}

			return max;
		}

		__forceinline__ __device__ __host__ OptixAabb* create_aabb(float3 corner, float3 up, float3 left, float3 forward)
		{
			float3 a, b, c, d, e, f, g, h;

			a = corner;
			b = a + forward;
			c = a + left + forward;
			d = a + left;

			e = a + up;
			f = e + forward;
			g = e + left + forward;
			h = e + left;

			std::vector<float3> corners = { a, b, c, d, e, f, g, h };

			float3 min = find_min(corners);
			float3 max = find_max(corners);

			OptixAabb* aabb = new OptixAabb();
			aabb->minX = min.x;
			aabb->minY = min.y;
			aabb->minZ = min.z;
			aabb->maxX = max.x;
			aabb->maxY = max.y;
			aabb->maxZ = max.z;

			return aabb;
		}

		__forceinline__ __device__ __host__ void copy(OptixAabb& dst, const OptixAabb& src)
		{
			dst.minX = src.minX;
			dst.minY = src.minY;
			dst.minZ = src.minZ;
			dst.maxX = src.maxX;
			dst.maxY = src.maxY;
			dst.maxZ = src.maxZ;
		}

		__forceinline__ __device__ __host__ OptixAabb* create_aabb(const OptixAabb& other)
		{
			OptixAabb* new_aabb = new OptixAabb();

			copy(*new_aabb, other);

			return new_aabb;
		}		

		/// <summary>
		/// [0] - foreground
		/// [1] - background
		/// [2] - left
		/// [3] - right
		/// [4] - top
		/// [5] - bottom
		/// </summary>
		/// <param name="aabb"></param>
		/// <param name="quadrangles"></param>
		/// <returns></returns>
		__forceinline__ __device__ __host__ void to_quadrangles(const OptixAabb aabb, Quadrangle quadrangles[6])
		{
			float x_size = aabb.maxX - aabb.minX;
			float y_size = aabb.maxY - aabb.minY;
			float z_size = aabb.maxZ - aabb.minZ;

			Quadrangle q;

			q.a = { aabb.minX, aabb.minY, aabb.minZ };
			q.b = q.a + vector3::left() * x_size;
			q.c = q.b + vector3::up() * y_size;
			q.d = q.a + vector3::up() * y_size;

			quadrangles[0] = q;
			quadrangles[1] = q.get_opposite(z_size);
			quadrangles[2] = q.get_left(z_size);
			quadrangles[3] = q.get_right(z_size);
			quadrangles[4] = q.get_top(z_size);
			quadrangles[5] = q.get_bottom(z_size);
		}
	};
}

