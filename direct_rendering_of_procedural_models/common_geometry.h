/**
 * Function that are used for the procedural generation
 * on demand and for the procedural generation to store it
 * in the memory.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <optix.h>

#include "rendering.h"
#include "math_utils.h"

namespace drpm
{
	__forceinline__ __device__ Quadrangle get_quadrangle_in_slot(
		const float3& horizontal_dir,
		const float3& vertical_dir,
		const float3& slot_start_point,
		const float quad_x, const float quad_y,
		const float gap_from_start
	)
	{
		Quadrangle window_quadrangle;
		window_quadrangle.a = slot_start_point + horizontal_dir * gap_from_start;
		window_quadrangle.b = window_quadrangle.a + horizontal_dir * quad_x;
		window_quadrangle.c = window_quadrangle.b + vertical_dir * quad_y;
		window_quadrangle.d = window_quadrangle.a + vertical_dir * quad_y;
		return window_quadrangle;
	}

	__forceinline__ __device__ float3 get_facade_slot_start_point(
		const float3& horizontal_dir,
		const float3& start_point_at_floor,
		const float slot_size,
		const float edge_gap,
		const float i
	)
	{
		return start_point_at_floor + horizontal_dir * (slot_size * i + edge_gap);
	}

	__forceinline__ __device__ Quadrangle get_wall_part_for_floor(
		const float3& vertical_dir,
		const float3& wall_left,
		const float3& wall_right,
		const float floor_height
	)
	{
		return Quadrangle::create_from_bottom_vertices(
			wall_left, 
			wall_right, 
			vertical_dir, 
			floor_height
		);
	}

	__forceinline__ __device__ Quadrangle get_slot_quadrangle(
		const float3& horizontal_dir,
		const float3& vertical_dir,
		const float3& slot_start_point,
		const float width,
		const float height
	)
	{
		Quadrangle ret;
		ret.a = slot_start_point;
		ret.b = ret.a + horizontal_dir * width;
		ret.c = ret.b + vertical_dir * height;
		ret.d = ret.a + vertical_dir * height;
		return ret;
	}

	__forceinline__ __device__ void get_simple_window_slot_rest_quadrangles(
		const Quadrangle& slot_quadrangle,
		const Quadrangle& window_quadrangle,
		Quadrangle rest_quadrangles[4]
	)
	{

		rest_quadrangles[0].a = slot_quadrangle.a;
		rest_quadrangles[0].b = slot_quadrangle.b;
		rest_quadrangles[0].c = window_quadrangle.b;
		rest_quadrangles[0].d = window_quadrangle.a;

		rest_quadrangles[1].a = slot_quadrangle.a;
		rest_quadrangles[1].b = window_quadrangle.a;
		rest_quadrangles[1].c = window_quadrangle.d;
		rest_quadrangles[1].d = slot_quadrangle.d;

		rest_quadrangles[2].a = window_quadrangle.d;
		rest_quadrangles[2].b = window_quadrangle.c;
		rest_quadrangles[2].c = slot_quadrangle.c;
		rest_quadrangles[2].d = slot_quadrangle.d;

		rest_quadrangles[3].a = window_quadrangle.b;
		rest_quadrangles[3].b = slot_quadrangle.b;
		rest_quadrangles[3].c = slot_quadrangle.c;
		rest_quadrangles[3].d = window_quadrangle.c;
	}

	__forceinline__ __device__ void get_window_2_balconies_window_slot_rest_quadrangles(
		const Quadrangle& slot_quadrangle,
		const Quadrangle window_quadrangles[2],
		const Quadrangle balcony_quadrangles[2],
		Quadrangle* rest_quadrangles
	)
	{

		rest_quadrangles[0].a = slot_quadrangle.a;
		rest_quadrangles[0].b = window_quadrangles[0].a;
		rest_quadrangles[0].c = window_quadrangles[0].d;
		rest_quadrangles[0].d = slot_quadrangle.d;

		rest_quadrangles[1].a = window_quadrangles[0].b;
		rest_quadrangles[1].b = balcony_quadrangles[0].a;
		rest_quadrangles[1].c = balcony_quadrangles[0].d;
		rest_quadrangles[1].d = window_quadrangles[0].c;

		rest_quadrangles[2].a = balcony_quadrangles[0].b;
		rest_quadrangles[2].b = balcony_quadrangles[1].a;
		rest_quadrangles[2].c = balcony_quadrangles[1].d;
		rest_quadrangles[2].d = balcony_quadrangles[0].c;

		rest_quadrangles[3].a = balcony_quadrangles[1].b;
		rest_quadrangles[3].b = window_quadrangles[1].a;
		rest_quadrangles[3].c = window_quadrangles[1].d;
		rest_quadrangles[3].d = balcony_quadrangles[1].c;

		rest_quadrangles[4].a = window_quadrangles[1].b;
		rest_quadrangles[4].b = slot_quadrangle.b;
		rest_quadrangles[4].c = slot_quadrangle.c;
		rest_quadrangles[4].d = window_quadrangles[1].c;

		rest_quadrangles[5].a = window_quadrangles[0].d;
		rest_quadrangles[5].b = window_quadrangles[0].c;
		rest_quadrangles[5].c = balcony_quadrangles[0].d;
		rest_quadrangles[5].d = slot_quadrangle.d;

		rest_quadrangles[6].a = balcony_quadrangles[0].d;
		rest_quadrangles[6].b = balcony_quadrangles[1].c;
		rest_quadrangles[6].c = slot_quadrangle.c;
		rest_quadrangles[6].d = slot_quadrangle.d;

		rest_quadrangles[7].a = window_quadrangles[1].d;
		rest_quadrangles[7].b = window_quadrangles[1].c;
		rest_quadrangles[7].c = slot_quadrangle.c;
		rest_quadrangles[7].d = balcony_quadrangles[1].c;





		rest_quadrangles[8].a = slot_quadrangle.a;
		rest_quadrangles[8].b = balcony_quadrangles[0].a;
		rest_quadrangles[8].c = window_quadrangles[0].b;
		rest_quadrangles[8].d = window_quadrangles[0].a;

		rest_quadrangles[9].a = slot_quadrangle.a;
		rest_quadrangles[9].b = slot_quadrangle.b;
		rest_quadrangles[9].c = balcony_quadrangles[1].b;
		rest_quadrangles[9].d = balcony_quadrangles[0].a;

		rest_quadrangles[10].a = balcony_quadrangles[1].b;
		rest_quadrangles[10].b = slot_quadrangle.b;
		rest_quadrangles[10].c = window_quadrangles[1].b;
		rest_quadrangles[10].d = window_quadrangles[1].a;
	}

	__forceinline__ __device__ int get_wall_top_and_bottom_quadrangles(
		const Quadrangle& wall,
		const float bottom_gap,
		const float top_gap,
		Quadrangle rest_quadrangles[2]
	)
	{
		int cur_n = 0;

		if (bottom_gap > 0.0f)
		{
			rest_quadrangles[cur_n].a = wall.a;
			rest_quadrangles[cur_n].b = wall.b;
			rest_quadrangles[cur_n].c = rest_quadrangles[cur_n].b + wall.get_vertical_direction() * bottom_gap;
			rest_quadrangles[cur_n].d = rest_quadrangles[cur_n].a + wall.get_vertical_direction() * bottom_gap;
			cur_n++;
		}

		if (top_gap > 0.0f)
		{
			rest_quadrangles[cur_n].d = wall.d;
			rest_quadrangles[cur_n].c = wall.c;
			rest_quadrangles[cur_n].b = rest_quadrangles[cur_n].c - wall.get_vertical_direction() * top_gap;
			rest_quadrangles[cur_n].a = rest_quadrangles[cur_n].d - wall.get_vertical_direction() * top_gap;
			cur_n++;
		}

		return cur_n;
	}

	__forceinline__ __device__ void get_floor_edge_quadrangles(const Quadrangle& wall, const float floor_y, const float floor_height, const float edge_gap, Quadrangle edge_quadrangles[2])
	{
		const float3 floor_a = make_float3(wall.a.x, floor_y, wall.a.z);
		edge_quadrangles[0].a = floor_a;
		edge_quadrangles[0].b = edge_quadrangles[0].a + wall.get_horizontal_direction() * edge_gap;
		edge_quadrangles[0].c = edge_quadrangles[0].b + wall.get_vertical_direction() * floor_height;
		edge_quadrangles[0].d = edge_quadrangles[0].a + wall.get_vertical_direction() * floor_height;

		const float3 floor_b = make_float3(wall.b.x, floor_y, wall.b.z);
		edge_quadrangles[1].b = floor_b;
		edge_quadrangles[1].a = edge_quadrangles[1].b - wall.get_horizontal_direction() * edge_gap;
		edge_quadrangles[1].c = edge_quadrangles[1].b + wall.get_vertical_direction() * floor_height;
		edge_quadrangles[1].d = edge_quadrangles[1].a + wall.get_vertical_direction() * floor_height;
	}

	__forceinline__ __device__ void get_quadrangles_for_window_glass_and_imposts(
		const Quadrangle& glass_initial_quadrangle,
		const float2& impost_position,
		const float impost_width,
		const float impost_thickness,
		Quadrangle quadrangles[16]
	)
	{
		//printf("%f\n", impost_width);
		Quadrangle horizontal_impost;
		horizontal_impost.a
			= glass_initial_quadrangle.a + glass_initial_quadrangle.get_vertical_direction() * glass_initial_quadrangle.get_height() * impost_position.y 
			- glass_initial_quadrangle.get_vertical_direction() * impost_width / 2.f 
			+ glass_initial_quadrangle.get_normal() * impost_thickness / 2.f;
		horizontal_impost.b = horizontal_impost.a + glass_initial_quadrangle.get_horizontal_direction() * glass_initial_quadrangle.get_width();
		horizontal_impost.c = horizontal_impost.b + glass_initial_quadrangle.get_vertical_direction() * impost_width;
		horizontal_impost.d = horizontal_impost.a + glass_initial_quadrangle.get_vertical_direction() * impost_width;

		float vertical_bottom_impost_h = horizontal_impost.a.y - glass_initial_quadrangle.a.y;
		Quadrangle vertical_bottom_impost;
		vertical_bottom_impost.a
			= horizontal_impost.a - glass_initial_quadrangle.get_vertical_direction() * vertical_bottom_impost_h
			+ glass_initial_quadrangle.get_horizontal_direction() * glass_initial_quadrangle.get_width() * impost_position.x;
		vertical_bottom_impost.b = vertical_bottom_impost.a + glass_initial_quadrangle.get_horizontal_direction() * impost_width;
		vertical_bottom_impost.c = vertical_bottom_impost.b + glass_initial_quadrangle.get_vertical_direction() * vertical_bottom_impost_h;
		vertical_bottom_impost.d = vertical_bottom_impost.a + glass_initial_quadrangle.get_vertical_direction() * vertical_bottom_impost_h;

		float vertical_top_impost_h = glass_initial_quadrangle.d.y - horizontal_impost.d.y;
		Quadrangle vertical_top_impost;
		vertical_top_impost.a = horizontal_impost.d + glass_initial_quadrangle.get_horizontal_direction() * glass_initial_quadrangle.get_width() * impost_position.x;
		vertical_top_impost.b = vertical_top_impost.a + glass_initial_quadrangle.get_horizontal_direction() * impost_width;
		vertical_top_impost.c = vertical_top_impost.b + glass_initial_quadrangle.get_vertical_direction() * vertical_top_impost_h;
		vertical_top_impost.d = vertical_top_impost.a + glass_initial_quadrangle.get_vertical_direction() * vertical_top_impost_h;

		Quadrangle glass_top_left;
		glass_top_left.a = horizontal_impost.d;
		glass_top_left.b = vertical_top_impost.a;
		glass_top_left.c = vertical_top_impost.d;
		glass_top_left.d = glass_initial_quadrangle.d + glass_initial_quadrangle.get_normal() * impost_thickness / 2.f;

		Quadrangle glass_top_right;
		glass_top_right.a = vertical_top_impost.b;
		glass_top_right.b = horizontal_impost.c;
		glass_top_right.c = glass_initial_quadrangle.c + glass_initial_quadrangle.get_normal() * impost_thickness / 2.f;
		glass_top_right.d = vertical_top_impost.c;

		Quadrangle glass_bottom_left;
		glass_bottom_left.a = glass_initial_quadrangle.a + glass_initial_quadrangle.get_normal() * impost_thickness / 2.f;
		glass_bottom_left.b = vertical_bottom_impost.a;
		glass_bottom_left.c = vertical_bottom_impost.d;
		glass_bottom_left.d = horizontal_impost.a;

		Quadrangle glass_bottom_right;
		glass_bottom_right.a = vertical_bottom_impost.b;
		glass_bottom_right.b = glass_initial_quadrangle.b + glass_initial_quadrangle.get_normal() * impost_thickness / 2.f;
		glass_bottom_right.c = horizontal_impost.b;
		glass_bottom_right.d = vertical_bottom_impost.c;


		quadrangles[0] = glass_top_left;
		quadrangles[1] = glass_top_right;
		quadrangles[2] = glass_bottom_left;
		quadrangles[3] = glass_bottom_right;

		quadrangles[4] = horizontal_impost;
		quadrangles[5] = horizontal_impost.get_opposite(impost_thickness);
		quadrangles[6] = horizontal_impost.get_top(impost_thickness);
		quadrangles[7] = horizontal_impost.get_bottom(impost_thickness);

		quadrangles[8] = vertical_bottom_impost;
		quadrangles[9] = vertical_bottom_impost.get_opposite(impost_thickness);
		quadrangles[10] = vertical_bottom_impost.get_left(impost_thickness);
		quadrangles[11] = vertical_bottom_impost.get_right(impost_thickness);

		quadrangles[12] = vertical_top_impost;
		quadrangles[13] = vertical_top_impost.get_opposite(impost_thickness);
		quadrangles[14] = vertical_top_impost.get_left(impost_thickness);
		quadrangles[15] = vertical_top_impost.get_right(impost_thickness);

		//printf("%f %f\n", horizontal_impost.get_height(), horizontal_impost.get_width());
	}
}