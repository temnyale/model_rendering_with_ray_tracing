/**
 * Settings for the geometry creation.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#include "geometry_settings.h"

namespace drpm
{
	float building_min_height = 20.0f;
	float building_max_height = 100.0f;

	float street_lamp_collider_size = 0.9f;

	bool ignore_terrain = false;
	bool ignore_buildings = false;
	bool ignore_edge_roads = false;
	bool ignore_joint_roads = false;
	bool ignore_sidewalks = false;
	bool ignore_street_lamps = false;

	bool procedural_terrain = false;
	bool procedural_buildings = false;
	bool procedural_edge_roads = false;
	bool procedural_joint_roads = false;
	bool procedural_sidewalks = false;
}