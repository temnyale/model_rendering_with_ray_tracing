/**
 * Settings for the geometry creation.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <vector_types.h>

#include "FacadeSlotParameters.h"

namespace drpm
{
	const float scene_block_size = 100.0f;
	const float empty_space_at_scene_block_edge = 6.0f;
	const float camera_height = 200.0f;
	const float terrain_height = 20.0f;
	const float terrain_extension_x = 00.0f;
	const float terrain_extension_z = 00.0f;
	// 0 to 100.
	const float box_probability = 100.0f;

	const float regular_building_bottom_gap_min = 0.1f;
	const float regular_building_bottom_gap_max = 0.3f;
	const float regular_building_top_gap_min = 1.5f;
	const float regular_building_top_gap_max = 1.8f;
	const float regular_building_edge_gap_min = 1.0;
	const float regular_building_edge_gap_max = 2.0;
	// Window sizes.
	const float regular_building_window_u_min = 1.2192f;
	const float regular_building_window_u_max = 1.5193f;
	const float regular_building_window_v_min = 1.2192f;
	const float regular_building_window_v_max = 1.5193f;
	const int number_of_facade_slot_initial_parameters = 2;
	const SimpleWindowSlotParameters simple_window_initial_parameters[number_of_facade_slot_initial_parameters] =
	{
		//     sizes
		//		 |  outside intrusion
		//		 |			|
		//		 |			| inside intrusion
		//		 |			|	  | 
		//		 |		    |	  |                               gap between windows
		//		 |			|	  |	                                      |	
		//		 |			|	  |		                                  | edge gap
		//		 |			|	  |		                                  |	   | 
		{ { { 1.8f, 1.6f }, 0.2f, 0.5f, true, { 0.7, 0.65 }, 0.08, 0.1 }, 2.0f, 0.8f },
		{ { { 1.5f, 1.5f }, 0.1f, 0.5f, true, { 0.7, 0.65 }, 0.08, 0.1 }, 1.8f, 0.8f }
	};
	const float simple_window_initial_floor_heights[number_of_facade_slot_initial_parameters] =
	{
		2.8f, 2.8f
	};
	const int number_of_shop_window_initial_parameters = 1;
	const SimpleWindowSlotParameters shop_window_initial_parameters[number_of_shop_window_initial_parameters] =
	{
		//     sizes
		//		 |  outside intrusion
		//		 |			|
		//		 |			| inside intrusion
		//		 |			|	  |     imposts
		//		 |		    |	  |       | gap between windows
		//		 |			|	  |	      |        |	
		//		 |			|	  |		  |        | edge gap
		//		 |			|	  |		  |        |	|      
		{ { { 5.0f, 2.5f }, 0.2f, 0.5f, false }, 0.5f, 1.0f }
	};
	const float shop_window_initial_floor_heights[number_of_shop_window_initial_parameters] =
	{
		2.8f
	};
	const int number_of_window_2_balconies_window_initial_parameters = 2;
	const Window2BalconiesWindowSlotParameters window_2_balconies_window_initial_parameters[number_of_window_2_balconies_window_initial_parameters] =
	{
		//   window sizes
		//		 |  window inside intrusion
		//		 |			 |
		//		 |			 | window outside intrusion
		//		 |			 |	   | 
		//		 |		     |	   |								gap between windows
		//		 |			 |	   |									   |	
		//		 |			 |	   |									   |edge gap
		//		 |			 |	   |									   |    |      balcony sizes
		//		 |			 |	   |									   |	|            |           balcony wall sizes
		//       |           |     |								       |    |            |               |    balcony intrusion
		//       |           |     |								       |    |            |               |        |gap between window and balcony
		//       |           |     |								       |    |            |               |        |	      |gap between window and balcony
		//       |           |     |								       |    |            |               |        |	      |     |     
		{ { { 1.5f, 1.3f }, 0.2f, 0.5f, true, { 0.7, 0.65 }, 0.08, 0.1 }, 2.0f, 0.8f, { { 2.5f, 2.5f }, 1.0f, 0.2f, 2.0f }, 0.5f, 0.5f },
		{ { { 1.5f, 1.5f }, 0.2f, 0.5f, true, { 0.65, 0.65 }, 0.08, 0.1 }, 2.0f, 0.8f, { { 2.5f, 2.5f }, 1.0f, 0.2f, 2.0f }, 0.5f, 0.5f }
	};
	const float window_2_balconies_window_initial_floor_heights[number_of_window_2_balconies_window_initial_parameters] =
	{
		2.8f, 2.8f
	};
	const float regular_building_shop_window_v_max = 2.6f;
	const float regular_building_shop_window_v_min = 2.5f;
	const float regular_building_shop_window_u_min = 5.0f;
	const float regular_building_shop_window_u_max = 5.0f;
	// Empty space around window.
	const float regular_building_gap_around_window_min = 1.0f;
	const float regular_building_gap_around_window_max = 3.5f;
	const float regular_building_floor_height_min = 2.7432f;
	const float regular_building_floor_height_max = 3.0f;
	const float window_glass_separator_width = 0.1f;
	const float shop_probability = 0.5f;

	const float road_width = 10.0f;
	const float road_height = 0.1f;
	const float min_road_width_for_markings = 2.5f;	

	const float sidewalk_height = 0.35f;

	const float lamp_glass_sphere_diameter = 0.7f;
	const float lamp_bulb_diameter = 0.25f;
	const float lamp_pillar_diameter = 0.25f;
	const float lamp_height = 5.0f;
	const float gap_between_road_edge_and_lamp = .5f;
	const float lamp_emissive_part_size = 0.5f;
	const float gap_around_lamp = 15.0f;

	// Scene parameters sizes.
	extern float building_min_height;
	extern float building_max_height;

	extern float street_lamp_collider_size;

	extern bool ignore_terrain;
	extern bool ignore_buildings;
	extern bool ignore_edge_roads;
	extern bool ignore_joint_roads;
	extern bool ignore_sidewalks;
	extern bool ignore_street_lamps;

	extern bool procedural_terrain;
	extern bool procedural_buildings;
	extern bool procedural_edge_roads;
	extern bool procedural_joint_roads;
	extern bool procedural_sidewalks;
}