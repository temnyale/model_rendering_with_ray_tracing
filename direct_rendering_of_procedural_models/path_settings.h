/**
 * File that contains the paths to files.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <iostream>
#include <vector>

namespace drpm
{
	const std::string resources_folder = "drpm_res";
	const std::string skydomes_folder = "Skydomes";
	const std::string textures_folder = "Textures";
	const std::string cities_folder = "Cities";

	// REFERENCES TO THE SOURCES OF ALL THE FILES ARE IN THE FOLDERS WITH THEM.
	const std::vector<std::string> paths_to_skydomes
	{
		"AmbienceExposure4k.hdr", "sunset.hdr", "daytime.hdr",
		"CoriolisNight4k.hdr"
	};

	extern std::string path_to_city;
	extern std::string path_to_heightmap;
}