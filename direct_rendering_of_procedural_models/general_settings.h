/**
 * General settings for the application, such as the seed, the window size, etc..
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#ifndef __SETTINGS_H__
#define __SETTINGS_H__

#ifndef __CUDACC__
#include "sutil/CUDAOutputBuffer.h"
#endif

namespace drpm
{	
	const int max_recursion_depth = 5;

	extern unsigned int global_seed;

	extern bool print_memory_info;
	extern bool print_scene_info;
	extern bool print_scene_geometry_info;


#ifndef __CUDACC__
	extern sutil::CUDAOutputBufferType cuda_output_buffer_type;
#endif

	// Window sizes.
	extern int application_window_width;
	extern int application_window_height;
}

#endif