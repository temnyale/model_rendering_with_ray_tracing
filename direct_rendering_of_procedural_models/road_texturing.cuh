/**
 * Functions for the road texturing.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <optix.h>
#include <optix_device.h>
#include <cuda/helpers.h>

#include "rendering.h"
#include "math_utils.h"

namespace drpm
{
	extern "C" __constant__ Parameters OPTIX_PARAMETERS_VAR;

	static __forceinline__ __device__ bool sample_marking_line(
		const float3& P,
		MarkingLineParameters marking_line_parameters,
		const float3& start_border,
		const float3& direction
	)
	{
		if (distance_between_line_and_point(start_border, direction, P) <= marking_line_parameters.line_width / 2.f)
		{
			for (int i = 0; i < marking_line_parameters.number_of_lines; i++)
			{
				float3 current_slot_start_border = start_border + direction * marking_line_parameters.get_slot() * i;
				float3 current_line_start_border = current_slot_start_border + direction * marking_line_parameters.gap_between_lines / 2.f;
				float3 current_line_end_border = current_line_start_border + direction * marking_line_parameters.line_length;

				if (is_point_between_planes(current_line_start_border, direction, current_line_end_border, -direction, P))
				{
					return true;
				}
			}
		}

		return false;
	}

	static __forceinline__ __device__ void sample_road_markings(const float3& P, HitGroupData* hit_data, const int prim_idx, float3& diffuse_color)
	{
		float3 A_3d, B_3d, C_3d, D_3d;
		float3 left;

		switch (hit_data->geometry_type)
		{
			case GeometryType::gt_PROCEDURAL:
			{
				float2 A_2d = hit_data->procedural_geometry_hit_group_data.geometry.polygon.contour_nodes[0];
				float2 B_2d = hit_data->procedural_geometry_hit_group_data.geometry.polygon.contour_nodes[1];
				float2 BA_2d = B_2d - A_2d;
				A_3d = make_float3(A_2d.x, hit_data->aabb.maxY, A_2d.y);
				B_3d = make_float3(B_2d.x, hit_data->aabb.maxY, B_2d.y);

				float2 C_2d = hit_data->procedural_geometry_hit_group_data.geometry.polygon.contour_nodes[2];
				float2 D_2d = hit_data->procedural_geometry_hit_group_data.geometry.polygon.contour_nodes[3];
				float2 CD_2d = C_2d - D_2d;
				C_3d = make_float3(C_2d.x, hit_data->aabb.maxY, C_2d.y);
				D_3d = make_float3(D_2d.x, hit_data->aabb.maxY, D_2d.y);

				left = normalize(make_float3(-BA_2d.y, .0f, BA_2d.x));

				/*diffuse_color = make_float3(0.5f);
				if (length(P - q.a) < 2.f || length(P - q.c) < 2.f)
				{
					diffuse_color = vector3::one();
				}
				else if (length(P - q.b) < 2.f || length(P - q.d) < 2.f)
				{
					diffuse_color = vector3::zero();
				}
				return;*/
			} break;

			case GeometryType::gt_TRADITIONAL:
			{
				Quadrangle q = get_quadrangle_from_vertex_array(hit_data, prim_idx);
				A_3d = q.a;
				B_3d = q.b;
				C_3d = q.c;
				D_3d = q.d;

				float2 BA_2d = normalize(float3_to_float2(B_3d - A_3d));
				left = normalize(make_float3(-BA_2d.y, .0f, BA_2d.x));				
			} break;

			default:
			{
				printf("Incorrect geometry type.\n");
			} break;
		}		

		float3 BA_3d = B_3d - A_3d;
		float3 AD_3d = A_3d - D_3d;
		float3 CD_3d = C_3d - D_3d;
		float road_width = get_projection(AD_3d, left);

		float3 Ms_3d = (A_3d + D_3d) / 2.f;
		float3 Me_3d = (B_3d + C_3d) / 2.f;

		float3 forward = normalize(Me_3d - Ms_3d);

		float3 closest_point_at_start;
		bool A_is_closest_at_start = false;
		{
			float3 D_Ms_3d_normalized = normalize(D_3d - Ms_3d);

			if (dot(forward, D_Ms_3d_normalized) > .0f)
			{
				closest_point_at_start = A_3d;
				A_is_closest_at_start = true;
			}
			else
			{
				closest_point_at_start = D_3d;
			}
		}

		float3 closest_point_at_end;
		{
			float3 B_Me_3d_normalized = normalize(B_3d - Me_3d);

			if (dot(forward, B_Me_3d_normalized) < .0f)
			{
				closest_point_at_end = B_3d;
			}
			else
			{
				closest_point_at_end = C_3d;
			}
		}

		float road_length = length(closest_point_at_end - closest_point_at_start);

		RoadMarkingParameters road_marking_parameters = hit_data->procedural_geometry_hit_group_data.miscellaneous.road_marking_properties;

		if (!road_marking_parameters.calculated)
		{
			road_marking_parameters.calculate_road_markings_parameters(road_length, road_width);
		}

		if (road_marking_parameters.central_line_parameters.number_of_lines > 0)
		{
			float3 start_border = closest_point_at_start + forward * road_marking_parameters.end_gap;
			float3 end_border = closest_point_at_end - forward * road_marking_parameters.end_gap;
			if (is_point_between_planes(start_border, forward, end_border, -forward, P))
			{
				if (distance_between_line_and_point(A_3d, BA_3d, P) > road_marking_parameters.side_gap
					&& distance_between_line_and_point(C_3d, CD_3d, P) > road_marking_parameters.side_gap)
				{
					float3 central_line_start_border
						= closest_point_at_start
						+ (A_is_closest_at_start ? left : -left) * road_width / 2.f
						+ forward * road_marking_parameters.end_gap;

					bool central_line_sampled = sample_marking_line(P, road_marking_parameters.central_line_parameters, central_line_start_border, forward);

					if (central_line_sampled)
					{
						diffuse_color = make_float3(1.f, 1.f, 0.f);
					}
					else
					{
						MarkingLineParameters lane_separator_parameters = road_marking_parameters.lane_separator_parameters;

						for (int i = 0; i < road_marking_parameters.number_of_lanes - 1; i++)
						{
							if (i == (road_marking_parameters.number_of_lanes / 2) - 1)
								continue;

							float3 current_line_start_border
								= closest_point_at_start
								+ (A_is_closest_at_start ? left : -left) * road_marking_parameters.lane_width * (i + 1)
								+ forward * road_marking_parameters.end_gap;

							bool line_sampled = sample_marking_line(P, lane_separator_parameters, current_line_start_border, forward);

							if (line_sampled)
							{
								diffuse_color = make_float3(1.f, 1.f, 1.f);
								break;
							}
						}
					}
				}
				else
				{
					diffuse_color = make_float3(1.f, 1.f, 1.f);
				}
			}
		}
	}
}