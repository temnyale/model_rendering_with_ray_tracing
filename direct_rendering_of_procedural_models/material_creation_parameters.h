/**
 * Data to create materials for the rendering.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <vector>
#include <string>
#include <vector_types.h>

namespace drpm
{
	struct TexturedDiffuseMaterialCreationParameters
	{
		float k_d;
		float k_s;
		float n_s;

		std::string path_to_texture;

		// Parameters for tiling.
		// Look at the report to learn more about it.
		float t_h;
		float t_v;
	};

	struct NonTexturedDiffuseMaterialCreationParameters
	{
		float k_d;
		float k_s;
		float n_s;

		float3 diffuse_color;
		float3 emissive_color;
	};

	struct NonTexturedGlassMaterialCreationParameters
	{
		float refractive_index;
		float fresnel_exponent;
		float fresnel_minimum;
		float fresnel_maximum;

		float3 diffuse_color;
		float3 emissive_color;
	};

	const std::vector<TexturedDiffuseMaterialCreationParameters> regular_building_material_creation_parameters
	{
		{ 1.0f, 0.0f, 0.0f, "TexturesCom_BrickLargeBlocks0026_1_seamless_S.png", 1.0f, 1.0f },
		{ 1.0f, 0.0f, 0.0f, "TexturesCom_BrickLargeBlocks0024_1_seamless_S.png", 2.0f, 2.0f },
		{ 1.0f, 0.0f, 0.0f, "TexturesCom_ConcreteNew0047_1_seamless_S.png", 2.0f, 2.0f },
		{ 1.0f, 0.0f, 0.0f, "TexturesCom_BrickSmallDark0053_1_seamless_S.png", 3.5f, 3.5f },
		{ 1.0f, 0.0f, 0.0f, "TexturesCom_BrickSmallDark0026_1_seamless_S.png", 5.0f, 5.0f }
	};

	const NonTexturedDiffuseMaterialCreationParameters road_asphalt_material_creation_parameters
	{
		0.6f, 0.4f, 30.0f, { 0.1647f, 0.16f, 0.1333f }, { 0.0f }
	};

	const NonTexturedDiffuseMaterialCreationParameters sidewalk_asphalt_material_creation_parameters
	{
		0.6f, 0.4f, 30.0f, { 0.2447f, 0.24f, 0.2233f }, { 0.0f }
	};

	const NonTexturedDiffuseMaterialCreationParameters balcony_fence_material_creation_parameters
	{
		0.2f, 0.8f, 50.0f, { 0.5f, 0.5f, 0.5f }, { 0.0f }
	};

	const NonTexturedDiffuseMaterialCreationParameters window_impost_material_creation_parameters
	{
		0.5f, 0.4f, 200.0f, { 1.0f, 1.0f, 1.0f }, { 0.0f }
	};

	const NonTexturedDiffuseMaterialCreationParameters street_lamp_bulb_material_creation_parameters
	{
		0.2f, 0.8f, 200.0f, { 1.0f, 1.0f, 1.0f }, { 0.0f }
	};

	const std::vector<NonTexturedDiffuseMaterialCreationParameters> inside_room_material_creation_parameters
	{
		{ 0.8f, 0.2f, 10.0f, { 0.8f, 0.8f, 0.8f }, { 1.f, 1.f, 1.f } },
		{ 0.8f, 0.2f, 10.0f, { 0.8f, 0.8f, 0.8f }, { 0.953125f, 0.828125f, 0.66796875f } },
		{ 0.8f, 0.2f, 10.0f, { 0.8f, 0.8f, 0.8f }, { 0.95703125f, 0.828125f, 0.51171875f } },

		{ 1.0f, 0.0f, 10.0f, { 0.55859f, 0.734375f, 0.55859f }, { 1.f, 1.f, 1.f } },
		{ 1.0f, 0.0f, 10.0f, { 0.55859f, 0.734375f, 0.55859f }, { 0.953125f, 0.828125f, 0.66796875f } },
		{ 1.0f, 0.0f, 10.0f, { 0.55859f, 0.734375f, 0.55859f }, { 0.95703125f, 0.828125f, 0.51171875f } },
	};

	const TexturedDiffuseMaterialCreationParameters grass_material_creation_parameters
	{
		1.0f, 0.0f, 0.0f, "TexturesCom_Grass0026_1_seamless_S.png", 1.0f, 1.0f
	};

	const NonTexturedDiffuseMaterialCreationParameters lamp_material_creation_parameters
	{
		0.4f, 0.6f, 100.0f,
		{ 0.27450980392156862745098039215686f, 0.27843137254901960784313725490196f, 0.24313725490196078431372549019608f },
		{ 0.0f }
	};

	const std::vector<NonTexturedDiffuseMaterialCreationParameters> skyscraper_material_creation_parameters
	{
		{
			0.2f, 0.8f, 100.0f, { 0.4f, 0.4f, 0.4f }, { 0.0f }
		}
	};

	const NonTexturedGlassMaterialCreationParameters street_lamp_glass_material_creation_parameters
	{
		1.5f,					// refraction index
		3.0f,					// fresnel exponent
		0.1f,					// fresnel minimum
		1.0f,					// fresnel maximum
		{ 1.0f, 1.0f, 1.0f },	// diffuse color
		{ 0.0f, 0.0f, 0.0f }	// emissive color
	};

	const NonTexturedGlassMaterialCreationParameters window_glass_material_creation_parameters
	{
		1.5f,					// refraction index
		5.0f,					// fresnel exponent
		0.1f,					// fresnel minimum
		1.0f,					// fresnel maximum
		{ 1.0f, 1.0f, 1.0f },	// diffuse color
		{ 0.0f, 0.0f, 0.0f }	// emissive color
	};

	const NonTexturedGlassMaterialCreationParameters balcony_glass_material_creation_parameters
	{
		1.5f,					// refraction index
		3.0f,					// fresnel exponent
		0.1f,					// fresnel minimum
		1.0f,					// fresnel maximum
		{ 1.0f, 1.0f, 1.0f },	// diffuse color
		{ 0.0f, 0.0f, 0.0f }	// emissive color
	};
}