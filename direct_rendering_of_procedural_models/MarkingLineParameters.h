/**
 * Parameters for the road marking lines.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include "road_texturing_settings.h"

namespace drpm
{
	enum MarkingLineType
	{
		CENTRAL_LINE = 0,
		LANE_SEPARATOR,

		MARKING_LINE_COUNT
	};

	class MarkingLineParameters
	{
	private:
		__forceinline__ __device__ void calculate_line_parameters_common(float road_length, float road_width)
		{
			number_of_lines = road_length / get_slot();

			if (number_of_lines <= 1)
			{
				gap_between_lines = 0.f;
				line_length = road_length + 1e-4f;
				number_of_lines = road_length / get_slot();
			}
			else
			{
				float remaining_length = road_length - number_of_lines * get_slot();
				line_length += remaining_length / 2.f;
				gap_between_lines += remaining_length / 2.f;
			}
		}

		__forceinline__ __device__ void calculate_central_line_parameters(float road_length, float road_width)
		{
			line_width = drpm::road_central_line_width;
			line_length = drpm::road_central_line_length;
			gap_between_lines = drpm::gap_between_road_central_lines;

			calculate_line_parameters_common(road_length, road_width);
		}

		__forceinline__ __device__ void calculate_lane_separator_parameters(float road_length, float road_width)
		{
			line_width = drpm::road_lane_separator_width;
			line_length = drpm::road_lane_separator_length;
			gap_between_lines = drpm::gap_between_road_lane_separators;

			calculate_line_parameters_common(road_length, road_width);
		}

	public:
		bool calculated;

		float line_width;
		float line_length;
		float gap_between_lines;
		int number_of_lines;

		__forceinline__ __device__ float get_slot()
		{
			return line_length + gap_between_lines;
		}

		__forceinline__ __device__ void calculate_line_parameters(float road_length, float road_width, MarkingLineType line_type)
		{
			number_of_lines = 0;

			switch (line_type)
			{
				case MarkingLineType::CENTRAL_LINE:
				{
					calculate_central_line_parameters(road_length, road_width);
				} break;

				case MarkingLineType::LANE_SEPARATOR:
				{
					calculate_lane_separator_parameters(road_length, road_width);
				} break;
			}

			calculated = true;
		}
	};
}