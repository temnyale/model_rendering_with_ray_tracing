/**
 * Data structures and functions that allows handling the data from a file
 * with the processed OpenStreetMap data.
 * 
 * That is, the data processed by the OSM parser.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#ifndef __OSM_CITY_H__
#define __OSM_CITY_H__

#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <vector_types.h>
#include <optix.h>
#include <limits>

#include "geometry_settings.h"

namespace drpm
{
	class Node
	{
	private:
		long long id;
		float3 location;
		std::unordered_set<Node*> neighbors;

	public:
		Node(long long _id, float3 _location)
		{
			id = _id;
			location = _location;
		}

		~Node()
		{

		}

		long long get_id()
		{
			return id;
		}

		float3 get_location()
		{
			return location;
		}

		std::unordered_set<Node*> get_neighbors()
		{
			return neighbors;
		}

		void add_neighbor(Node* neighbor)
		{
			neighbors.insert(neighbor);
		}
	};

	class Edge
	{

	};

	class Building
	{
	public:
		long long id;
		std::vector<float2> vertices;
		OptixAabb aabb;

		Building(long long _id, float minY, float maxY)
		{
			id = _id;

			aabb.maxX = -1e16;
			aabb.maxY = maxY;
			aabb.maxZ = -1e16;

			aabb.minX = 1e16;
			aabb.minY = minY;
			aabb.minZ = 1e16;
		}

		~Building()
		{

		}

		void calculate_aabb()
		{
			for (auto node : vertices)
			{
				aabb.minX = std::min(node.x, aabb.minX);
				aabb.minZ = std::min(node.y, aabb.minZ);

				aabb.maxX = std::max(node.x, aabb.maxX);
				aabb.maxZ = std::max(node.y, aabb.maxZ);
			}
		}
	};

	class Road
	{
	public:
		std::vector<float2> vertices;
		float height;
		OptixAabb aabb;

		void calculate_aabb()
		{
			for (auto point : vertices)
			{
				aabb.minX = std::min(point.x, aabb.minX);
				aabb.minZ = std::min(point.y, aabb.minZ);

				aabb.maxX = std::max(point.x, aabb.maxX);
				aabb.maxZ = std::max(point.y, aabb.maxZ);
			}
		}

	protected:
		Road(std::vector<float2> _vertices, float max_y, float min_y)
		{
			for (auto v : _vertices)
			{
				vertices.push_back(v);
			}

			height = max_y - min_y;

			aabb.maxX = -1e16;
			aabb.maxY = max_y;
			aabb.maxZ = -1e16;

			aabb.minX = 1e16;
			aabb.minY = min_y;
			aabb.minZ = 1e16;

			calculate_aabb();
		}

		~Road()
		{

		}
	};

	class EdgeRoad : public Road
	{
	public:
		std::string type;
		float2 a_outer;
		float2 b_outer;
		float2 c_outer;
		float2 d_outer;

		EdgeRoad(
			std::string type, 
			float2 _a_inner, float2 _b_inner, float2 _c_inner, float2 _d_inner, 
			float2 _a_outer, float2 _b_outer, float2 _c_outer, float2 _d_outer,
			float max_y, float min_y
		) : Road({ _a_inner, _b_inner, _c_inner, _d_inner }, max_y, min_y)
		{
			a_outer = _a_outer;
			b_outer = _b_outer;
			c_outer = _c_outer;
			d_outer = _d_outer;
		}

		~EdgeRoad()
		{

		}

		float2 get_a_inner() 
		{
			return vertices[0];
		}

		float2 get_b_inner()
		{
			return vertices[1];
		}

		float2 get_c_inner()
		{
			return vertices[2];
		}

		float2 get_d_inner()
		{
			return vertices[3];
		}
	};

	class Sidewalk : public Road
	{
	public:
		Sidewalk(std::vector<float2> _vertices, float max_y, float min_y) : Road(_vertices, max_y, min_y)
		{

		}
	};

	class JointRoad : public Road
	{
	public:
		JointRoad(std::vector<float2> _vertices, float max_y, float min_y) : Road(_vertices, max_y, min_y)
		{

		}

		~JointRoad()
		{

		}
	};

	class StreetLamp
	{
	public:
		std::vector<float2> lamp_contour_vertices;
		float lamp_height;
		OptixAabb aabb;

		void calculate_aabb()
		{
			for (auto point : lamp_contour_vertices)
			{
				aabb.minX = std::min(point.x, aabb.minX);
				aabb.minZ = std::min(point.y, aabb.minZ);

				aabb.maxX = std::max(point.x, aabb.maxX);
				aabb.maxZ = std::max(point.y, aabb.maxZ);
			}
		}

		void calculate_collider_aabb(OptixAabb& collider_aabb)
		{
			float center_x = (aabb.maxX + aabb.minX) / 2.f;
			float center_z = (aabb.maxZ + aabb.minZ) / 2.f;

			collider_aabb.minY = aabb.maxY + 0.5f;
			collider_aabb.maxY = collider_aabb.minY + street_lamp_collider_size;
			collider_aabb.maxX = center_x + (street_lamp_collider_size / 2.0f);
			collider_aabb.minX = center_x - (street_lamp_collider_size / 2.0f);
			collider_aabb.maxZ = center_z + (street_lamp_collider_size / 2.0f);
			collider_aabb.minZ = center_z - (street_lamp_collider_size / 2.0f);
		}

		StreetLamp(std::vector<float2> _lamp_contour_vertices, float max_y, float min_y)
		{
			for (auto v : _lamp_contour_vertices)
			{
				lamp_contour_vertices.push_back(v);
			}

			lamp_height = max_y - min_y;

			aabb.maxX = -1e16;
			aabb.maxY = max_y;
			aabb.maxZ = -1e16;

			aabb.minX = 1e16;
			aabb.minY = min_y;
			aabb.minZ = 1e16;

			calculate_aabb();
		}

		~StreetLamp()
		{

		}
	};

	class OSMCity
	{
	private:
		OptixAabb terrain_aabb;
		std::vector<Node*> nodes;
		std::vector<Building*> buildings;
		std::vector<EdgeRoad*> edge_roads;
		std::vector<JointRoad*> joint_roads;
		std::vector<Sidewalk*> sidewalks;
		std::vector<StreetLamp*> street_lamps;
		std::unordered_map<StreetLamp*, OptixAabb*> street_lamps_and_colliders;

		void create_street_lamps();
		void create_block_of_lamps(float2 start_position, float2 lamp_left, float2 lamp_forward, float2 shift, int number, float min_y, float max_y);
		void create_sidewalk_and_lamps(
			const float2 end_right, const float2 end_left, const float2 start_right, const float2 start_left
		);
		float calculate_building_average_height();

	public:
		OSMCity()
		{

		}

		~OSMCity()
		{
			for (auto& o : buildings)
			{
				delete o;
			}

			for (auto& o : nodes)
			{
				delete o;
			}

			for (auto& o : edge_roads)
			{
				delete o;
			}

			for (auto& o : joint_roads)
			{
				delete o;
			}

			for (auto& o : sidewalks)
			{
				delete o;
			}

			for (auto& o : street_lamps)
			{
				if (street_lamps_and_colliders.find(o) != street_lamps_and_colliders.end())
				{
					delete street_lamps_and_colliders[o];
				}
			}

			for (auto& o : street_lamps)
			{
				delete o;
			}
		}

		void read_file(const std::string path_to_file);
		void print_scene_info();

		OptixAabb get_terrain_aabb()
		{
			return terrain_aabb;
		}

		std::vector<Building*> get_buildings()
		{
			return buildings;
		}

		std::vector<EdgeRoad*> get_edge_roads()
		{
			return edge_roads;
		}

		std::vector<JointRoad*> get_joint_roads()
		{
			return joint_roads;
		}

		std::vector<Sidewalk*> get_sidewalks()
		{
			return sidewalks;
		}

		std::vector<StreetLamp*> get_street_lamps()
		{
			return street_lamps;
		}

		std::unordered_map<StreetLamp*, OptixAabb*> get_street_lamps_and_colliders()
		{
			return street_lamps_and_colliders;
		}
	};
}

#endif

