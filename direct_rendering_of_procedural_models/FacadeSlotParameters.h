/**
 * Data structure for the facade slots.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <vector_types.h>
#include <cuda/helpers.h>

namespace drpm
{
	enum FacadeSlotType
	{
		SIMPLE_WINDOWS = 0,
		WINDOW_BALCONY_BALCONY_WINDOW,

		WINDOW_TYPE_COUNT
	};

	struct WindowParameters
	{
		/// <summary>
		/// Horizontal and vertical.
		/// </summary>
		float2 sizes;
		float outside_intrusion;
		float inside_intrusion;

		bool imposts;
		float2 impost_position;
		float impost_width;
		float impost_thickness;
	};

	struct BalconyParameters
	{
		/// <summary>
		/// Horizontal and vertical.
		/// </summary>
		float2 balcony_sizes;
		float balcony_wall_height;
		float balcony_wall_thickness;
		float balcony_intrusion;
	};

	struct SimpleWindowSlotParameters
	{
		WindowParameters window_parameters;
		float gap_between_windows;
		float edge_gap;

		__forceinline__ __device__ float get_slot() const 
		{
			return window_parameters.sizes.x + gap_between_windows;
		}
	};

	struct Window2BalconiesWindowSlotParameters
	{
		WindowParameters window_parameters;
		float gap_between_windows;
		float edge_gap;
		BalconyParameters balcony_parameters;
		float gap_between_window_and_balcony;
		float gap_between_balconies;

		__forceinline__ __device__ float get_slot() const 
		{
			return gap_between_windows + 2.0f * gap_between_window_and_balcony + gap_between_balconies + 2.0f * balcony_parameters.balcony_sizes.x + 2.0f * window_parameters.sizes.x;
		}

		__forceinline__ __device__ float get_max_vertical_size() const
		{
			return fmaxf(window_parameters.sizes.y, balcony_parameters.balcony_sizes.y);
		}
	};
}