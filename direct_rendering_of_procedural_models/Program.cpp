/**
 * Data structures to handle the application.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#include "Program.h"

#include <optix.h>
#include <optix_function_table_definition.h>
#include <optix_stack_size.h>
#include <optix_stubs.h>

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

#include <random>
#include <unordered_set>
#include <sutil\Matrix.h>
#include <array>
#include <algorithm>
#include <iterator>

#include "earcut.hpp"

#include "vector3.h"
#include "hdrloader.h"
#include "TextureCreator.h"
#include "OSMCity.h"
#include "math_utils.h"
#include "path_settings.h"
#include "traditional_geometry.h"
#include "material_creation_parameters.h"
#include "memory_measurement.h"
#include "utils.h"
#include "image_handling.h"
#include "IterationCounter.h"
#include "TimeCounter.h"

namespace drpm
{
	Program* Program::instance = nullptr;		

	static float2* create_vertices_array(std::vector<float2> vertices, bool copy_last)
	{
		int vertices_n = vertices.size() + static_cast<int>(copy_last);

		float2* ret = new float2[vertices_n];

		for (int i = 0; i < vertices_n; i++)
			ret[i] = vertices[i % vertices.size()];

		return ret;
	}

	template<class T>
	static void cudaMallocAndMemcpy(void** p_cuda, void** p_host, int n)
	{
		CUDA_CHECK(cudaMalloc(p_cuda, n * sizeof(T)));
		CUDA_CHECK(cudaMemcpy(
			*p_cuda,
			*p_host,
			n * sizeof(T),
			cudaMemcpyHostToDevice
		));
	}

	static void quadrangles_to_vertex_vector(const Quadrangle* quadrangles, const int quadrangles_num, std::vector<float3>& vertex_vector)
	{
		for (int i = 0; i < quadrangles_num; i++)
		{
			auto& q = quadrangles[i];
			Triangle triangles[2];
			q.to_triangles(triangles);
			vertex_vector.push_back(triangles[0].a);
			vertex_vector.push_back(triangles[0].b);
			vertex_vector.push_back(triangles[0].c);
			vertex_vector.push_back(triangles[1].a);
			vertex_vector.push_back(triangles[1].b);
			vertex_vector.push_back(triangles[1].c);
		}
	}

	static void triangles_to_vertex_vector(const Triangle* triangles, const int triangles_num, std::vector<float3>& vertex_vector)
	{
		for (int i = 0; i < triangles_num; i++)
		{
			auto& t = triangles[i];
			vertex_vector.push_back(t.a);
			vertex_vector.push_back(t.b);
			vertex_vector.push_back(t.c);
		}
	}

	static void extruded_polygon2d_to_quadrangles(
		const std::vector<float2>& polygon_points, const bool last_equal_to_first, 
		const float min_y, const float max_y, 
		std::vector<Quadrangle>& quadrangles
	)
	{
		if (polygon_points.size() != 4)
			throw "Incorrect number of points\n";

		const float height = max_y - min_y;

		Quadrangle q, _q;

		// Bottom.
		q.a = float2_to_float3(polygon_points[0], min_y);
		q.b = float2_to_float3(polygon_points[1], min_y);
		q.c = float2_to_float3(polygon_points[2], min_y);
		q.d = float2_to_float3(polygon_points[3], min_y);
		_q = q;
		q = q.get_reversed();
		quadrangles.push_back(q);

		// Top.
		_q = _q + vector3::up() * height;
		quadrangles.push_back(_q);

		int point_n = last_equal_to_first ? polygon_points.size() - 1 : polygon_points.size();

		for (int i = 0; i < point_n; i++)
		{
			float2 left_point = polygon_points[i];
			float2 right_point = polygon_points[(i + 1) % point_n];

			q.a = float2_to_float3(left_point, min_y);
			q.b = float2_to_float3(right_point, min_y);
			q.d = q.a + vector3::up() * height;
			q.c = q.b + vector3::up() * height;

			quadrangles.push_back(q);
		}
	}

	static void extruded_polygon2d_to_triangles(
		const std::vector<float2>& polygon_points, const bool last_equal_to_first,
		const float min_y, const float max_y,
		std::vector<Triangle>& triangles,
		bool only_top_and_bottom = false
	)
	{
		float height = max_y - min_y;

		{
			using Coord = double;
			using idx_t = uint32_t;
			using Point = std::array<Coord, 2>;

			std::vector<float2> _points = polygon_points;
		    std::reverse(_points.begin(), _points.end());

			std::vector<Point> polyline;
			for (auto& p : _points)
			{
				polyline.push_back({ p.x, p.y });
			}

			std::vector<std::vector<Point>> polygon;
			polygon.push_back(polyline);
			std::vector<idx_t> indices = mapbox::earcut<idx_t>(polygon);

			std::vector<Triangle> upper_polygon_triangles;
			for (int i = 0; i < indices.size(); i += 3)
			{
				idx_t a_idx = indices[i];
				idx_t b_idx = indices[i + 1];
				idx_t c_idx = indices[i + 2];

				Point _a = polyline[a_idx];
				Point _b = polyline[b_idx];
				Point _c = polyline[c_idx];

				Triangle triangle;

				triangle.a = make_float3(_a[0], max_y, _a[1]);
				triangle.b = make_float3(_b[0], max_y, _b[1]);
				triangle.c = make_float3(_c[0], max_y, _c[1]);

				if (dot(triangle.get_normal(), vector3::up()) < 0.f)
					triangle = triangle.get_reversed();
				triangles.push_back(triangle);
				upper_polygon_triangles.push_back(triangle);
			}

			for (auto& _triagle : upper_polygon_triangles)
			{
				Triangle triangle = _triagle.get_reversed() - vector3::up() * height;
				if (dot(triangle.get_normal(), -vector3::up()) < 0.f)
					triangle = triangle.get_reversed();
				triangles.push_back(triangle);
			}
		}

		if (!only_top_and_bottom)
		{
			int point_n = last_equal_to_first ? polygon_points.size() - 1 : polygon_points.size();

			for (int i = 0; i < point_n; i++)
			{
				float2 left_point = polygon_points[i];
				float2 right_point = polygon_points[(i + 1) % point_n];

				Quadrangle quadrangle;
				Triangle triangles_of_quadrangle[2];

				quadrangle.d = float2_to_float3(left_point, max_y);
				quadrangle.c = float2_to_float3(right_point, max_y);
				quadrangle.a = quadrangle.d - vector3::up() * height;
				quadrangle.b = quadrangle.c - vector3::up() * height;

				quadrangle.to_triangles(triangles_of_quadrangle);

				triangles.push_back(triangles_of_quadrangle[0]);
				triangles.push_back(triangles_of_quadrangle[1]);
			}
		}
	}

	void Program::create_scene()
	{
		// Create materials and textures.
		{
			create_building_materials();
			create_asphalt_material();
			create_street_lamp_bulb_material();
			create_window_impost_material();
			create_grass_material();
			create_street_lamp_material();
			create_glass_materials();
			create_skydome_textures();
		}

		// Load a city.
		{
			std::string full_path_to_city = resources_folder + "/" + cities_folder + "/" + path_to_city + ".txt";
			
			scene.osm_city = new OSMCity();

			if (path_to_heightmap != "")
			{
				path_to_heightmap = resources_folder + "/" + textures_folder + "/" + path_to_heightmap + ".png";
				path_to_heightmap = sutil::sampleDataFilePath(path_to_heightmap.c_str());
			}

			scene.osm_city->read_file(sutil::sampleDataFilePath(full_path_to_city.c_str()));

			if (drpm::print_scene_info)
				scene.osm_city->print_scene_info();
		}

		// Set the camera.
		{
			const float scene_mid_x = (scene.osm_city->get_terrain_aabb().maxX + scene.osm_city->get_terrain_aabb().minX) / 2.f;
			const float scene_mid_z = (scene.osm_city->get_terrain_aabb().maxZ + scene.osm_city->get_terrain_aabb().minZ) / 2.f;
			const float direct_dist = 500.f;
			const float angle = 45.f;
			const float x_and_y_dist = direct_dist * cosf(deg2rad(angle));

			scene.camera.setEye(make_float3(scene_mid_x, x_and_y_dist, scene_mid_z + x_and_y_dist));
			scene.camera.setLookat(make_float3(0.f, 0.f, 0.f));
			scene.camera.setUp(vector3::up());
			scene.camera.setFovY(35.0f);
			scene.camera_changed = true;

			// For New York
			//scene.camera.setEye(make_float3(-448.559, 34.6087, -252.803));
			//scene.camera.setLookat(make_float3(-184.533, 84.4232, 95.6427));

			// For Berlin
			//scene.camera.setEye(make_float3(472.774, 33.1482, 376.45));
			//scene.camera.setLookat(make_float3(319.762, 19.7087, 390.246));

			// For Berlin
			//scene.camera.setEye(make_float3(26.8954, 21.8673, 433.58));
			//scene.camera.setLookat(make_float3(84.7537, 27.5668, 421.121));

			//scene.camera.setEye(make_float3(891.348, 22.0915, -198.575));
			//scene.camera.setLookat(make_float3(823.049, 31.9472, -251.644));

			//scene.camera.setEye(make_float3(751.821, 24.0901, 11.0358));
			//scene.camera.setLookat(make_float3(757.659, 39.9803, -45.9616));

			//scene.camera.setEye(make_float3(-27.1295, 30.0704, 5.54176));
			//scene.camera.setLookat(make_float3(-35.6907, 27.6765, 34.7296));

			user_input.trackball.setCamera(&scene.camera);
			user_input.trackball.setMoveSpeed(10.0f);
			user_input.trackball.setReferenceFrame(
				make_float3(1.0f, 0.0f, 0.0f),
				make_float3(0.0f, 0.0f, 1.0f),
				make_float3(0.0f, 1.0f, 0.0f)
			);
			user_input.trackball.setGimbalLock(true);
		}		
	}

	void Program::create_context()
	{
		// Initialize CUDA
		CUDA_CHECK(cudaFree(0));

		OptixDeviceContext context;
		CUcontext cu_ctx = 0;  // zero means take the current context
		OPTIX_CHECK(optixInit());
		OptixDeviceContextOptions options = {};
		options.logCallbackFunction = &context_log_cb;
		options.logCallbackLevel = 4;
		OPTIX_CHECK(optixDeviceContextCreate(cu_ctx, &options, &context));

		optix_state.context = context;
	}

	void Program::build_traditional_GAS(SceneMemoryMeasurer& scene_memory_measurer)
	{
		int number_of_objects_with_normal_geometry = get_number_of_objects_with_normal_geometry();

		if (number_of_objects_with_normal_geometry > 0)
		{
			optix_state.vertex_buffers.resize(number_of_objects_with_normal_geometry);

			std::vector<OptixBuildInput> triangle_inputs(number_of_objects_with_normal_geometry);
			std::vector<uint32_t> triangle_input_flags(number_of_objects_with_normal_geometry);

			std::vector<size_t> object_vertices_sizes;

			{
				int i = 0;

				if (!ignore_terrain && !procedural_terrain)
				{
					Quadrangle object_quadrangles[6];
					OptixAabb_ext::to_quadrangles(scene.osm_city->get_terrain_aabb(), object_quadrangles);
					std::vector<float3> object_vertices;
					quadrangles_to_vertex_vector(object_quadrangles, 6, object_vertices);

					object_vertices_sizes.push_back(object_vertices.size());
					float3* object_vertices_data = object_vertices.data();
					cudaMallocAndMemcpy<float3>(
						reinterpret_cast<void**>(&optix_state.vertex_buffers[i]),
						reinterpret_cast<void**>(&object_vertices_data),
						object_vertices.size()
						);

					i++;
				}

				if (!ignore_buildings && !procedural_buildings)
				{
					for (auto& building : scene.osm_city->get_buildings())
					{
						std::vector<Triangle> triangles;
						std::vector<TriangleData> object_parts;

						{
							std::vector<std::pair<Triangle, TriangleData>> other_triangles;
							float height = building->aabb.maxY - building->aabb.minY;
							for (int w_i = 0; w_i < building->vertices.size() - 1; w_i++)
							{
								float3 left_node = float2_to_float3(building->vertices[w_i], building->aabb.minY);
								float3 right_node = float2_to_float3(building->vertices[w_i + 1], building->aabb.minY);
								Quadrangle wall = Quadrangle::create_from_bottom_vertices(left_node, right_node, vector3::up(), height);
								RegularBuildingFacadeParameters facade_parameters;
								unsigned int seed_b = static_cast<unsigned int>(i);
								facade_parameters.calculate_window_parameters(wall.get_width(), wall.get_height(), seed_b);
								get_facade_triangles(wall, facade_parameters, other_triangles);
							}
							for (auto& t : other_triangles)
							{
								triangles.push_back(t.first);
								object_parts.push_back(t.second);
							}
						}

						{
							extruded_polygon2d_to_triangles(building->vertices, true, building->aabb.minY, building->aabb.maxY, triangles, true);

							for (auto& t : triangles)
							{
								object_parts.push_back({ SceneObjectPart::BUILDING_ROOF, -1 });
							}
						}

						std::vector<float3> object_vertices;
						triangles_to_vertex_vector(triangles.data(), triangles.size(), object_vertices);

						scene.building_parts.push_back(object_parts);

						object_vertices_sizes.push_back(object_vertices.size());
						float3* object_vertices_data = object_vertices.data();
						cudaMallocAndMemcpy<float3>(
							reinterpret_cast<void**>(&optix_state.vertex_buffers[i]),
							reinterpret_cast<void**>(&object_vertices_data),
							object_vertices.size()
							);
						object_vertices.clear();
						object_vertices.shrink_to_fit();

						i++;
					}
				}

				if (!ignore_edge_roads && !procedural_edge_roads)
				{
					for (auto& edge_road : scene.osm_city->get_edge_roads())
					{
						std::vector<Quadrangle> object_quadrangles;
						extruded_polygon2d_to_quadrangles(edge_road->vertices, false, edge_road->aabb.minY, edge_road->aabb.maxY, object_quadrangles);
						std::vector<float3> object_vertices;
						quadrangles_to_vertex_vector(object_quadrangles.data(), object_quadrangles.size(), object_vertices);

						object_vertices_sizes.push_back(object_vertices.size());
						float3* object_vertices_data = object_vertices.data();
						cudaMallocAndMemcpy<float3>(
							reinterpret_cast<void**>(&optix_state.vertex_buffers[i]),
							reinterpret_cast<void**>(&object_vertices_data),
							object_vertices.size()
							);
						object_vertices.clear();
						object_vertices.shrink_to_fit();

						i++;
					}
				}

				if (!ignore_joint_roads && !procedural_joint_roads)
				{
					for (auto& joint_road : scene.osm_city->get_joint_roads())
					{
						std::vector<Triangle> object_triangles;
						extruded_polygon2d_to_triangles(joint_road->vertices, false, joint_road->aabb.minY, joint_road->aabb.maxY, object_triangles);
						std::vector<float3> object_vertices;
						triangles_to_vertex_vector(object_triangles.data(), object_triangles.size(), object_vertices);

						object_vertices_sizes.push_back(object_vertices.size());
						float3* object_vertices_data = object_vertices.data();
						cudaMallocAndMemcpy<float3>(
							reinterpret_cast<void**>(&optix_state.vertex_buffers[i]),
							reinterpret_cast<void**>(&object_vertices_data),
							object_vertices.size()
							);
						object_vertices.clear();
						object_vertices.shrink_to_fit();

						i++;
					}
				}

				if (!ignore_sidewalks && !procedural_sidewalks)
				{
					for (auto& sidewalk : scene.osm_city->get_sidewalks())
					{
						std::vector<Quadrangle> object_quadrangles;
						extruded_polygon2d_to_quadrangles(sidewalk->vertices, false, sidewalk->aabb.minY, sidewalk->aabb.maxY, object_quadrangles);
						std::vector<float3> object_vertices;
						quadrangles_to_vertex_vector(object_quadrangles.data(), object_quadrangles.size(), object_vertices);

						object_vertices_sizes.push_back(object_vertices.size());
						float3* object_vertices_data = object_vertices.data();
						cudaMallocAndMemcpy<float3>(
							reinterpret_cast<void**>(&optix_state.vertex_buffers[i]),
							reinterpret_cast<void**>(&object_vertices_data),
							object_vertices.size()
							);
						object_vertices.clear();
						object_vertices.shrink_to_fit();

						i++;
					}
				}
			}

			size_t triangle_counter = 0;

			for (int i = 0; i < number_of_objects_with_normal_geometry; i++)
			{
				triangle_input_flags[i] = OPTIX_GEOMETRY_FLAG_DISABLE_ANYHIT;
				triangle_inputs[i].type = OPTIX_BUILD_INPUT_TYPE_TRIANGLES;
				triangle_inputs[i].triangleArray.vertexFormat = OPTIX_VERTEX_FORMAT_FLOAT3;
				triangle_inputs[i].triangleArray.vertexStrideInBytes = sizeof(float3);
				triangle_inputs[i].triangleArray.numVertices = static_cast<uint32_t>(object_vertices_sizes[i]);
				triangle_inputs[i].triangleArray.vertexBuffers = &optix_state.vertex_buffers[i];
				triangle_inputs[i].triangleArray.flags = &triangle_input_flags[i];
				triangle_inputs[i].triangleArray.numSbtRecords = 1;
				triangle_inputs[i].triangleArray.sbtIndexOffsetBuffer = 0;
				triangle_inputs[i].triangleArray.sbtIndexOffsetSizeInBytes = sizeof(uint32_t);
				triangle_inputs[i].triangleArray.sbtIndexOffsetStrideInBytes = sizeof(uint32_t);

				triangle_counter += object_vertices_sizes[i] / 3;
			}

			if (drpm::print_scene_geometry_info)
			{
				std::cout << "\n";
				std::cout << "\n";
				std::cout << "=============================================================\n";
				std::cout << "\n";
				std::cout << "The geometry was created.\n";
				std::cout << "Number of triangles: " << triangle_counter << "\n";
				std::cout << "Number of vertices: " << triangle_counter * 3 << "\n";
				std::cout << "\n";
				std::cout << "=============================================================\n";
				std::cout << "\n";
				std::cout << "\n";
			}

			OptixAccelBuildOptions accel_options = {};
			accel_options.buildFlags = OPTIX_BUILD_FLAG_ALLOW_COMPACTION;
			accel_options.operation = OPTIX_BUILD_OPERATION_BUILD;

			OptixAccelBufferSizes gas_buffer_sizes;
			OPTIX_CHECK(optixAccelComputeMemoryUsage(
				optix_state.context,
				&accel_options,
				triangle_inputs.data(),
				triangle_inputs.size(),  // num_build_inputs
				&gas_buffer_sizes
			));

			CUdeviceptr d_temp_buffer;
			CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(&d_temp_buffer), gas_buffer_sizes.tempSizeInBytes));

			// non-compacted output
			CUdeviceptr d_buffer_temp_output_gas_and_compacted_size;
			size_t      compactedSizeOffset = roundUp<size_t>(gas_buffer_sizes.outputSizeInBytes, 8ull);
			CUDA_CHECK(cudaMalloc(
				reinterpret_cast<void**>(&d_buffer_temp_output_gas_and_compacted_size),
				compactedSizeOffset + 8
			));

			OptixAccelEmitDesc emitProperty = {};
			emitProperty.type = OPTIX_PROPERTY_TYPE_COMPACTED_SIZE;
			emitProperty.result = (CUdeviceptr)((char*)d_buffer_temp_output_gas_and_compacted_size + compactedSizeOffset);

			OPTIX_CHECK(optixAccelBuild(
				optix_state.context,
				0,                                  // CUDA stream
				&accel_options,
				triangle_inputs.data(),
				triangle_inputs.size(),             // num build inputs
				d_temp_buffer,
				gas_buffer_sizes.tempSizeInBytes,
				d_buffer_temp_output_gas_and_compacted_size,
				gas_buffer_sizes.outputSizeInBytes,
				&optix_state.traditional_geometry_gas_handle,
				&emitProperty,                      // emitted property list
				1                                   // num emitted properties
			));

			scene_memory_measurer.set_traditional_gas_memory(gas_buffer_sizes.outputSizeInBytes);

			CUDA_CHECK(cudaFree(reinterpret_cast<void*>(d_temp_buffer)));

			size_t compacted_gas_size;
			CUDA_CHECK(cudaMemcpy(&compacted_gas_size, (void*)emitProperty.result, sizeof(size_t), cudaMemcpyDeviceToHost));

			if (compacted_gas_size < gas_buffer_sizes.outputSizeInBytes)
			{
				CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(&optix_state.traditional_geometry_gas_buffer), compacted_gas_size));

				// use handle as input and output
				OPTIX_CHECK(optixAccelCompact(optix_state.context, 0, optix_state.traditional_geometry_gas_handle, optix_state.traditional_geometry_gas_buffer, compacted_gas_size, &optix_state.traditional_geometry_gas_handle));

				CUDA_CHECK(cudaFree((void*)d_buffer_temp_output_gas_and_compacted_size));
			}
			else
			{
				optix_state.traditional_geometry_gas_buffer = d_buffer_temp_output_gas_and_compacted_size;
			}
		}
	}

	void Program::build_procedural_GAS(SceneMemoryMeasurer& scene_memory_measurer)
	{
		int number_of_objects_with_procedural_geometry = get_number_of_objects_with_procedural_geometry();

		if (number_of_objects_with_procedural_geometry > 0)
		{
			optix_state.aabb_buffers.resize(number_of_objects_with_procedural_geometry);

			std::vector<OptixBuildInput> aabb_inputs(number_of_objects_with_procedural_geometry);
			std::vector<uint32_t> aabb_input_flags(number_of_objects_with_procedural_geometry);

			std::vector<OptixAabb> aabbs;

			if (!ignore_terrain && procedural_terrain)
				aabbs.push_back(scene.osm_city->get_terrain_aabb());

			if (!ignore_buildings && procedural_buildings)
				for (auto& o : scene.osm_city->get_buildings())
					aabbs.push_back(o->aabb);

			if (!ignore_edge_roads && procedural_edge_roads)
				for (auto& o : scene.osm_city->get_edge_roads())
					aabbs.push_back(o->aabb);

			if (!ignore_joint_roads && procedural_joint_roads)
				for (auto& o : scene.osm_city->get_joint_roads())
					aabbs.push_back(o->aabb);

			if (!ignore_sidewalks && procedural_sidewalks)
				for (auto& o : scene.osm_city->get_sidewalks())
					aabbs.push_back(o->aabb);

			if (!ignore_street_lamps)
			{
				for (auto& o : scene.osm_city->get_street_lamps())
					aabbs.push_back(o->aabb);

				for (auto& o : scene.osm_city->get_street_lamps_and_colliders())
					aabbs.push_back(*o.second);
			}

			for (int i = 0; i < aabbs.size(); i++)
			{
				auto& aabb = aabbs[i];

				CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(&optix_state.aabb_buffers[i]), sizeof(OptixAabb)));
				CUDA_CHECK(cudaMemcpy(
					reinterpret_cast<void*>(optix_state.aabb_buffers[i]),
					&aabb, sizeof(OptixAabb),
					cudaMemcpyHostToDevice
				));

				aabb_inputs[i] = {};

				aabb_inputs[i].type = OPTIX_BUILD_INPUT_TYPE_CUSTOM_PRIMITIVES;
				aabb_inputs[i].customPrimitiveArray.aabbBuffers = &optix_state.aabb_buffers[i];
				aabb_inputs[i].customPrimitiveArray.flags = &aabb_input_flags[i];
				aabb_inputs[i].customPrimitiveArray.numSbtRecords = 1;
				aabb_inputs[i].customPrimitiveArray.numPrimitives = 1;
				aabb_inputs[i].customPrimitiveArray.sbtIndexOffsetBuffer = 0;
				aabb_inputs[i].customPrimitiveArray.sbtIndexOffsetSizeInBytes = sizeof(uint32_t);
				aabb_inputs[i].customPrimitiveArray.primitiveIndexOffset = 0;
			}

			OptixAccelBuildOptions accel_options = {};
			accel_options.buildFlags = OPTIX_BUILD_FLAG_ALLOW_COMPACTION;
			accel_options.operation = OPTIX_BUILD_OPERATION_BUILD;

			OptixAccelBufferSizes gas_buffer_sizes;
			OPTIX_CHECK(optixAccelComputeMemoryUsage(
				optix_state.context,
				&accel_options,
				aabb_inputs.data(),
				aabb_inputs.size(),  // num_build_inputs
				&gas_buffer_sizes
			));

			CUdeviceptr d_temp_buffer;
			CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(&d_temp_buffer), gas_buffer_sizes.tempSizeInBytes));

			// non-compacted output
			CUdeviceptr d_buffer_temp_output_gas_and_compacted_size;
			size_t compactedSizeOffset = roundUp<size_t>(gas_buffer_sizes.outputSizeInBytes, 8ull);
			CUDA_CHECK(cudaMalloc(
				reinterpret_cast<void**>(&d_buffer_temp_output_gas_and_compacted_size),
				compactedSizeOffset + 8
			));

			OptixAccelEmitDesc emitProperty = {};
			emitProperty.type = OPTIX_PROPERTY_TYPE_COMPACTED_SIZE;
			emitProperty.result = (CUdeviceptr)((char*)d_buffer_temp_output_gas_and_compacted_size + compactedSizeOffset);

			OPTIX_CHECK(optixAccelBuild(
				optix_state.context,
				0,                                  // CUDA stream
				&accel_options,
				aabb_inputs.data(),
				aabb_inputs.size(),             // num build inputs
				d_temp_buffer,
				gas_buffer_sizes.tempSizeInBytes,
				d_buffer_temp_output_gas_and_compacted_size,
				gas_buffer_sizes.outputSizeInBytes,
				&optix_state.procedural_geometry_gas_handle,
				&emitProperty,                      // emitted property list
				1                                   // num emitted properties
			));

			scene_memory_measurer.set_procedural_gas_memory(gas_buffer_sizes.outputSizeInBytes);

			CUDA_CHECK(cudaFree(reinterpret_cast<void*>(d_temp_buffer)));

			size_t compacted_gas_size;
			CUDA_CHECK(cudaMemcpy(&compacted_gas_size, (void*)emitProperty.result, sizeof(size_t), cudaMemcpyDeviceToHost));

			if (compacted_gas_size < gas_buffer_sizes.outputSizeInBytes)
			{
				CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(&optix_state.procedural_geometry_gas_buffer), compacted_gas_size));

				// use handle as input and output
				OPTIX_CHECK(optixAccelCompact(optix_state.context, 0, optix_state.procedural_geometry_gas_handle, optix_state.procedural_geometry_gas_buffer, compacted_gas_size, &optix_state.procedural_geometry_gas_handle));

				CUDA_CHECK(cudaFree((void*)d_buffer_temp_output_gas_and_compacted_size));
			}
			else
			{
				optix_state.procedural_geometry_gas_buffer = d_buffer_temp_output_gas_and_compacted_size;
			}
		}
	}

	template <typename T>
	void copyToDevice(const T& source, CUdeviceptr destination)
	{
		CUDA_CHECK(cudaMemcpy(reinterpret_cast<void*>(destination), &source, sizeof(T), cudaMemcpyHostToDevice));
	}

	template <typename T>
	void createOnDevice(const T& source, CUdeviceptr* destination)
	{
		CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(destination), sizeof(T)));
		copyToDevice(source, *destination);
	}

	template <typename T>
	void copyToDevice(const std::vector<T>& source, CUdeviceptr destination)
	{
		CUDA_CHECK(cudaMemcpy(reinterpret_cast<void*>(destination), source.data(), source.size() * sizeof(T), cudaMemcpyHostToDevice));
	}

	template <typename T>
	void createOnDevice(const std::vector<T>& source, CUdeviceptr* destination)
	{
		CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(destination), source.size() * sizeof(T)));
		copyToDevice(source, *destination);
	}

	void Program::build_IAS()
	{
		std::vector<OptixInstance> instances;
		unsigned int               sbtOffset = 0;

		OptixInstance instance = {};
		// Common instance settings
		instance.instanceId = 0;
		instance.visibilityMask = 0xFF;
		instance.flags = OPTIX_INSTANCE_FLAG_NONE;
		sutil::Matrix3x4 yUpTransform = {
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
		};

		{
			memcpy(instance.transform, yUpTransform.getData(), sizeof(float) * 12);
			instance.sbtOffset = sbtOffset;
			instance.traversableHandle = optix_state.procedural_geometry_gas_handle;
			sbtOffset += get_number_of_objects_with_procedural_geometry() * RAY_TYPE_COUNT;
			instances.push_back(instance);
		}

		{
			memcpy(instance.transform, yUpTransform.getData(), sizeof(float) * 12);
			instance.sbtOffset = sbtOffset;
			instance.traversableHandle = optix_state.traditional_geometry_gas_handle;
			sbtOffset += RAY_TYPE_COUNT;
			instances.push_back(instance);
		}
		CUdeviceptr deviceInstances = 0;
		//createOnDevice(instances, &deviceInstances);
		void* inctances_data_p = instances.data();
		cudaMallocAndMemcpy<OptixInstance>(reinterpret_cast<void**>(&deviceInstances), reinterpret_cast<void**>(&inctances_data_p), instances.size());

		// Instance build input.
		OptixBuildInput buildInput = {};

		buildInput.type = OPTIX_BUILD_INPUT_TYPE_INSTANCES;
		buildInput.instanceArray.instances = deviceInstances;
		buildInput.instanceArray.numInstances = static_cast<unsigned int>(instances.size());

		OptixAccelBuildOptions accelBuildOptions = {};
		accelBuildOptions.buildFlags = OPTIX_BUILD_FLAG_NONE;
		accelBuildOptions.operation = OPTIX_BUILD_OPERATION_BUILD;

		OptixAccelBufferSizes bufferSizesIAS;
		OPTIX_CHECK(optixAccelComputeMemoryUsage(optix_state.context, &accelBuildOptions, &buildInput,
			1,  // Number of build inputs
			&bufferSizesIAS));

		CUdeviceptr deviceTempBufferIAS;
		CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(&deviceTempBufferIAS),
			bufferSizesIAS.tempSizeInBytes));
		CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(&optix_state.deviceBufferIAS),
			bufferSizesIAS.outputSizeInBytes));

		OPTIX_CHECK(optixAccelBuild(
			optix_state.context,
			0,  // CUDA stream
			&accelBuildOptions,
			&buildInput,
			1,  // num build inputs
			deviceTempBufferIAS,
			bufferSizesIAS.tempSizeInBytes,
			optix_state.deviceBufferIAS,
			bufferSizesIAS.outputSizeInBytes,
			&optix_state.hIAS,
			nullptr,  // emitted property list
			0));    // num emitted properties

		CUDA_CHECK(cudaFree(reinterpret_cast<void*>(deviceInstances)));
		CUDA_CHECK(cudaFree(reinterpret_cast<void*>(deviceTempBufferIAS)));
	}

	void Program::create_module()
	{
		char log[2048];

		OptixModuleCompileOptions module_compile_options = {};
		module_compile_options.maxRegisterCount = OPTIX_COMPILE_DEFAULT_MAX_REGISTER_COUNT;
		module_compile_options.optLevel = OPTIX_COMPILE_OPTIMIZATION_DEFAULT;
		module_compile_options.debugLevel = OPTIX_COMPILE_DEBUG_LEVEL_LINEINFO;

		optix_state.pipeline_compile_options.usesMotionBlur = false;
		optix_state.pipeline_compile_options.traversableGraphFlags = OPTIX_TRAVERSABLE_GRAPH_FLAG_ALLOW_ANY;
		optix_state.pipeline_compile_options.numPayloadValues = 7;
		optix_state.pipeline_compile_options.numAttributeValues = 7;
		optix_state.pipeline_compile_options.exceptionFlags = OPTIX_EXCEPTION_FLAG_DEBUG | OPTIX_EXCEPTION_FLAG_TRACE_DEPTH | OPTIX_EXCEPTION_FLAG_STACK_OVERFLOW;
		optix_state.pipeline_compile_options.pipelineLaunchParamsVariableName = "params";

		std::string ptx = sutil::getPtxString(OPTIX_SAMPLE_NAME, OPTIX_SAMPLE_DIR, "rendering.cu");

		size_t sizeof_log = sizeof(log);

		OPTIX_CHECK_LOG(optixModuleCreateFromPTX(
			optix_state.context,
			&module_compile_options,
			&optix_state.pipeline_compile_options,
			ptx.c_str(),
			ptx.size(),
			log,
			&sizeof_log,
			&optix_state.module
		));
	}

	void Program::create_program_groups()
	{
		char log[2048];

		OptixProgramGroupOptions program_group_options = {}; // Initialize to zeros
		size_t sizeof_log;

		// Raygen.
		{
			OptixProgramGroupDesc raygen_prog_group_desc = {}; //
			raygen_prog_group_desc.kind = OPTIX_PROGRAM_GROUP_KIND_RAYGEN;
			raygen_prog_group_desc.raygen.module = optix_state.module;
			raygen_prog_group_desc.raygen.entryFunctionName = "__raygen__rg";
			sizeof_log = sizeof(log);
			OPTIX_CHECK_LOG(optixProgramGroupCreate(
				optix_state.context,
				&raygen_prog_group_desc,
				1,   // num program groups
				&program_group_options,
				log,
				&sizeof_log,
				&optix_state.raygen_prog_group
			));
		}

		// Miss.
		{
			OptixProgramGroupDesc miss_prog_group_desc = {};
			miss_prog_group_desc.kind = OPTIX_PROGRAM_GROUP_KIND_MISS;
			miss_prog_group_desc.raygen.module = optix_state.module;
			miss_prog_group_desc.raygen.entryFunctionName = "__miss__radiance";
			sizeof_log = sizeof(log);
			OPTIX_CHECK_LOG(optixProgramGroupCreate(
				optix_state.context,
				&miss_prog_group_desc,
				1,   // num program groups
				&program_group_options,
				log,
				&sizeof_log,
				&optix_state.radiance_miss_prog_group
			));
		}

		// Hit for procedural geometry.
		{
			OptixProgramGroupDesc hit_prog_group_desc = {};
			hit_prog_group_desc.kind = OPTIX_PROGRAM_GROUP_KIND_HITGROUP;
			hit_prog_group_desc.hitgroup.moduleIS = optix_state.module;
			hit_prog_group_desc.hitgroup.entryFunctionNameIS = "__intersection__is";
			hit_prog_group_desc.hitgroup.moduleCH = optix_state.module;
			hit_prog_group_desc.hitgroup.entryFunctionNameCH = "__closesthit__radiance";

			char log[2048];
			size_t sizeof_log = sizeof(log);
			OPTIX_CHECK(optixProgramGroupCreate(
				optix_state.context,
				&hit_prog_group_desc,
				1,
				&program_group_options,
				log,
				&sizeof_log,
				&optix_state.procedural_geometry_radiance_hit_prog_group
			));
		}

		// Hit for prepared geometry.
		{
			OptixProgramGroupDesc hit_prog_group_desc = {};
			hit_prog_group_desc.kind = OPTIX_PROGRAM_GROUP_KIND_HITGROUP;
			hit_prog_group_desc.hitgroup.moduleCH = optix_state.module;
			hit_prog_group_desc.hitgroup.entryFunctionNameCH = "__closesthit__radiance";

			char log[2048];
			size_t sizeof_log = sizeof(log);
			OPTIX_CHECK(optixProgramGroupCreate(
				optix_state.context,
				&hit_prog_group_desc,
				1,
				&program_group_options,
				log,
				&sizeof_log,
				&optix_state.normal_geometry_radiance_hit_prog_group
			));
		}
	}

	void Program::create_pipeline()
	{
		OptixProgramGroup program_groups[] =
		{
			optix_state.raygen_prog_group,
			optix_state.radiance_miss_prog_group,
			optix_state.procedural_geometry_radiance_hit_prog_group,
			optix_state.normal_geometry_radiance_hit_prog_group
		};

		uint32_t max_trace_depth = max_recursion_depth;

		OptixPipelineLinkOptions pipeline_link_options = {};
		pipeline_link_options.maxTraceDepth = max_trace_depth;
		pipeline_link_options.debugLevel = OPTIX_COMPILE_DEBUG_LEVEL_FULL;

		char log[2048];
		size_t sizeof_log = sizeof(log);
		OPTIX_CHECK_LOG(optixPipelineCreate(
			optix_state.context,
			&optix_state.pipeline_compile_options,
			&pipeline_link_options,
			program_groups,
			sizeof(program_groups) / sizeof(program_groups[0]),
			log,
			&sizeof_log,
			&optix_state.pipeline
		));

		// We need to specify the max traversal depth.  Calculate the stack sizes, so we can specify all
		// parameters to optixPipelineSetStackSize.
		OptixStackSizes stack_sizes = {};
		OPTIX_CHECK(optixUtilAccumulateStackSizes(optix_state.raygen_prog_group, &stack_sizes));
		OPTIX_CHECK(optixUtilAccumulateStackSizes(optix_state.radiance_miss_prog_group, &stack_sizes));
		OPTIX_CHECK(optixUtilAccumulateStackSizes(optix_state.procedural_geometry_radiance_hit_prog_group, &stack_sizes));
		OPTIX_CHECK(optixUtilAccumulateStackSizes(optix_state.normal_geometry_radiance_hit_prog_group, &stack_sizes));

		uint32_t max_cc_depth = 0;
		uint32_t max_dc_depth = 0;
		uint32_t direct_callable_stack_size_from_traversal;
		uint32_t direct_callable_stack_size_from_state;
		uint32_t continuation_stack_size;
		OPTIX_CHECK(optixUtilComputeStackSizes(
			&stack_sizes,
			max_trace_depth,
			max_cc_depth,
			max_dc_depth,
			&direct_callable_stack_size_from_traversal,
			&direct_callable_stack_size_from_state,
			&continuation_stack_size
		));

		const uint32_t max_traversal_depth = 1;
		OPTIX_CHECK(optixPipelineSetStackSize(
			optix_state.pipeline,
			direct_callable_stack_size_from_traversal,
			direct_callable_stack_size_from_state,
			continuation_stack_size,
			max_traversal_depth
		));
	}

	void Program::create_SBT(SceneMemoryMeasurer& scene_memory_measurer)
	{
		int number_of_objects = get_number_of_objects_with_procedural_geometry() + get_number_of_objects_with_normal_geometry();

		CUdeviceptr d_raygen_record;
		const size_t raygen_record_size = sizeof(RayGenRecord);
		CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(&d_raygen_record), raygen_record_size));

		RayGenRecord rg_sbt = {};
		OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.raygen_prog_group, &rg_sbt));

		CUDA_CHECK(cudaMemcpy(
			reinterpret_cast<void*>(d_raygen_record),
			&rg_sbt,
			raygen_record_size,
			cudaMemcpyHostToDevice
		));

		CUdeviceptr d_miss_records;
		const size_t miss_record_size = sizeof(MissRecord);
		CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(&d_miss_records), miss_record_size * RAY_TYPE_COUNT));

		MissRecord ms_sbt;
		OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.radiance_miss_prog_group, &ms_sbt));
		ms_sbt.data.skydome_parameters.texture_object = scene.skydome_textures[optix_state.skydome_parameters.texture_number].cuda_texture_object;
		ms_sbt.data.skydome_parameters.emissive_color = optix_state.skydome_parameters.emissive_color;
		ms_sbt.data.skydome_parameters.texture_color_factor = optix_state.skydome_parameters.texture_color_factor;

		CUDA_CHECK(cudaMemcpy(
			reinterpret_cast<void*>(d_miss_records),
			&ms_sbt,
			miss_record_size * RAY_TYPE_COUNT,
			cudaMemcpyHostToDevice
		));

		CUdeviceptr d_hitgroup_records;
		const size_t hitgroup_record_size = sizeof(HitGroupRecord);
		CUDA_CHECK(cudaMalloc(
			reinterpret_cast<void**>(&d_hitgroup_records),
			hitgroup_record_size * RAY_TYPE_COUNT * number_of_objects
		));

		optix_state.hitgroup_records.resize(RAY_TYPE_COUNT * number_of_objects);

		size_t procedural_geometry_req_memory = 0;

		{
			int j = 0;

			if (!ignore_terrain && procedural_terrain)
			{
				const int sbt_idx = j * RAY_TYPE_COUNT + 0;  // SBT for radiance ray-type for ith material

				OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.procedural_geometry_radiance_hit_prog_group, &optix_state.hitgroup_records[sbt_idx]));
				optix_state.hitgroup_records[sbt_idx].data.geometry_type = GeometryType::gt_PROCEDURAL;
				optix_state.hitgroup_records[sbt_idx].data.seed = global_seed * (j + 1) * 123456789;
				optix_state.hitgroup_records[sbt_idx].data.scene_object_type = SceneObjectType::TERRAIN;
				OptixAabb_ext::copy(optix_state.hitgroup_records[sbt_idx].data.aabb, scene.osm_city->get_terrain_aabb());

				j++;

				procedural_geometry_req_memory += sizeof(HitGroupData) - sizeof(TraditionalGeometryHitGroupData);
			}

			if (!ignore_buildings && procedural_buildings)
			{
				for (auto& building : scene.osm_city->get_buildings())
				{
					const int sbt_idx = j * RAY_TYPE_COUNT + 0;  // SBT for radiance ray-type for ith material

					float height = building->aabb.maxY - building->aabb.minY;
					unsigned int seed_for_building = global_seed * (j + 1) * 123456789;
					OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.procedural_geometry_radiance_hit_prog_group, &optix_state.hitgroup_records[sbt_idx]));
					optix_state.hitgroup_records[sbt_idx].data.seed = seed_for_building;
					optix_state.hitgroup_records[sbt_idx].data.scene_object_type = SceneObjectType::BUILDING;
					OptixAabb_ext::copy(optix_state.hitgroup_records[sbt_idx].data.aabb, building->aabb);

					int number_of_vertices = building->vertices.size();

					float2* vertices = create_vertices_array(building->vertices, false);

					optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.geometry.polygon.number_of_nodes = number_of_vertices;
					cudaMallocAndMemcpy<float2>(
						reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.geometry.polygon.contour_nodes),
						reinterpret_cast<void**>(&vertices),
						number_of_vertices
						);
					optix_state.cuda_allocated_sbt_memory.push_back(
						reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.geometry.polygon.contour_nodes)
					);

					{
						RegularBuildingFacadeParameters* facade_parameters = new RegularBuildingFacadeParameters[number_of_vertices];

						for (int i = 0; i < number_of_vertices - 1; i++)
						{
							facade_parameters[i].calculated = false;
							facade_parameters[i].calculate_window_parameters(length(vertices[i] - vertices[i + 1]), height, seed_for_building);
						}

						cudaMallocAndMemcpy<RegularBuildingFacadeParameters>(
							reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.miscellaneous.building_properties.facade_params),
							reinterpret_cast<void**>(&facade_parameters),
							number_of_vertices
							);
						optix_state.cuda_allocated_sbt_memory.push_back(
							reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.miscellaneous.building_properties.facade_params)
						);

						delete[] facade_parameters;
					}

					delete[] vertices;

					j++;

					procedural_geometry_req_memory += sizeof(HitGroupData) - sizeof(TraditionalGeometryHitGroupData);
					procedural_geometry_req_memory += sizeof(float2) * number_of_vertices;
					procedural_geometry_req_memory += sizeof(RegularBuildingFacadeParameters) * number_of_vertices;
				}
			}

			if (!ignore_edge_roads && procedural_edge_roads)
			{
				for (auto& edge_road : scene.osm_city->get_edge_roads())
				{
					const int sbt_idx = j * RAY_TYPE_COUNT + 0;  // SBT for radiance ray-type for ith material

					OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.procedural_geometry_radiance_hit_prog_group, &optix_state.hitgroup_records[sbt_idx]));
					optix_state.hitgroup_records[sbt_idx].data.geometry_type = GeometryType::gt_PROCEDURAL;
					optix_state.hitgroup_records[sbt_idx].data.seed = global_seed * (j + 1) * 123456789;
					optix_state.hitgroup_records[sbt_idx].data.scene_object_type = SceneObjectType::EDGE_ROAD;
					optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.miscellaneous.road_marking_properties.calculated = false;
					OptixAabb_ext::copy(optix_state.hitgroup_records[sbt_idx].data.aabb, edge_road->aabb);

					int number_of_vertices = edge_road->vertices.size() + 1;

					{
						float2* vertices = create_vertices_array(edge_road->vertices, true);

						optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.geometry.polygon.number_of_nodes = number_of_vertices;
						cudaMallocAndMemcpy<float2>(
							reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.geometry.polygon.contour_nodes),
							reinterpret_cast<void**>(&vertices),
							number_of_vertices
							);
						optix_state.cuda_allocated_sbt_memory.push_back(
							reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.geometry.polygon.contour_nodes)
						);

						delete[] vertices;
					}

					j++;

					procedural_geometry_req_memory += sizeof(HitGroupData) - sizeof(TraditionalGeometryHitGroupData);
					procedural_geometry_req_memory += sizeof(float2) * number_of_vertices;
				}
			}

			if (!ignore_joint_roads && procedural_joint_roads)
			{
				for (auto& joint_road : scene.osm_city->get_joint_roads())
				{
					const int sbt_idx = j * RAY_TYPE_COUNT + 0;  // SBT for radiance ray-type for ith material

					OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.procedural_geometry_radiance_hit_prog_group, &optix_state.hitgroup_records[sbt_idx]));
					optix_state.hitgroup_records[sbt_idx].data.geometry_type = GeometryType::gt_PROCEDURAL;
					optix_state.hitgroup_records[sbt_idx].data.seed = global_seed * (j + 1) * 123456789;
					optix_state.hitgroup_records[sbt_idx].data.scene_object_type = SceneObjectType::JOINT_ROAD;
					OptixAabb_ext::copy(optix_state.hitgroup_records[sbt_idx].data.aabb, joint_road->aabb);

					int number_of_vertices = joint_road->vertices.size() + 1;

					{
						float2* vertices = create_vertices_array(joint_road->vertices, true);

						optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.geometry.polygon.number_of_nodes = number_of_vertices;
						cudaMallocAndMemcpy<float2>(
							reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.geometry.polygon.contour_nodes),
							reinterpret_cast<void**>(&vertices),
							number_of_vertices
							);
						optix_state.cuda_allocated_sbt_memory.push_back(
							reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.geometry.polygon.contour_nodes)
						);

						delete[] vertices;
					}

					j++;

					procedural_geometry_req_memory += sizeof(HitGroupData) - sizeof(TraditionalGeometryHitGroupData);
					procedural_geometry_req_memory += sizeof(float2) * number_of_vertices;
				}
			}

			if (!ignore_sidewalks && procedural_sidewalks)
			{
				for (auto& sidewalk : scene.osm_city->get_sidewalks())
				{
					const int sbt_idx = j * RAY_TYPE_COUNT + 0;  // SBT for radiance ray-type for ith material

					OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.procedural_geometry_radiance_hit_prog_group, &optix_state.hitgroup_records[sbt_idx]));
					optix_state.hitgroup_records[sbt_idx].data.geometry_type = GeometryType::gt_PROCEDURAL;
					optix_state.hitgroup_records[sbt_idx].data.seed = global_seed * (j + 1) * 123456789;
					optix_state.hitgroup_records[sbt_idx].data.scene_object_type = SceneObjectType::SIDEWALK;
					OptixAabb_ext::copy(optix_state.hitgroup_records[sbt_idx].data.aabb, sidewalk->aabb);

					int number_of_vertices = sidewalk->vertices.size() + 1;

					{
						float2* vertices = create_vertices_array(sidewalk->vertices, true);

						optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.geometry.polygon.number_of_nodes = number_of_vertices;
						cudaMallocAndMemcpy<float2>(
							reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.geometry.polygon.contour_nodes),
							reinterpret_cast<void**>(&vertices),
							number_of_vertices
							);
						optix_state.cuda_allocated_sbt_memory.push_back(
							reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.geometry.polygon.contour_nodes)
						);

						delete[] vertices;
					}

					j++;

					procedural_geometry_req_memory += sizeof(HitGroupData) - sizeof(TraditionalGeometryHitGroupData);
					procedural_geometry_req_memory += sizeof(float2) * number_of_vertices;
				}
			}

			// Street lamps.
			if (!ignore_street_lamps)
			{
				int number_of_street_lamps = scene.osm_city->get_street_lamps().size();

				for (auto& street_lamp : scene.osm_city->get_street_lamps())
				{
					const int sbt_idx = j * RAY_TYPE_COUNT + 0;  // SBT for radiance ray-type for ith material

					OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.procedural_geometry_radiance_hit_prog_group, &optix_state.hitgroup_records[sbt_idx]));
					optix_state.hitgroup_records[sbt_idx].data.geometry_type = GeometryType::gt_PROCEDURAL;
					optix_state.hitgroup_records[sbt_idx].data.seed = global_seed * (j + 1) * 123456789;
					optix_state.hitgroup_records[sbt_idx].data.scene_object_type = SceneObjectType::STREET_LAMP;
					OptixAabb_ext::copy(optix_state.hitgroup_records[sbt_idx].data.aabb, street_lamp->aabb);

					j++;

					procedural_geometry_req_memory += sizeof(HitGroupData) - sizeof(TraditionalGeometryHitGroupData);
				}

				for (auto& street_lamp_and_collider_pair : scene.osm_city->get_street_lamps_and_colliders())
				{
					auto& street_lamp = street_lamp_and_collider_pair.first;
					auto& collider_aabb = street_lamp_and_collider_pair.second;

					const int sbt_idx = j * RAY_TYPE_COUNT + 0;  // SBT for radiance ray-type for ith material

					OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.procedural_geometry_radiance_hit_prog_group, &optix_state.hitgroup_records[sbt_idx]));
					optix_state.hitgroup_records[sbt_idx].data.geometry_type = GeometryType::gt_PROCEDURAL;
					optix_state.hitgroup_records[sbt_idx].data.seed = global_seed * (j + 1) * 123456789;
					optix_state.hitgroup_records[sbt_idx].data.scene_object_type = SceneObjectType::LIGHT_SOURCE_COLLIDER;
					OptixAabb_ext::copy(optix_state.hitgroup_records[sbt_idx].data.procedural_geometry_hit_group_data.miscellaneous.street_lamp_collider_properties.lamp_aabb, street_lamp->aabb);
					OptixAabb_ext::copy(optix_state.hitgroup_records[sbt_idx].data.aabb, *collider_aabb);

					j++;

					procedural_geometry_req_memory += sizeof(HitGroupData) - sizeof(TraditionalGeometryHitGroupData);
				}
			}

			scene_memory_measurer.set_procedural_sbt_memory(procedural_geometry_req_memory);

			int k = 0;
			size_t traditional_geometry_req_memory = 0;

			if (!ignore_terrain && !procedural_terrain)
			{
				const int sbt_idx = j * RAY_TYPE_COUNT + 0;  // SBT for radiance ray-type for ith material

				OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.normal_geometry_radiance_hit_prog_group, &optix_state.hitgroup_records[sbt_idx]));
				optix_state.hitgroup_records[sbt_idx].data.geometry_type = GeometryType::gt_TRADITIONAL;
				optix_state.hitgroup_records[sbt_idx].data.seed = j;
				optix_state.hitgroup_records[sbt_idx].data.scene_object_type = SceneObjectType::TERRAIN;
				optix_state.hitgroup_records[sbt_idx].data.traditional_geometry_hit_group_data.vertices = reinterpret_cast<float3*>(optix_state.vertex_buffers[k]);

				j++;
				k++;

				traditional_geometry_req_memory += sizeof(HitGroupData) - sizeof(ProceduralGeometryHitGroupData);
			}

			if (!ignore_buildings && !procedural_buildings)
			{
				int b_i = 0;
				for (auto& building : scene.osm_city->get_buildings())
				{
					const int sbt_idx = j * RAY_TYPE_COUNT + 0;  // SBT for radiance ray-type for ith material

					OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.normal_geometry_radiance_hit_prog_group, &optix_state.hitgroup_records[sbt_idx]));

					{
						optix_state.hitgroup_records[sbt_idx].data.geometry_type = GeometryType::gt_TRADITIONAL;
						optix_state.hitgroup_records[sbt_idx].data.seed = j;
						optix_state.hitgroup_records[sbt_idx].data.scene_object_type = SceneObjectType::BUILDING;
						optix_state.hitgroup_records[sbt_idx].data.traditional_geometry_hit_group_data.vertices = reinterpret_cast<float3*>(optix_state.vertex_buffers[k]);
					}

					int building_object_parts_n = scene.building_parts[b_i].size();
					SceneObjectPart* building_object_parts = new SceneObjectPart[building_object_parts_n];
					int* building_object_part_ids = new int[building_object_parts_n];
					float2* building_object_parts_UVs = new float2[building_object_parts_n * 3];

					for (int bp_i = 0; bp_i < scene.building_parts[b_i].size(); bp_i++)
					{
						auto& p = scene.building_parts[b_i][bp_i];
						building_object_parts[bp_i] = p.scene_object_part;
						building_object_part_ids[bp_i] = p.scene_object_part_id;
						building_object_parts_UVs[bp_i * 3 + 0] = { p.UVs[0].x, p.UVs[0].y };
						building_object_parts_UVs[bp_i * 3 + 1] = { p.UVs[1].x, p.UVs[1].y };
						building_object_parts_UVs[bp_i * 3 + 2] = { p.UVs[2].x, p.UVs[2].y };
					}

					cudaMallocAndMemcpy<SceneObjectPart>(
						reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.traditional_geometry_hit_group_data.object_parts),
						reinterpret_cast<void**>(&building_object_parts),
						static_cast<int>(building_object_parts_n)
						);
					optix_state.cuda_allocated_sbt_memory.push_back(
						reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.traditional_geometry_hit_group_data.object_parts)
					);

					cudaMallocAndMemcpy<SceneObjectPart>(
						reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.traditional_geometry_hit_group_data.object_part_ids),
						reinterpret_cast<void**>(&building_object_part_ids),
						static_cast<int>(building_object_parts_n)
						);
					optix_state.cuda_allocated_sbt_memory.push_back(
						reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.traditional_geometry_hit_group_data.object_part_ids)
					);

					cudaMallocAndMemcpy<float2>(
						reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.traditional_geometry_hit_group_data.UVs),
						reinterpret_cast<void**>(&building_object_parts_UVs),
						static_cast<int>(building_object_parts_n * 3)
						);
					optix_state.cuda_allocated_sbt_memory.push_back(
						reinterpret_cast<void**>(&optix_state.hitgroup_records[sbt_idx].data.traditional_geometry_hit_group_data.UVs)
					);

					delete[] building_object_parts;
					delete[] building_object_part_ids;
					delete[] building_object_parts_UVs;

					j++;
					k++;
					b_i++;

					traditional_geometry_req_memory += sizeof(HitGroupData) - sizeof(ProceduralGeometryHitGroupData);
					traditional_geometry_req_memory += sizeof(int) * building_object_parts_n;
					traditional_geometry_req_memory += sizeof(SceneObjectPart) * building_object_parts_n;
					traditional_geometry_req_memory += sizeof(float2) * building_object_parts_n;
				}
			}

			if (!ignore_edge_roads && !procedural_edge_roads)
			{
				for (auto& edge_road : scene.osm_city->get_edge_roads())
				{
					const int sbt_idx = j * RAY_TYPE_COUNT + 0;  // SBT for radiance ray-type for ith material

					OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.normal_geometry_radiance_hit_prog_group, &optix_state.hitgroup_records[sbt_idx]));
					optix_state.hitgroup_records[sbt_idx].data.geometry_type = GeometryType::gt_TRADITIONAL;
					optix_state.hitgroup_records[sbt_idx].data.seed = j;
					optix_state.hitgroup_records[sbt_idx].data.scene_object_type = SceneObjectType::EDGE_ROAD;
					optix_state.hitgroup_records[sbt_idx].data.traditional_geometry_hit_group_data.vertices = reinterpret_cast<float3*>(optix_state.vertex_buffers[k]);

					j++;
					k++;

					traditional_geometry_req_memory += sizeof(HitGroupData) - sizeof(ProceduralGeometryHitGroupData);
				}
			}

			if (!ignore_joint_roads && !procedural_joint_roads)
			{
				for (auto& joint_road : scene.osm_city->get_joint_roads())
				{
					const int sbt_idx = j * RAY_TYPE_COUNT + 0;  // SBT for radiance ray-type for ith material

					OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.normal_geometry_radiance_hit_prog_group, &optix_state.hitgroup_records[sbt_idx]));
					optix_state.hitgroup_records[sbt_idx].data.geometry_type = GeometryType::gt_TRADITIONAL;
					optix_state.hitgroup_records[sbt_idx].data.seed = j;
					optix_state.hitgroup_records[sbt_idx].data.scene_object_type = SceneObjectType::JOINT_ROAD;
					optix_state.hitgroup_records[sbt_idx].data.traditional_geometry_hit_group_data.vertices = reinterpret_cast<float3*>(optix_state.vertex_buffers[k]);

					j++;
					k++;

					traditional_geometry_req_memory += sizeof(HitGroupData) - sizeof(ProceduralGeometryHitGroupData);
				}
			}

			if (!ignore_sidewalks && !procedural_sidewalks)
			{
				for (auto& sidewalk : scene.osm_city->get_sidewalks())
				{
					const int sbt_idx = j * RAY_TYPE_COUNT + 0;  // SBT for radiance ray-type for ith material

					OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.normal_geometry_radiance_hit_prog_group, &optix_state.hitgroup_records[sbt_idx]));
					optix_state.hitgroup_records[sbt_idx].data.geometry_type = GeometryType::gt_TRADITIONAL;
					optix_state.hitgroup_records[sbt_idx].data.seed = j;
					optix_state.hitgroup_records[sbt_idx].data.scene_object_type = SceneObjectType::SIDEWALK;
					optix_state.hitgroup_records[sbt_idx].data.traditional_geometry_hit_group_data.vertices = reinterpret_cast<float3*>(optix_state.vertex_buffers[k]);

					j++;
					k++;

					traditional_geometry_req_memory += sizeof(HitGroupData) - sizeof(ProceduralGeometryHitGroupData);
				}
			}

			scene_memory_measurer.set_traditional_sbt_memory(traditional_geometry_req_memory);
		}

		scene.building_parts.clear();
		scene.building_parts.shrink_to_fit();

		CUDA_CHECK(cudaMemcpy(
			reinterpret_cast<void*>(d_hitgroup_records),
			optix_state.hitgroup_records.data(),
			hitgroup_record_size * RAY_TYPE_COUNT * number_of_objects,
			cudaMemcpyHostToDevice
		));

		optix_state.sbt.raygenRecord = d_raygen_record;
		optix_state.sbt.missRecordBase = d_miss_records;
		optix_state.sbt.missRecordStrideInBytes = static_cast<uint32_t>(miss_record_size);
		optix_state.sbt.missRecordCount = RAY_TYPE_COUNT;
		optix_state.sbt.hitgroupRecordBase = d_hitgroup_records;
		optix_state.sbt.hitgroupRecordStrideInBytes = static_cast<uint32_t>(hitgroup_record_size);
		optix_state.sbt.hitgroupRecordCount = RAY_TYPE_COUNT * number_of_objects;
	}

	void Program::initialize_launch_parameters()
	{
		optix_state.params.width = window_state.width;
		optix_state.params.height = window_state.height;

		CUDA_CHECK(cudaMalloc(
			reinterpret_cast<void**>(&optix_state.params.accum_buffer),
			optix_state.params.width * optix_state.params.height * sizeof(float4)
		));
		optix_state.params.frame_buffer = nullptr;  // Will be set when output buffer is mapped

		optix_state.skydome_parameters.texture_color_factor = make_float3(1.0f);
		optix_state.skydome_parameters.diffuse_color_factor = make_float3(1.0f);
		optix_state.skydome_parameters.emissive_color = make_float3(0, 0, 0);
		optix_state.skydome_parameters.texture_number = 0;

		optix_state.current_ray_parameters.samples_per_launch = 1;
		optix_state.current_ray_parameters.russian_roulette_depth = 3;
		optix_state.current_ray_parameters.ray_termination_probability = 0.5f;
		optix_state.image_rendering_ray_parameters = optix_state.current_ray_parameters;
		optix_state.params.ray_parameters = optix_state.current_ray_parameters;

		optix_state.params.subframe_index = 0u;

		optix_state.image_rendering_scene_parameters = optix_state.current_procedural_geometry_generation_parameters;
		optix_state.params.procedural_geometry_generation_parameters = optix_state.current_procedural_geometry_generation_parameters;

		optix_state.params.procedural_geometry_traversable = optix_state.hIAS;
		optix_state.params.normal_geometry_traversable = optix_state.traditional_geometry_gas_handle;

		if (!procedural_buildings)
			optix_state.params.procedural_geometry_generation_parameters.render_building_aabbs = false;

		optix_state.parameter_changed = true;

		CUDA_CHECK(cudaStreamCreate(&optix_state.stream));
		CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(&optix_state.d_params), sizeof(Parameters)));
	}

	void Program::launch_subframe(sutil::CUDAOutputBuffer<uchar4>& output_buffer)
	{	
		uchar4* result_buffer_data = output_buffer.map();
		optix_state.params.frame_buffer = result_buffer_data;
		CUDA_CHECK(cudaMemcpyAsync(
			reinterpret_cast<void*>(optix_state.d_params),
			&optix_state.params, sizeof(Parameters),
			cudaMemcpyHostToDevice, optix_state.stream
		));

		OPTIX_CHECK(optixLaunch(
			optix_state.pipeline,
			optix_state.stream,
			reinterpret_cast<CUdeviceptr>(optix_state.d_params),
			sizeof(Parameters),
			&optix_state.sbt,
			optix_state.params.width,   // launch width
			optix_state.params.height,  // launch height
			1                     // launch depth
		));

		output_buffer.unmap();
		CUDA_SYNC_CHECK();
	}

	void Program::display_subframe(sutil::CUDAOutputBuffer<uchar4>& output_buffer, sutil::GLDisplay& gl_display, GLFWwindow* window)
	{
		int framebuf_res_x = 0;  // The display's resolution (could be HDPI res)
		int framebuf_res_y = 0;  //
		glfwGetFramebufferSize(window, &framebuf_res_x, &framebuf_res_y);
		gl_display.display(
			output_buffer.width(),
			output_buffer.height(),
			framebuf_res_x,
			framebuf_res_y,
			output_buffer.getPBO()
		);
	}

	void Program::update_state(sutil::CUDAOutputBuffer<uchar4>& output_buffer)
	{
		{
			if (window_state.window_resized)
			{
				optix_state.params.subframe_index = 0;

				window_state.window_resized = false;

				optix_state.params.width = window_state.width;
				optix_state.params.height = window_state.height;

				output_buffer.resize(window_state.width, window_state.height);

				// Realloc accumulation buffer
				CUDA_CHECK(cudaFree(reinterpret_cast<void*>(optix_state.params.accum_buffer)));
				CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(&optix_state.params.accum_buffer), optix_state.params.width * optix_state.params.height * sizeof(float4)));
			}
		}

		{
			if (scene.camera_changed)
			{
				optix_state.params.subframe_index = 0;

				scene.camera_changed = false;

				scene.camera.setAspectRatio(static_cast<float>(window_state.width) / static_cast<float>(window_state.height));
				optix_state.params.eye = scene.camera.eye();
				scene.camera.UVWFrame(optix_state.params.U, optix_state.params.V, optix_state.params.W);
			}
		}

		{
			if (optix_state.parameter_changed)
			{
				optix_state.parameter_changed = false;

				const size_t miss_record_size = sizeof(MissRecord);

				optix_state.params.procedural_geometry_generation_parameters = optix_state.current_procedural_geometry_generation_parameters;
				optix_state.params.ray_parameters = optix_state.current_ray_parameters;

				MissRecord ms_sbt;
				OPTIX_CHECK(optixSbtRecordPackHeader(optix_state.radiance_miss_prog_group, &ms_sbt));
				ms_sbt.data.skydome_parameters.texture_object = scene.skydome_textures[optix_state.skydome_parameters.texture_number].cuda_texture_object;
				ms_sbt.data.skydome_parameters.emissive_color = optix_state.skydome_parameters.emissive_color;
				ms_sbt.data.skydome_parameters.texture_color_factor = optix_state.skydome_parameters.texture_color_factor;
				ms_sbt.data.skydome_parameters.diffuse_color_factor = optix_state.skydome_parameters.diffuse_color_factor;

				CUDA_CHECK(cudaMemcpy(
					reinterpret_cast<void*>(optix_state.sbt.missRecordBase),
					&ms_sbt,
					miss_record_size * RAY_TYPE_COUNT,
					cudaMemcpyHostToDevice
				));

				optix_state.params.subframe_index = 0;
			}
		}
	}

	static void create_cuda_texture_hdr(
		float* float3_pixels, 
		unsigned int width, unsigned int height, 
		cudaArray_t& cuda_texture_pixel_array, cudaTextureObject_t& cuda_texture_object
	)
	{
		cudaResourceDesc res_desc = {};
		cudaChannelFormatDesc channel_desc;
		int32_t numComponents = 4;
		int32_t pitch = width * numComponents * sizeof(float);
		channel_desc = cudaCreateChannelDesc<float4>();

		CUDA_CHECK(cudaMallocArray(&cuda_texture_pixel_array, &channel_desc, width, height));

		CUDA_CHECK(cudaMemcpy2DToArray(cuda_texture_pixel_array,
			/* offset */0, 0,
			float3_pixels,
			pitch, pitch, height,
			cudaMemcpyHostToDevice
		));
		res_desc.resType = cudaResourceTypeArray;
		res_desc.res.array.array = cuda_texture_pixel_array;

		cudaTextureDesc tex_desc = {};
		tex_desc.addressMode[0] = cudaAddressModeWrap;
		tex_desc.addressMode[1] = cudaAddressModeWrap;
		tex_desc.filterMode = cudaFilterModeLinear;
		tex_desc.readMode = cudaReadModeElementType;
		tex_desc.normalizedCoords = 1;
		tex_desc.maxAnisotropy = 1;
		tex_desc.maxMipmapLevelClamp = 99;
		tex_desc.minMipmapLevelClamp = 0;
		tex_desc.mipmapFilterMode = cudaFilterModeLinear;
		tex_desc.borderColor[0] = 0.0f;
		tex_desc.borderColor[1] = 0.0f;
		tex_desc.borderColor[2] = 0.0f;
		tex_desc.borderColor[3] = 1.0f;
		tex_desc.sRGB = 0;

		CUDA_CHECK(cudaCreateTextureObject(&cuda_texture_object, &res_desc, &tex_desc, nullptr));
	}

	void Program::create_skydome_textures()
	{
		int number_of_skyboxes = paths_to_skydomes.size() + 1;

		optix_state.skydome_parameters.texture_number = 0;
		scene.skydome_textures.resize(number_of_skyboxes);

		{
			const int black_texture_width = 4096;
			const int black_texture_height = 2048;

			std::vector<float> black_texture;

			{
				std::vector<float4> black_texture_tmp;
				TextureCreator::create_pseudo_night_sky_texture(black_texture_tmp, black_texture_width, black_texture_height, 0.001f, global_seed);

				TextureCreator::float4_to_float(black_texture, black_texture_tmp, black_texture_width, black_texture_height);
			}

			create_cuda_texture_hdr(
				black_texture.data(), 
				black_texture_width,
				black_texture_height,
				scene.skydome_textures[0].cuda_pixel_array,
				scene.skydome_textures[0].cuda_texture_object
			);
		}

		{
			for (int i = 0; i < paths_to_skydomes.size(); i++)
			{
				int current_texture_id = std::count_if(
					scene.skydome_textures.begin(), 
					scene.skydome_textures.end(), 
					[](TextureInstance i) { return i.cuda_pixel_array != nullptr; }
				);

				int32_t width;
				int32_t height;

				float* texture_pixels = nullptr;

				{
					HDRLoaderResult* hdr_loader_result = new HDRLoaderResult();

					bool result = false;

					std::string path = resources_folder + "/" + skydomes_folder + "/" + paths_to_skydomes[i];
					result = HDRLoader::load(sutil::sampleDataFilePath(path.c_str()), *hdr_loader_result);

					if (!result)
					{
						throw new std::exception("Texture loading failed.\n");
					}
					else
					{
						width = hdr_loader_result->width;
						height = hdr_loader_result->height;

						texture_pixels = new float[height * width * 4];
						//printf("%d %d\n", height, width);

						for (int y = 0; y < height; y++)
						{
							for (int x = 0; x < width; x++)
							{
								// The image is flipped, so use height - 1 - y.
								int index_in_load_res = ((height - 1 - y) * width + x) * 3;
								int index_in_texture_array = (y * width + x) * 4;

								float r_float = hdr_loader_result->cols[index_in_load_res];
								float g_float = hdr_loader_result->cols[index_in_load_res + 1];
								float b_float = hdr_loader_result->cols[index_in_load_res + 2];

								texture_pixels[index_in_texture_array + 0] = r_float;
								texture_pixels[index_in_texture_array + 1] = g_float;
								texture_pixels[index_in_texture_array + 2] = b_float;
								texture_pixels[index_in_texture_array + 3] = 0.0f;
							}
						}
					}

					delete hdr_loader_result;
				}				

				{
					create_cuda_texture_hdr(
						texture_pixels,
						width,
						height, 
						scene.skydome_textures[current_texture_id].cuda_pixel_array, 
						scene.skydome_textures[current_texture_id].cuda_texture_object
					);
				}

				delete[] texture_pixels;
			}
		}
	}

	static void load_and_create_cuda_texture_png(const std::string path, cudaArray_t& cuda_pixel_array, cudaTextureObject_t& cuda_texture_object)
	{
		sutil::ImageBuffer image = sutil::loadImage(sutil::sampleDataFilePath(path.c_str()));

		cudaResourceDesc res_desc = {};
		cudaChannelFormatDesc channel_desc;
		int32_t numComponents = 4;
		int32_t pitch = image.width * numComponents * sizeof(uint8_t);
		channel_desc = cudaCreateChannelDesc<uchar4>();
		CUDA_CHECK(cudaMallocArray(&cuda_pixel_array, &channel_desc, image.width, image.height));

		CUDA_CHECK(cudaMemcpy2DToArray(
			cuda_pixel_array,
			/* offset */0, 0,
			image.data,
			pitch, pitch, image.height,
			cudaMemcpyHostToDevice
		));
		res_desc.resType = cudaResourceTypeArray;
		res_desc.res.array.array = cuda_pixel_array;

		cudaTextureDesc tex_desc = {};
		tex_desc.addressMode[0] = cudaAddressModeWrap;
		tex_desc.addressMode[1] = cudaAddressModeWrap;
		tex_desc.filterMode = cudaFilterModeLinear;
		tex_desc.readMode = cudaReadModeNormalizedFloat;
		tex_desc.normalizedCoords = 1;
		tex_desc.maxAnisotropy = 1;
		tex_desc.maxMipmapLevelClamp = 99;
		tex_desc.minMipmapLevelClamp = 0;
		tex_desc.mipmapFilterMode = cudaFilterModePoint;
		tex_desc.borderColor[0] = 1.0f;
		tex_desc.sRGB = 10;

		CUDA_CHECK(cudaCreateTextureObject(&cuda_texture_object, &res_desc, &tex_desc, nullptr));

		delete[] image.data;
	}

	static void create_material(const TexturedDiffuseMaterialCreationParameters& material_creation_parameters, const cudaTextureObject_t& cuda_texture_object, Material& material_for_cuda)
	{
		material_for_cuda.has_texture = true;
		material_for_cuda.texture.t_h = material_creation_parameters.t_h;
		material_for_cuda.texture.t_v = material_creation_parameters.t_v;
		material_for_cuda.diffuse_reflection_parameters.k_s = material_creation_parameters.k_s;
		material_for_cuda.diffuse_reflection_parameters.n_s = material_creation_parameters.n_s;
		material_for_cuda.diffuse_reflection_parameters.k_d = material_creation_parameters.k_d;
		material_for_cuda.texture.texture_object = cuda_texture_object;
	}

	static void create_material(const NonTexturedDiffuseMaterialCreationParameters& material_creation_parameters, Material& material_for_cuda)
	{
		material_for_cuda.has_texture = false;
		material_for_cuda.diffuse_color = material_creation_parameters.diffuse_color;
		material_for_cuda.emissive_color = material_creation_parameters.emissive_color;
		material_for_cuda.diffuse_reflection_parameters.k_s = material_creation_parameters.k_s;
		material_for_cuda.diffuse_reflection_parameters.n_s = material_creation_parameters.n_s;
		material_for_cuda.diffuse_reflection_parameters.k_d = material_creation_parameters.k_d;
	}

	static void create_material(const NonTexturedGlassMaterialCreationParameters& material_creation_parameters, Material& material_for_cuda)
	{
		material_for_cuda.has_texture = false;
		material_for_cuda.glass_parameters.refractive_index = material_creation_parameters.refractive_index;
		material_for_cuda.glass_parameters.fresnel_exponent = material_creation_parameters.fresnel_exponent;
		material_for_cuda.glass_parameters.fresnel_minimum = material_creation_parameters.fresnel_minimum;
		material_for_cuda.glass_parameters.fresnel_maximum = material_creation_parameters.fresnel_maximum;
		material_for_cuda.diffuse_color = window_glass_material_creation_parameters.diffuse_color;
		material_for_cuda.emissive_color = window_glass_material_creation_parameters.emissive_color;
	}

	void Program::create_building_materials()
	{
		int number_of_regular_building_materials = regular_building_material_creation_parameters.size();

		scene.building_textures.resize(number_of_regular_building_materials);

		for (int i = 0; i < number_of_regular_building_materials; i++)
		{
			load_and_create_cuda_texture_png(
				resources_folder + "/" + textures_folder + "/" + regular_building_material_creation_parameters[i].path_to_texture,
				scene.building_textures[i].cuda_pixel_array,
				scene.building_textures[i].cuda_texture_object
			);
		}

		{
			optix_state.params.number_of_regular_building_materials = number_of_regular_building_materials;

			Material* regular_building_materials = new Material[number_of_regular_building_materials];
			for (int i = 0; i < number_of_regular_building_materials; i++)
			{
				create_material(regular_building_material_creation_parameters[i], scene.building_textures[i].cuda_texture_object, regular_building_materials[i]);
			}

			CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(&optix_state.params.regular_bulding_materials), number_of_regular_building_materials * sizeof(Material)));
			CUDA_CHECK(cudaMemcpy(
				reinterpret_cast<void*>(optix_state.params.regular_bulding_materials), 
				reinterpret_cast<void*>(regular_building_materials),
				number_of_regular_building_materials * sizeof(Material),
				cudaMemcpyHostToDevice
			));

			delete[] regular_building_materials;
		}

		create_material(balcony_fence_material_creation_parameters, optix_state.params.balcony_fence_material);

		{
			int number_of_inside_room_materials = inside_room_material_creation_parameters.size();
			optix_state.params.number_of_inside_room_materials = number_of_inside_room_materials;

			Material* inside_room_materials = new Material[number_of_inside_room_materials];
			for (int i = 0; i < number_of_inside_room_materials; i++)
			{
				create_material(inside_room_material_creation_parameters[i], inside_room_materials[i]);
			}

			CUDA_CHECK(cudaMalloc(reinterpret_cast<void**>(&optix_state.params.inside_room_materials), number_of_inside_room_materials * sizeof(Material)));
			CUDA_CHECK(cudaMemcpy(
				reinterpret_cast<void*>(optix_state.params.inside_room_materials),
				reinterpret_cast<void*>(inside_room_materials),
				number_of_inside_room_materials * sizeof(Material),
				cudaMemcpyHostToDevice
			));

			delete[] inside_room_materials;
		}
	}

	void Program::create_asphalt_material()
	{
		create_material(road_asphalt_material_creation_parameters, optix_state.params.road_asphalt_material);
		create_material(sidewalk_asphalt_material_creation_parameters, optix_state.params.sidewalk_asphalt_material);
	}	

	void Program::create_street_lamp_bulb_material()
	{
		create_material(street_lamp_bulb_material_creation_parameters, optix_state.params.street_lamp_bulb_material);
	}

	void Program::create_window_impost_material()
	{
		create_material(window_impost_material_creation_parameters, optix_state.params.window_impost_material);
	}

	void Program::create_grass_material()
	{
		load_and_create_cuda_texture_png(
			resources_folder + "/" + textures_folder + "/" + grass_material_creation_parameters.path_to_texture,
			scene.grass_texture.cuda_pixel_array, 
			scene.grass_texture.cuda_texture_object
		);

		create_material(grass_material_creation_parameters, scene.grass_texture.cuda_texture_object, optix_state.params.grass_material);
	}

	void Program::create_street_lamp_material()
	{
		create_material(lamp_material_creation_parameters, optix_state.params.lamp_material);
	}

	void Program::create_glass_materials()
	{		
		create_material(window_glass_material_creation_parameters, optix_state.params.window_glass);
		create_material(balcony_glass_material_creation_parameters, optix_state.params.balcony_glass);
		create_material(street_lamp_glass_material_creation_parameters, optix_state.params.street_lamp_glass);
	}

	void Program::cleanup()
	{
		OPTIX_CHECK(optixPipelineDestroy(optix_state.pipeline));
		OPTIX_CHECK(optixProgramGroupDestroy(optix_state.raygen_prog_group));
		OPTIX_CHECK(optixProgramGroupDestroy(optix_state.radiance_miss_prog_group));
		OPTIX_CHECK(optixProgramGroupDestroy(optix_state.procedural_geometry_radiance_hit_prog_group));
		OPTIX_CHECK(optixProgramGroupDestroy(optix_state.normal_geometry_radiance_hit_prog_group));
		OPTIX_CHECK(optixModuleDestroy(optix_state.module));
		OPTIX_CHECK(optixDeviceContextDestroy(optix_state.context));

		CUDA_CHECK(cudaFree(reinterpret_cast<void*>(optix_state.sbt.raygenRecord)));
		CUDA_CHECK(cudaFree(reinterpret_cast<void*>(optix_state.sbt.missRecordBase)));
		CUDA_CHECK(cudaFree(reinterpret_cast<void*>(optix_state.sbt.hitgroupRecordBase)));
		CUDA_CHECK(cudaFree(reinterpret_cast<void*>(optix_state.deviceBufferIAS)));
		CUDA_CHECK(cudaFree(reinterpret_cast<void*>(optix_state.procedural_geometry_gas_buffer)));
		CUDA_CHECK(cudaFree(reinterpret_cast<void*>(optix_state.traditional_geometry_gas_buffer)));
		CUDA_CHECK(cudaFree(reinterpret_cast<void*>(optix_state.params.accum_buffer)));
		CUDA_CHECK(cudaFree(reinterpret_cast<void*>(optix_state.params.regular_bulding_materials)));
		CUDA_CHECK(cudaFree(reinterpret_cast<void*>(optix_state.params.inside_room_materials)));
		CUDA_CHECK(cudaFree(reinterpret_cast<void*>(optix_state.d_params)));

		for (int i = 0; i < optix_state.cuda_allocated_sbt_memory.size(); i++)
		{
			auto& p = optix_state.cuda_allocated_sbt_memory[i];
			CUDA_CHECK(cudaFree(*p));
		}

		{
			int number_of_textures = scene.skydome_textures.size();

			for (int i = 0; i < scene.skydome_textures.size(); i++)
			{
				CUDA_CHECK(cudaDestroyTextureObject(scene.skydome_textures[i].cuda_texture_object));
				CUDA_CHECK(cudaFreeArray(scene.skydome_textures[i].cuda_pixel_array));
			}
		}

		if (optix_state.params.grass_material.has_texture)
		{
			int number_of_textures = scene.building_textures.size();

			for (int i = 0; i < scene.building_textures.size(); i++)
			{
				CUDA_CHECK(cudaDestroyTextureObject(scene.building_textures[i].cuda_texture_object));
				CUDA_CHECK(cudaFreeArray(scene.building_textures[i].cuda_pixel_array));
			}

			CUDA_CHECK(cudaDestroyTextureObject(scene.grass_texture.cuda_texture_object));
			CUDA_CHECK(cudaFreeArray(scene.grass_texture.cuda_pixel_array));
		}

		for (int i = 0; i < get_number_of_objects_with_procedural_geometry(); i++)
			CUDA_CHECK(cudaFree(reinterpret_cast<void*>(optix_state.aabb_buffers[i])));

		for (int i = 0; i < get_number_of_objects_with_normal_geometry(); i++)
			CUDA_CHECK(cudaFree(reinterpret_cast<void*>(optix_state.vertex_buffers[i])));

		delete scene.osm_city;
	}

	void Program::initialize_gui(GLFWwindow* window)
	{
		// Setup Dear ImGui context
		IMGUI_CHECKVERSION();
		ImGui::CreateContext();
		ImGuiIO& io = ImGui::GetIO();
		// Setup Platform/Renderer bindings
		ImGui_ImplGlfw_InitForOpenGL(window, true);
		ImGui_ImplOpenGL3_Init();
		// Setup Dear ImGui style
		ImGui::StyleColorsDark();
		ImGui::GetStyle().WindowBorderSize = 0.0f;
	}

	static void ImGui_NewFrame(
		const float background_alpha, 
		const int window_position_x, const int window_position_y, 
		const int window_size_x, const int window_size_y,
		const char* name, const ImGuiWindowFlags flags
	)
	{
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();
		ImGui::SetNextWindowBgAlpha(background_alpha);
		ImGui::SetNextWindowPos(ImVec2(window_position_x, window_position_y));
		ImGui::SetNextWindowSize(ImVec2(window_size_x, window_size_y));
		ImGui::Begin(name, nullptr, flags);
	}

	static void ImGui_EndFrame_and_Render()
	{
		ImGui::End();
		// Render dear imgui into screen
		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	}

	static bool display_gui_for_number_of_samples(std::string atribute_name, int& current_number_of_samples)
	{
		ImGui::TextColored(ImColor(0.7f, 0.7f, 0.7f, 1.0f), "%s", "Number of samples");
		auto ret = ImGui::InputInt(atribute_name.c_str(), &current_number_of_samples);

		if (ret)
		{
			if (current_number_of_samples < 1)
				current_number_of_samples = 1;
		}

		return ret;
	}

	static bool display_gui_for_russian_roulette_depth(std::string atribute_name, int& current_russian_roulette_depth)
	{
		ImGui::TextColored(ImColor(0.7f, 0.7f, 0.7f, 1.0f), "%s", "Russian roulette depth");
		auto ret = ImGui::InputInt(atribute_name.c_str(), &current_russian_roulette_depth);

		if (ret)
		{
			if (current_russian_roulette_depth < 1)
				current_russian_roulette_depth = 1;
		}

		return ret;
	}

	static bool display_gui_for_ray_termination_probability(std::string atribute_name, float& current_ray_termination_probability)
	{
		const float ray_termination_probability_min = 0.01f;
		const float ray_termination_probability_max = 0.99f;
		ImGui::TextColored(ImColor(0.7f, 0.7f, 0.7f, 1.0f), "%s", "Ray termination");
		ImGui::TextColored(ImColor(0.7f, 0.7f, 0.7f, 1.0f), "%s", "probability");
		return ImGui::SliderFloat(
			atribute_name.c_str(),
			&current_ray_termination_probability,
			ray_termination_probability_min,
			ray_termination_probability_max
		);
	}

	void Program::display_gui()
	{		
		optix_state.parameter_changed = false;
		window_state.mouse_hovering_gui = false;

		{
			const auto text_color1 = ImColor(1.0f, 1.0f, 1.0f, 1.0f);
			const auto text_color2 = ImColor(0.7f, 0.7f, 0.7f, 1.0f);
			const auto text_color3 = ImColor(0.6f, 0.6f, 0.6f, 1.0f);

			const int major_space = 40;
			const int medium_space = 20;
			const int minor_space = 10;

			const int menu_position_x = 10;
			const int menu_position_y = 100;

			const int menu_size_x = 220;
			const int menu_size_y = min(static_cast<unsigned int>(600), static_cast<unsigned int>(window_state.height - menu_position_y));

			const int scroll_bar_width = 30;

			ImGui_NewFrame(
				1.0f, 
				menu_position_x,
				menu_position_y,
				menu_size_x, 
				menu_size_y, 
				"##gui_for_variable_parameters", ImGuiWindowFlags_NoResize/* | ImGuiWindowFlags_NoScrollbar*/ | ImGuiWindowFlags_NoSavedSettings
			);

			ImGui::PushItemWidth(menu_size_x - scroll_bar_width);

			window_state.mouse_hovering_gui = max(ImGui::IsMouseHoveringWindow(), window_state.mouse_hovering_gui);

			ImGui::Separator();
			ImGui::TextColored(text_color1, "%s", "Path tracing parameters");
			ImGui::TextColored(text_color1, "%s", "Real time");
			ImGui::Separator();

			{
				bool changed = display_gui_for_number_of_samples("##real_time_number_of_samples", optix_state.current_ray_parameters.samples_per_launch);
				optix_state.current_ray_parameters.samples_per_launch = max(0, optix_state.current_ray_parameters.samples_per_launch);
				optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
			}

			{
				bool changed = display_gui_for_russian_roulette_depth("##real_time_russian_roulette_depth", optix_state.current_ray_parameters.russian_roulette_depth);
				optix_state.current_ray_parameters.russian_roulette_depth = max(0, optix_state.current_ray_parameters.russian_roulette_depth);
				optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
			}

			{
				bool changed = display_gui_for_ray_termination_probability("##real_time_ray_termination_probability", optix_state.current_ray_parameters.ray_termination_probability);
				optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
			}

			ImGui::Separator();

			ImGui::Dummy(ImVec2(0, major_space));
			ImGui::Separator();
			ImGui::TextColored(text_color1, "%s", "Path tracing parameters");
			ImGui::TextColored(text_color1, "%s", "Image rendering");
			ImGui::Separator();

			{
				display_gui_for_number_of_samples("##image_rendering_number_of_samples", optix_state.image_rendering_ray_parameters.samples_per_launch);
				optix_state.image_rendering_ray_parameters.samples_per_launch = max(0, optix_state.image_rendering_ray_parameters.samples_per_launch);
			}

			{
				display_gui_for_russian_roulette_depth("##image_rendering_russian_roulette_depth", optix_state.image_rendering_ray_parameters.russian_roulette_depth);
				optix_state.image_rendering_ray_parameters.russian_roulette_depth = max(0, optix_state.image_rendering_ray_parameters.russian_roulette_depth);
			}

			{
				display_gui_for_ray_termination_probability("##image_rendering_ray_termination_probability", optix_state.image_rendering_ray_parameters.ray_termination_probability);
			}			

			ImGui::Separator();

			ImGui::Dummy(ImVec2(0, major_space));

			ImGui::Separator();
			ImGui::TextColored(text_color1, "%s", "Scene parameters");
			ImGui::Separator();

			{
				ImGui::TextColored(text_color2, "%s", "Skydome");
				bool changed = ImGui::InputInt("##current_skydome_number", &optix_state.skydome_parameters.texture_number);

				optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
			}

			{
				ImGui::TextColored(text_color2, "%s", "Skydome texture color");
				ImGui::TextColored(text_color2, "%s", "factor");
				bool changed = ImGui::SliderFloat3(
					"##slider_skydome_texture_color_factor",
					reinterpret_cast<float*>(&optix_state.skydome_parameters.texture_color_factor),
					0.0f,
					2.0f
				);

				optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
			}

			{
				ImGui::TextColored(text_color2, "%s", "Skydome diffuse color");
				ImGui::TextColored(text_color2, "%s", "factor");
				bool changed = ImGui::SliderFloat3(
					"##slider_skydome_diffuse_color_factor",
					reinterpret_cast<float*>(&optix_state.skydome_parameters.diffuse_color_factor),
					0.0f,
					2.0f
				);

				optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
			}

			{
				ImGui::TextColored(text_color2, "%s", "Skydome emissive color");
				bool changed = ImGui::SliderFloat3(
					"##slider_skydome_emissive_color",
					reinterpret_cast<float*>(&optix_state.skydome_parameters.emissive_color),
					0.0f,
					1.0f
				);

				optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
			}

			ImGui::Dummy(ImVec2(0, medium_space));
			// Perlin noise.
			ImGui::Separator();
			ImGui::TextColored(text_color2, "%s", "Perlin noise parameters");
			ImGui::Separator();
			{
				{
					ImGui::TextColored(text_color3, "%s", "Scale");
					bool changed = ImGui::SliderFloat(
						"##slider_perlin_noise_scale",
						&optix_state.params.perlin_noise_scale,
						0.0f,
						10.0f
					);

					optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
				}

				{
					ImGui::TextColored(text_color3, "%s", "N");
					bool changed = ImGui::SliderInt(
						"##slider_perlin_noise_n",
						&optix_state.params.perlin_noise_n,
						1,
						80
					);

					optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
				}

				{
					ImGui::TextColored(text_color3, "%s", "Lacunarity");
					bool changed = ImGui::SliderFloat(
						"##slider_perlin_noise_lacunarity",
						&optix_state.params.perlin_noise_lacunarity,
						0.0f,
						4.0f
					);

					optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
				}

				{
					ImGui::TextColored(text_color2, "%s", "Decay");
					bool changed = ImGui::SliderFloat(
						"##slider_perlin_noise_decay",
						&optix_state.params.perlin_noise_decay,
						0.0f,
						2.0f
					);

					optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
				}

				{
					ImGui::TextColored(text_color2, "%s", "Minimum");
					bool changed = ImGui::SliderFloat(
						"##slider_perlin_noise_min",
						&optix_state.params.perlin_noise_min,
						0.0f,
						4.0f
					);

					optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
				}

				{
					ImGui::TextColored(text_color2, "%s", "Maximum");
					bool changed = ImGui::SliderFloat(
						"##slider_perlin_noise_max",
						&optix_state.params.perlin_noise_max,
						0.0f,
						4.0f
					);

					optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
				}
			}

			ImGui::Dummy(ImVec2(0, medium_space));
			{
				ImGui::TextColored(text_color2, "%s", "Windows with");
				ImGui::TextColored(text_color2, "%s", "lights on");
				bool changed = ImGui::SliderFloat(
					"##slider_lighting_window_probability",
					&optix_state.params.window_with_lights_on_percentage,
					0.0f,
					1.0f
				);

				optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
			}

			{
				ImGui::TextColored(text_color2, "%s", "Window emissive");
				ImGui::TextColored(text_color2, "%s", "color factor");
				bool changed = ImGui::SliderFloat(
					"##slider_window_emissive_color_factor",
					&optix_state.params.window_emissive_color_factor,
					0.0f,
					1.0f
				);

				optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
			}

			{
				ImGui::TextColored(text_color2, "%s", "Shop windows with");
				ImGui::TextColored(text_color2, "%s", "lights on");
				bool changed = ImGui::SliderFloat(
					"##slider_lighting_in_shop_probability",
					&optix_state.params.shop_window_with_lights_on_percentage,
					0.0f,
					1.0f
				);

				optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
			}	

			{
				ImGui::TextColored(text_color2, "%s", "Shop window emissive");
				ImGui::TextColored(text_color2, "%s", "color factor");
				bool changed = ImGui::SliderFloat(
					"##slider_shop_window_emissive_color_factor",
					&optix_state.params.shop_window_emissive_color_factor,
					0.0f,
					1.0f
				);

				optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
			}

			ImGui::Dummy(ImVec2(0, medium_space));
			{
				ImGui::TextColored(text_color2, "%s", "Lamps on/off");
				bool changed = ImGui::Checkbox(
					"##check_box_lamps_on_off",
					&optix_state.params.street_lamps_on
				);

				optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
			}

			if (optix_state.params.street_lamps_on)
			{	
				{
					ImGui::TextColored(text_color2, "%s", "Don't use the");
					ImGui::TextColored(text_color2, "%s", "street lamp collider");
					bool changed = ImGui::Checkbox(
						"##check_box_street_lamp_colliders",
						&optix_state.params.dont_use_street_lamp_colliders
					);

					optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
				}

				{
					ImGui::TextColored(text_color2, "%s", "Street lamp");
					ImGui::TextColored(text_color2, "%s", "emissive color");
					bool changed = ImGui::SliderFloat3(
						"##slider_street_lamp_emissive_color",
						reinterpret_cast<float*>(&optix_state.params.street_lamp_emissive_color),
						0.0f,
						1.0f
					);

					optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
				}

				{
					ImGui::TextColored(text_color2, "%s", "Lamp emissive");
					ImGui::TextColored(text_color2, "%s", "energy factor");
					bool changed = ImGui::SliderFloat(
						"##slider_lamp_emissive_energy_factor",
						&optix_state.params.street_lamp_emissive_energy_factor,
						1.0f,
						5.0f
					);

					optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
				}

				{
					ImGui::TextColored(text_color2, "%s", "Lamp secondary emissive");
					ImGui::TextColored(text_color2, "%s", "energy factor");
					bool changed = ImGui::SliderFloat(
						"##slider_lamp_secondary_emissive_energy_factor",
						&optix_state.params.lamp_secondary_emissive_energy_factor,
						0.0f,
						5.0f
					);

					optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
				}
			}

			ImGui::Dummy(ImVec2(0, medium_space));
			if (!ignore_buildings && procedural_buildings)
			{
				{
					ImGui::TextColored(text_color2, "%s", "Draw building AABBs");
					bool changed = ImGui::Checkbox(
						"##draw_building_aabbs_checkbox",
						&optix_state.current_procedural_geometry_generation_parameters.render_building_aabbs
					);

					optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
				}

				if (!optix_state.current_procedural_geometry_generation_parameters.render_building_aabbs)
				{
					ImGui::TextColored(text_color2, "%s", "Generate building");
					ImGui::TextColored(text_color2, "%s", "geometry");
					bool changed = ImGui::Checkbox(
						"##draw_building_geometry_checkbox",
						&optix_state.current_procedural_geometry_generation_parameters.generate_building_geometry
					);

					optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
				}
			}

			if ((!ignore_edge_roads && procedural_edge_roads) || (!ignore_joint_roads && procedural_edge_roads))
			{
				ImGui::TextColored(text_color2, "%s", "Render road and");
				ImGui::TextColored(text_color2, "%s", "sidewalk AABBs");
				bool changed = ImGui::Checkbox(
					"##draw_road_aabbs_checkbox",
					&optix_state.current_procedural_geometry_generation_parameters.render_road_and_sidewalk_aabbs
				);

				optix_state.parameter_changed = max(changed, optix_state.parameter_changed);
			}			

			ImGui::Separator();

			ImGui::Dummy(ImVec2(0, major_space));

			ImGui::Separator();

			ImGui::TextColored(text_color2, "%s", "Image rendering");
			{
				int value = image_rendering_handler.get_total_number_of_frames();
				ImGui::TextColored(text_color2, "%s", "Number of frames");
				ImGui::SliderInt(
					"##slider_number_of_frames_for_image_rendering",
					&value,
					1,
					200
				);
				image_rendering_handler.set_total_number_of_frames(value);
			}

			{
				bool render_image_clicked = ImGui::Button("Render image");

				if (render_image_clicked)
					start_image_rendering();
			}

			ImGui::Separator();

			{
				bool save_current_image_clicked = ImGui::Button("Save current image");

				if (save_current_image_clicked)
					image_rendering_handler.save_current_image();
			}

			ImGui_EndFrame_and_Render();
		}

		optix_state.skydome_parameters.texture_number = clamp(
			static_cast<unsigned int>(optix_state.skydome_parameters.texture_number),
			static_cast<unsigned int>(0),
			static_cast<unsigned int>(scene.skydome_textures.size() - 1)
		);
	}

	void Program::start_image_rendering()
	{
		image_rendering_handler.start_image_rendering();

		optix_state.current_ray_parameters_backup = optix_state.current_ray_parameters;
		optix_state.current_procedural_geometry_generation_parameters_backup = optix_state.current_procedural_geometry_generation_parameters;

		optix_state.current_ray_parameters = optix_state.image_rendering_ray_parameters;
		optix_state.current_procedural_geometry_generation_parameters = optix_state.image_rendering_scene_parameters;

		optix_state.current_procedural_geometry_generation_parameters.render_building_aabbs = false;
		optix_state.current_procedural_geometry_generation_parameters.render_road_and_sidewalk_aabbs = false;
		optix_state.current_procedural_geometry_generation_parameters.generate_building_geometry = true;

		optix_state.parameter_changed = true;
	}

	void Program::stop_image_rendering()
	{
		optix_state.current_ray_parameters = optix_state.current_ray_parameters_backup;
		optix_state.current_procedural_geometry_generation_parameters = optix_state.current_procedural_geometry_generation_parameters_backup;

		optix_state.parameter_changed = true;
	}

	void Program::window_size_callback(GLFWwindow* window, int32_t res_x, int32_t res_y)
	{
		Program* program = Program::get_instance();

		if (program->window_state.minimized)
		{
			return;
		}

		program->scene.camera_changed = true;
		program->window_state.window_resized = true;
		program->window_state.width = res_x;
		program->window_state.height = res_y;
	}

	void Program::window_iconify_callback(GLFWwindow* window, int32_t iconified)
	{
		Program* program = Program::get_instance();

		program->window_state.minimized = iconified > 0;
	}

	void Program::mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
	{
		Program* program = Program::get_instance();

		if (program->window_state.mouse_hovering_gui)
		{
			return;
		}

		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);

		if (action == GLFW_PRESS)
		{
			program->user_input.mouse_button = button;
			program->user_input.trackball.startTracking(static_cast<int>(xpos), static_cast<int>(ypos));
		}
		else
		{
			program->user_input.mouse_button = -1;
		}
	}

	void Program::cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
	{
		Program* program = Program::get_instance();

		if (program->user_input.mouse_button == GLFW_MOUSE_BUTTON_LEFT)
		{
			program->user_input.trackball.setViewMode(sutil::Trackball::LookAtFixed);
			program->user_input.trackball.updateTracking(static_cast<int>(xpos), static_cast<int>(ypos), program->window_state.width, program->window_state.height);
			program->scene.camera_changed = true;
		}
		else if (program->user_input.mouse_button == GLFW_MOUSE_BUTTON_RIGHT)
		{
			program->user_input.trackball.setViewMode(sutil::Trackball::EyeFixed);
			program->user_input.trackball.updateTracking(static_cast<int>(xpos), static_cast<int>(ypos), program->window_state.width, program->window_state.height);
			program->scene.camera_changed = true;
		}
	}

	void Program::key_callback(GLFWwindow* window, int32_t key, int32_t scancode, int32_t action, int32_t mods)
	{
		if (action == GLFW_PRESS)
		{
			if (key == GLFW_KEY_Q || key == GLFW_KEY_ESCAPE)
			{
				glfwSetWindowShouldClose(window, true);
			}
			else if (key == GLFW_KEY_C)
			{
				Program* program = Program::get_instance();
				std::cout
					<< "Camera. Position: ["
					<< program->scene.camera.eye().x << ", "
					<< program->scene.camera.eye().y << ", "
					<< program->scene.camera.eye().z << "] "
					<< "Camera. Target: ["
					<< program->scene.camera.lookat().x << ", "
					<< program->scene.camera.lookat().y << ", "
					<< program->scene.camera.lookat().z << "] "
					<< "\n";
			}
		}
	}

	void Program::scroll_callback(GLFWwindow* window, double xscroll, double yscroll)
	{
		Program* program = Program::get_instance();

		if (program->window_state.mouse_hovering_gui)
		{
			return;
		}

		if (program->user_input.trackball.wheelEvent((int)yscroll))
		{
			program->scene.camera_changed = true;
		}
	}	

	void Program::run()
	{
		SceneMemoryMeasurer scene_memory_measurer(InformationUnit::BYTE);

		GraphicsCardMemoryMeasurer graphics_card_memory_measurer(InformationUnit::MEGABYTE);
		graphics_card_memory_measurer.start();

		// Initialization.
		create_scene();
		create_context();
		build_traditional_GAS(scene_memory_measurer);
		build_procedural_GAS(scene_memory_measurer);
		build_IAS();
		create_module();
		create_program_groups();
		create_pipeline();
		create_SBT(scene_memory_measurer);
		initialize_launch_parameters();

		if (drpm::print_scene_geometry_info)
			scene_memory_measurer.print();

		// The main loop.
		{
			GLFWwindow* window = sutil::initUI("Direct rendering of procedural models", optix_state.params.width, optix_state.params.height);
			glfwSetMouseButtonCallback(window, mouse_button_callback);
			glfwSetCursorPosCallback(window, cursor_position_callback);
			glfwSetWindowSizeCallback(window, window_size_callback);
			glfwSetWindowIconifyCallback(window, window_iconify_callback);
			glfwSetKeyCallback(window, key_callback);
			glfwSetScrollCallback(window, scroll_callback);

			{
				graphics_card_memory_measurer.end();
				if (drpm::print_memory_info)
					graphics_card_memory_measurer.print();

				sutil::CUDAOutputBuffer<uchar4> output_buffer(
					drpm::cuda_output_buffer_type,
					window_state.width,
					window_state.height
				);

				output_buffer.setStream(optix_state.stream);
				sutil::GLDisplay gl_display;

				std::chrono::duration<double> state_update_time(0.0);
				std::chrono::duration<double> rendering_time_for_gui(0.0);
				std::chrono::duration<double> rendering_time_to_count_average(0.0);
				std::chrono::duration<double> render_time_for_each_frame(0.0);
				std::chrono::duration<double> display_time(0.0);
				std::chrono::duration<double> render_sum_time(0.0);

				initialize_gui(window);

				image_rendering_handler.initialize(&output_buffer);

				do
				{
					image_rendering_handler.update();					

					auto t0 = std::chrono::steady_clock::now();
					glfwPollEvents();
					update_state(output_buffer);
					auto t1 = std::chrono::steady_clock::now();
					state_update_time += t1 - t0;
					t0 = t1;

					// Launch
					launch_subframe(output_buffer);
					t1 = std::chrono::steady_clock::now();
					rendering_time_for_gui += t1 - t0;
					if (optix_state.params.subframe_index == 0)
						rendering_time_to_count_average = std::chrono::milliseconds::zero();
					rendering_time_to_count_average += t1 - t0;
					render_time_for_each_frame = t1 - t0;
					t0 = t1;					

					// Display
					display_subframe(output_buffer, gl_display, window);
					t1 = std::chrono::steady_clock::now();
					display_time += t1 - t0;

					if (image_rendering_handler.is_rendering())
					{
						std::cout << "Rendering time: " << render_time_for_each_frame.count() << "\n";
						image_rendering_handler.print();
						if (image_rendering_handler.number_of_iterations_to_end() == 0)
						{
							auto avg_time = rendering_time_to_count_average / static_cast<double>(optix_state.params.subframe_index + 1);
							std::cout << "Average rendering time: " << avg_time.count() << "\n";
						}
					}
					else
					{
						sutil::displayStats(state_update_time, rendering_time_for_gui, display_time);
						display_gui();
					}

					if (image_rendering_handler.finished())
					{
						stop_image_rendering();
					}

					glfwSwapBuffers(window);

					++optix_state.params.subframe_index;
				} while (!glfwWindowShouldClose(window));
				CUDA_SYNC_CHECK();
			}

			sutil::cleanupUI(window);
		}

		// The main loop broke, clean up everything.
		cleanup();
	}
}
