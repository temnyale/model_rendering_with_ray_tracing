﻿/**
 * Auxiliary functions.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <iostream>
#include <vector>

namespace drpm
{
	namespace utils
	{
		template<typename Base, typename T>
		inline bool instanceof(const T* ptr)
		{
			return dynamic_cast<const Base*>(ptr) != nullptr;
		}

		std::vector<std::string> split_string(std::string string, char symbol);
	}
}
