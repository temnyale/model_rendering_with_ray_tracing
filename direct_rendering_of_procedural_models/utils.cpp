/**
 * Auxiliary functions.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#include "utils.h"
#include <string>
#include <iostream>
#include <sstream>
#include <cstdarg>

namespace drpm
{
	namespace utils
	{
		std::vector<std::string> split_string(std::string string, char symbol)
		{
			std::stringstream stringstream(string);
			std::string segment;
			std::vector<std::string> splitted;

			while (std::getline(stringstream, segment, symbol))
			{
				splitted.push_back(segment);
			}

			return splitted;
		}		
	}
}
