/**
 * File which contains the paths to files.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#include "path_settings.h"

namespace drpm
{
	std::string path_to_city = "Berlin_1";
	std::string path_to_heightmap = "";
}