/**
 * Shader wit the raygen program, the closesthit program etc..
 * Some parts are taken from the Nvidia sample projects.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#include <optix.h>
#include <optix_device.h>
#include <cuda/helpers.h>
#include <math_constants.h>

#include "optix_helpers.cuh"
#include "cudaNoise.cuh"
#include "procedural_geometry.cuh"
#include "road_texturing.cuh"

#include "rendering.h"
#include "random.h"
#include "vector3.h"
#include "math_utils.h"

namespace drpm
{
	extern "C" __constant__ Parameters OPTIX_PARAMETERS_VAR;

	/// <summary>
	/// Data which a ray holds.
	/// </summary>
	struct RadiancePRD
	{
		// Parameters passed from the raygen program.
		int depth;
		int recursion_depth;
		unsigned int seed;

		// Parameters returned to the raygen program.
		float3 emissive_color;
		float3 diffuse_color;
		float3 origin;
		float3 direction;
		bool done;
		bool miss;
		MaterialType previous_object_material;
		SceneObjectType previous_object_type;
		SceneObjectPart previous_object_part;
		bool ignore_light_source_colliders;
		float tmax;
	};	

	// From an Nvidia sample project.
	static __forceinline__ __device__ RadiancePRD* get_PRD()
	{
		const unsigned int u0 = optixGetPayload_0();
		const unsigned int u1 = optixGetPayload_1();
		return reinterpret_cast<RadiancePRD*>(unpack_pointer(u0, u1));
	}

	static __forceinline__ __device__ void trace_radiance_ray(RadiancePRD* prd, float3 ray_origin, float3 ray_direction, float tmin = 0.01f, float tmax = 1e16f)
	{
		unsigned int u0, u1;
		pack_pointer(prd, u0, u1);
		optixTrace(
			OPTIX_PARAMETERS_VAR.procedural_geometry_traversable,
			ray_origin,
			ray_direction,
			tmin,
			tmax,
			0.0f,                // rayTime
			OptixVisibilityMask(1),
			OPTIX_RAY_FLAG_NONE,
			RAY_TYPE_RADIANCE,        // SBT offset
			RAY_TYPE_COUNT,           // SBT stride
			RAY_TYPE_RADIANCE,        // missSBTIndex
			u0, u1
		);

		//RadiancePRD prd1 = *prd;
		//RadiancePRD prd2 = *prd;

		//unsigned int u0, u1;

		//pack_pointer(&prd1, u0, u1);
		//optixTrace(
		//	OPTIX_PARAMETERS_VAR.procedural_geometry_traversable,
		//	ray_origin,
		//	ray_direction,
		//	tmin,
		//	tmax,
		//	0.0f,                // rayTime
		//	OptixVisibilityMask(1),
		//	OPTIX_RAY_FLAG_NONE,
		//	0,        // SBT offset
		//	RAY_TYPE_COUNT,           // SBT stride
		//	RAY_TYPE_RADIANCE,        // missSBTIndex
		//	u0, u1
		//);

		//pack_pointer(&prd2, u0, u1);
		//optixTrace(
		//	OPTIX_PARAMETERS_VAR.normal_geometry_traversable,
		//	ray_origin,
		//	ray_direction,
		//	tmin,
		//	tmax,
		//	0.0f,                // rayTime
		//	OptixVisibilityMask(1),
		//	OPTIX_RAY_FLAG_NONE,
		//	1,        // SBT offset
		//	RAY_TYPE_COUNT,           // SBT stride
		//	RAY_TYPE_RADIANCE,        // missSBTIndex
		//	u0, u1
		//);

		//if (prd1.miss && prd2.miss)
		//{
		//	*prd = prd1;
		//}
		//else if (!prd1.miss && prd2.miss)
		//{
		//	*prd = prd1;
		//}
		//else if (prd1.miss && !prd2.miss)
		//{
		//	*prd = prd2;
		//}
		//else // if (!prd1.miss && !prd2.miss)
		//{
		//	if (prd1.tmax < prd2.tmax)
		//	{
		//		//printf("%f\n", -length(prd->origin - prd1.origin) + length(prd->origin - prd2.origin));
		//		*prd = prd1;
		//	}
		//	else
		//	{
		//		*prd = prd2;
		//	}
		//}
	}

	__forceinline__ __device__ float3 path_tracing_loop(
		unsigned int& seed,
		const int initial_depth,
		const int initial_recursion_depth,
		const float3& initial_ray_origin,
		const float3& initial_ray_dir,
		const SceneObjectType first_object_type,
		const SceneObjectPart first_object_part,
		const MaterialType first_material_type
	)
	{
		float3 result = vector3::zero();

		float3 ray_direction;
		float3 ray_origin;
		RadiancePRD prd;

		{
			ray_direction = initial_ray_dir;
			ray_origin = initial_ray_origin;
		}

		{
			prd.emissive_color = make_float3(0.f);
			prd.diffuse_color = make_float3(0.f);
			prd.done = false;
			prd.seed = seed;
			prd.depth = initial_depth;
			prd.recursion_depth = initial_recursion_depth;
			prd.previous_object_type = first_object_type;
			prd.previous_object_part = first_object_part;
			prd.previous_object_material = first_material_type;
		}

		{
			// EVERYTHING IS EXPLAINED IN THE REPORT.
			// The final color is calculated as
			// emissive0 + diffuse0 * (emissive1 + diffuse1 * (emissive2 + diffuse2 * (......))) 
			// emissive0 + emissive1 * diffuse0 + emissive2 * diffuse0 * diffuse1 + diffuse0 * diffuse1 * diffuse2 ......
			// The numbers at the ends means the depth of that result.
			// So we need to accumulate values.
			float3 accumulator = make_float3(1.0f);
			do
			{				
				prd.ignore_light_source_colliders = false;

				switch (prd.previous_object_type)
				{
					case SceneObjectType::CAMERA:
					{
						prd.ignore_light_source_colliders = true;
					} break;
				}

				switch (prd.previous_object_part)
				{
					case SceneObjectPart::STREET_LAMP_PILLAR:
					case SceneObjectPart::STREET_LAMP_GLASS_SPHERE:
					{
						prd.ignore_light_source_colliders = true;
					} break;
				}

				switch (prd.previous_object_material)
				{					
					case MaterialType::GLASS:
					case MaterialType::MIRROR:
					case MaterialType::NON_TRANSPARENT_GLASS:
					{
						prd.ignore_light_source_colliders = true;
					} break;
				}

				trace_radiance_ray(&prd, ray_origin, ray_direction);

				result += prd.emissive_color * accumulator;
				accumulator *= prd.diffuse_color;

				ray_origin = prd.origin;
				ray_direction = prd.direction;
				
				prd.depth = prd.depth + 1;
			} while (!prd.done);

			result += accumulator;
		}

		return result;
	}	

	static __forceinline__ __device__ void set_material_parameters(HitGroupData* hit_data)
	{
		OptixAabb aabb = hit_data->aabb;

		unsigned int seed = hit_data->seed;
		
		switch (hit_data->scene_object_type)
		{
			case SceneObjectType::BUILDING:
			{
				float height = aabb.maxY - aabb.minY;
				hit_data->material = OPTIX_PARAMETERS_VAR.regular_bulding_materials[lcg(seed) % OPTIX_PARAMETERS_VAR.number_of_regular_building_materials];
			} break;

			case SceneObjectType::STREET_LAMP:
			{
				hit_data->material = OPTIX_PARAMETERS_VAR.lamp_material;
			} break;

			case SceneObjectType::JOINT_ROAD:
			case SceneObjectType::EDGE_ROAD:
			{
				hit_data->material = OPTIX_PARAMETERS_VAR.road_asphalt_material;
			} break;

			case SceneObjectType::SIDEWALK:
			{
				hit_data->material = OPTIX_PARAMETERS_VAR.sidewalk_asphalt_material;
			} break;

			case SceneObjectType::TERRAIN:
			{
				hit_data->material = OPTIX_PARAMETERS_VAR.grass_material;
			} break;

			default:
			{
				hit_data->material.has_texture = false;
				hit_data->material.diffuse_color = make_float3(0);
				hit_data->material.emissive_color = make_float3(1);
				hit_data->material.diffuse_reflection_parameters.n_s = 1000.0f;
				hit_data->material.diffuse_reflection_parameters.k_d = 0.5f;
				hit_data->material.diffuse_reflection_parameters.k_s = 1.0f - hit_data->material.diffuse_reflection_parameters.k_d;
			} break;
		}
	}			

	static __forceinline__ __device__ void intersection_extruded_polygon(
		const float3& ray_origin, const float3& ray_dir, HitGroupData* hit_data, RadiancePRD* prd
	)
	{
		float3 aabb_min, aabb_max;
		float3 a, b, c, d, e, f, g, h;
		float3 efgh_n;
		OptixAabb& aabb = hit_data->aabb;

		{
			// Determine the face of the intersection, the UV coordinates and the normal vector.
			aabb_to_max_min_aabbs(aabb, aabb_min, aabb_max);
			aabb_to_points(aabb, a, b, c, d, e, f, g, h);

			efgh_n = normalize(cross(h - e, f - e));
		}

		{
			float t;

			// efgh_n is the normal vector of the horizontal polygon.
			if (dot(ray_dir, efgh_n) <= 0.0f && does_ray_intersect_plane(efgh_n, e, ray_origin, ray_dir, t))
			{
				float3 point_of_intersection = ray_origin + ray_dir * t;
				float2 point_of_intersection2d = make_float2(point_of_intersection.x, point_of_intersection.z);

				if (cn_PnPoly(
					point_of_intersection2d, 
					hit_data->procedural_geometry_hit_group_data.geometry.polygon.contour_nodes,
					hit_data->procedural_geometry_hit_group_data.geometry.polygon.number_of_nodes - 1) != 0)
				{
					float u = 0.0f;
					float v = 0.0f;
					SceneObjectPart object_part = SceneObjectPart::SCENE_OBJECT_PART_COUNT;

					if (hit_data->scene_object_type == SceneObjectType::BUILDING)
						object_part = SceneObjectPart::BUILDING_ROOF;

					report_intersection(t, efgh_n, u, v, object_part);
				}
			}

			for (int i = 0; i < hit_data->procedural_geometry_hit_group_data.geometry.polygon.number_of_nodes - 1; i++)
			{
				float2 left_node2d = hit_data->procedural_geometry_hit_group_data.geometry.polygon.contour_nodes[i];
				float2 right_node2d = hit_data->procedural_geometry_hit_group_data.geometry.polygon.contour_nodes[i + 1];

				float3 left_node3d = make_float3(left_node2d.x, aabb.minY, left_node2d.y);
				float3 right_node3d = make_float3(right_node2d.x, aabb.minY, right_node2d.y);

				float3 point_of_intersection = ray_origin + ray_dir * t;
				float2 point_of_intersection2d = make_float2(point_of_intersection.x, point_of_intersection.z);

				float3 wall_a3d = left_node3d;
				float3 wall_b3d = right_node3d;

				float wall_height = aabb_max.y - aabb_min.y;

				float3 wall_c3d = wall_b3d + vector3::up() * wall_height;
				float3 wall_d3d = wall_a3d + vector3::up() * wall_height;

				Quadrangle wall;
				wall.a = wall_a3d;
				wall.b = wall_b3d;
				wall.c = wall_c3d;
				wall.d = wall_d3d;

				if (hit_data->scene_object_type == SceneObjectType::BUILDING)
				{
					RegularBuildingFacadeParameters& facade_parameters = hit_data->procedural_geometry_hit_group_data.miscellaneous.building_properties.facade_params[i];

					/*if (!facade_parameters.calculated)
					{
						unsigned int seed = hit_data->seed;
						float3 horizontal_dir = wall.get_horizontal_direction();
						float wall_width = wall.get_width();
						float wall_height = wall.get_height();

						facade_parameters.calculate_window_parameters(wall_width, wall_height, seed);
					}*/

					if (OPTIX_PARAMETERS_VAR.procedural_geometry_generation_parameters.generate_building_geometry)
					{
						BoundingQuadrangles bounding_quads;
						bounding_quads.initialize_bounding_quadrangles(
							wall,
							facade_parameters.max_extrusion,
							facade_parameters.max_intrusion
						);

						if (bounding_quads.does_ray_intersect_bounding_quadrangles(ray_origin, ray_dir))
							sample_facade(ray_origin, ray_dir, hit_data, wall, facade_parameters);
					}
					else
					{
						if (does_ray_intersect_wall(wall, ray_origin, ray_dir, t))
						{
							float3 intersection_point;
							float3 wall_n;
							float u, v;
							prepare_data_for_intersection_report(ray_origin, ray_dir, wall, t, &hit_data->material, intersection_point, u, v, wall_n);
							report_intersection(t, wall_n, u, v, SceneObjectPart::BUILDING_WALL);
						}
					}
				}
				else
				{
					if (does_ray_intersect_wall(wall, ray_origin, ray_dir, t))
					{
						float3 point_of_intersection;

						float u = 0.0f;
						float v = 0.0f;

						float3 wall_n;

						prepare_data_for_intersection_report(ray_origin, ray_dir, wall, t, &hit_data->material, point_of_intersection, u, v, wall_n);
						report_intersection(t, wall_n, u, v, SceneObjectPart::SCENE_OBJECT_PART_COUNT);
					}
				}
			}

		}
	}	

	/// <summary>
	/// Checks if there is an intersection.
	/// </summary>
	static __forceinline__ __device__ void intersection_aabb(
		const float3& ray_origin, const float3& ray_dir, HitGroupData* hit_data, RadiancePRD* prd
	)
	{
		OptixAabb& aabb = hit_data->aabb;

		float3 a, b, c, d, e, f, g, h;		
		Quadrangle walls[6];

		{		
			// Determine the face of the intersection, the UV coordinates and the normal vector.
			aabb_to_points(aabb, a, b, c, d, e, f, g, h);		

			{
				Quadrangle vertical_wall;
				vertical_wall.a = a;
				vertical_wall.b = b;
				vertical_wall.c = f;
				vertical_wall.d = e;

				walls[0] = vertical_wall;
			}

			{
				Quadrangle vertical_wall;
				vertical_wall.a = b;
				vertical_wall.b = c;
				vertical_wall.c = g;
				vertical_wall.d = f;

				walls[1] = vertical_wall;
			}

			{
				Quadrangle vertical_wall;
				vertical_wall.a = c;
				vertical_wall.b = d;
				vertical_wall.c = h;
				vertical_wall.d = g;

				walls[2] = vertical_wall;
			}

			{
				Quadrangle vertical_wall;
				vertical_wall.a = d;
				vertical_wall.b = a;
				vertical_wall.c = e;
				vertical_wall.d = h;

				walls[3] = vertical_wall;
			}			

			{
				Quadrangle horizontal_wall;
				horizontal_wall.a = e;
				horizontal_wall.b = f;
				horizontal_wall.c = g;
				horizontal_wall.d = h;

				walls[4] = horizontal_wall;
			}

			{
				Quadrangle horizontal_wall;
				horizontal_wall.a = d;
				horizontal_wall.b = c;
				horizontal_wall.c = b;
				horizontal_wall.d = a;

				walls[5] = horizontal_wall;
			}
		}

		for (auto& wall : walls)
		{
			float t;
			if (does_ray_intersect_wall(wall, ray_origin, ray_dir, t))
			{
				float3 point_of_intersection;
				float3 wall_n = wall.get_normal();
				float u;
				float v;
				prepare_data_for_intersection_report(ray_origin, ray_dir, wall, t, &hit_data->material, point_of_intersection, u, v, wall_n);
				report_intersection(t, wall_n, u, v, SceneObjectPart::SCENE_OBJECT_PART_COUNT);

				break;
			}
		}
	}	

	static __forceinline__ __device__ void intersection_street_lamp(
		const float3& ray_origin, const float3& ray_dir, HitGroupData* hit_data, RadiancePRD* prd
	)
	{		
		const float ray_tmin = optixGetRayTmin(), ray_tmax = optixGetRayTmax();
		float3 bottom_disc_center = get_lamp_pillar_bottom_disc_center(hit_data->aabb);

		float max_y = hit_data->aabb.maxY;
		float min_y = hit_data->aabb.minY;

		float3 top_disc_center = bottom_disc_center + vector3::up() * lamp_height;

		float pillar_radius = lamp_pillar_diameter / 2.0f;
		float glass_sphere_radius = lamp_glass_sphere_diameter / 2.0f;
		float emissive_sphere_radius = lamp_bulb_diameter / 2.0f;
		float glass_sphere_h = calculate_lamp_glass_sphere_h(glass_sphere_radius, pillar_radius);
		float pillar_max_y = calculate_lamp_pillar_max_y(max_y, glass_sphere_radius, glass_sphere_h);
		float3 glass_sphere_center = calculate_lamp_glass_sphere_center(max_y, bottom_disc_center, glass_sphere_radius);
		float3 emissive_sphere_center = calculate_lamp_bulb_center(pillar_max_y, bottom_disc_center, emissive_sphere_radius);

		// Spheres.
		{
			bool glass_sphere_t1_suitable, glass_sphere_t2_suitable;
			float glass_sphere_t1, glass_sphere_t2;
			float3 glass_sphere_intersection1, glass_sphere_intersection2;
			float3 glass_sphere_normal1, glass_sphere_normal2;

			if (does_ray_intersect_sphere(
				ray_origin, ray_dir, glass_sphere_center, glass_sphere_radius,
				glass_sphere_t1_suitable, glass_sphere_t1, glass_sphere_intersection1, glass_sphere_normal1,
				glass_sphere_t2_suitable, glass_sphere_t2, glass_sphere_intersection2, glass_sphere_normal2
			))
			{
				float u, v;

				if (glass_sphere_t1_suitable && glass_sphere_intersection1.y >= pillar_max_y)
					report_intersection(
						glass_sphere_t1,
						glass_sphere_normal1,
						u,
						v,
						SceneObjectPart::STREET_LAMP_GLASS_SPHERE
					);

				if (glass_sphere_t2_suitable && glass_sphere_intersection2.y >= pillar_max_y)
					report_intersection(
						glass_sphere_t2,
						glass_sphere_normal2,
						u,
						v,
						SceneObjectPart::STREET_LAMP_GLASS_SPHERE
					);
			}

			bool emissive_sphere_t1_suitable, emissive_sphere_t2_suitable;
			float emissive_sphere_t1, emissive_sphere_t2;
			float3 emissive_sphere_intersection1, emissive_sphere_intersection2;
			float3 emissive_sphere_normal1, emissive_sphere_normal2;

			if (does_ray_intersect_sphere(
				ray_origin, ray_dir, emissive_sphere_center, emissive_sphere_radius,
				emissive_sphere_t1_suitable, emissive_sphere_t1, emissive_sphere_intersection1, emissive_sphere_normal1,
				emissive_sphere_t2_suitable, emissive_sphere_t2, emissive_sphere_intersection2, emissive_sphere_normal2
			))
			{
				float u, v;

				if (emissive_sphere_t1_suitable && emissive_sphere_intersection1.y >= pillar_max_y)
					report_intersection(
						emissive_sphere_t1,
						emissive_sphere_normal1,
						u,
						v,
						SceneObjectPart::STREET_LAMP_BULB
					);

				if (emissive_sphere_t2_suitable && emissive_sphere_intersection2.y >= pillar_max_y)
					report_intersection(
						emissive_sphere_t2,
						emissive_sphere_normal2,
						u,
						v,
						SceneObjectPart::STREET_LAMP_BULB
					);
			}
		}

		// Pillar.
		{
			bool t1_suitable, t2_suitable, t3_suitable, t4_suitable;
			float t1, t2, t3, t4;
			float3 intersection1, intersection2, intersection3, intersection4;
			float3 normal1, normal2, normal3, normal4;
			if (does_ray_intersect_axis_aligned_cylinder(
				ray_origin, ray_dir, ray_tmin, ray_tmax,
				bottom_disc_center, pillar_radius, pillar_max_y - min_y,
				t1_suitable, t1, intersection1, normal1,
				t2_suitable, t2, intersection2, normal2,
				t3_suitable, t3, intersection3, normal3,
				t4_suitable, t4, intersection4, normal4
			))
			{
				float u, v;
				if (t1_suitable)
					report_intersection(
						t1,
						normal1,
						u,
						v,
						SceneObjectPart::STREET_LAMP_PILLAR
					);
				if (t2_suitable)
					report_intersection(
						t2,
						normal2,
						u,
						v,
						SceneObjectPart::STREET_LAMP_PILLAR
					);
				if (t3_suitable)
					report_intersection(
						t3,
						normal3,
						u,
						v,
						SceneObjectPart::STREET_LAMP_PILLAR
					);
				if (t4_suitable)
					report_intersection(
						t4,
						normal4,
						u,
						v,
						SceneObjectPart::STREET_LAMP_PILLAR
					);
			}
		}
	}

	static __forceinline__ __device__ void get_parameters(
		HitGroupData* hit_data, const float3& ray_dir, const float3& intersection_point, const int prim_idx,
		float3& N, float2& UV, SceneObjectPart& scene_object_part, int& scene_object_part_id
	)
	{
		scene_object_part = SceneObjectPart::SCENE_OBJECT_PART_COUNT;

		switch (hit_data->geometry_type)
		{
			case GeometryType::gt_PROCEDURAL:
			{
				N = make_float3(
					int_as_float(optixGetAttribute_0()),
					int_as_float(optixGetAttribute_1()),
					int_as_float(optixGetAttribute_2())
				);
				UV = make_float2(
					int_as_float(optixGetAttribute_3()),
					int_as_float(optixGetAttribute_4())
				);
				scene_object_part = static_cast<SceneObjectPart>(optixGetAttribute_5());
				scene_object_part_id = optixGetAttribute_6();
			} break;

			case GeometryType::gt_TRADITIONAL:
			{
				Quadrangle quadrangle = get_quadrangle_from_vertex_array(hit_data, prim_idx);

				float3 vertices[3];
				get_vertices(hit_data, prim_idx, vertices);

				const float3 v0 = vertices[0];
				const float3 v1 = vertices[1];
				const float3 v2 = vertices[2];

				N = normalize(cross(v0 - v1, v2 - v1));
				N = faceforward(N, -ray_dir, N);

				if (hit_data->traditional_geometry_hit_group_data.object_parts != nullptr)
					scene_object_part = hit_data->traditional_geometry_hit_group_data.object_parts[prim_idx];

				if (hit_data->traditional_geometry_hit_group_data.object_part_ids != nullptr)
					scene_object_part_id = hit_data->traditional_geometry_hit_group_data.object_part_ids[prim_idx];

				if (hit_data->material.has_texture)
				{
					float2 prim_a_UVs;
					float2 prim_b_UVs;
					float2 prim_c_UVs;
					float bx;
					float by;
					if (hit_data->traditional_geometry_hit_group_data.UVs != nullptr)
					{
						prim_a_UVs = hit_data->traditional_geometry_hit_group_data.UVs[prim_idx * 3 + 0];
						prim_b_UVs = hit_data->traditional_geometry_hit_group_data.UVs[prim_idx * 3 + 1];
						prim_c_UVs = hit_data->traditional_geometry_hit_group_data.UVs[prim_idx * 3 + 2];

						bx = optixGetTriangleBarycentrics().x;
						by = optixGetTriangleBarycentrics().y;
					}

					switch (scene_object_part)
					{
						case SceneObjectPart::ROOM_WALL:
							UV = prim_a_UVs + (prim_b_UVs - prim_a_UVs) * bx + (prim_c_UVs - prim_a_UVs) * by;
							break;

						case SceneObjectPart::BUILDING_ROOF:
							UV = make_float2(0.f);
							break;

						case SceneObjectPart::BUILDING_WALL:
							UV = prim_a_UVs + (prim_b_UVs - prim_a_UVs) * bx + (prim_c_UVs - prim_a_UVs) * by;
							float2 _UV = UV;
							UV.x = fmodf(UV.x, hit_data->material.texture.t_h) / hit_data->material.texture.t_h;
							UV.y = fmodf(UV.y, hit_data->material.texture.t_v) / hit_data->material.texture.t_v;
							//printf("([%f %f] [%f %f] [%f %f]), _UV=[%f %f], UV=[%f %f]\n", prim_a_UVs.x, prim_a_UVs.y, prim_b_UVs.x, prim_b_UVs.y, prim_c_UVs.x, prim_c_UVs.y, _UV.x, _UV.y, UV.x, UV.y);
							//UV = make_float2(0.f);
							break;

						default:
							wall_UV_map(UV.x, UV.y, quadrangle, intersection_point, hit_data->material.texture.t_h, hit_data->material.texture.t_v);
							break;
					}
				}
				else
					UV = make_float2(0.f);				
			} break;

			default:
			{
				printf("Incorrect geometry type.\n");
			} break;
		}
	}

	/// <summary>
	/// Hit. Calculate the color and the direction of reflection.
	/// </summary>
	extern "C" __global__ void __closesthit__radiance()
	{		
		HitGroupData* hit_data = reinterpret_cast<HitGroupData*>(optixGetSbtDataPointer());
		RadiancePRD* prd = get_PRD();

		// Determine all the properties once.
		if (hit_data->first_pass)
		{
			set_material_parameters(hit_data);
			hit_data->first_pass = false;
		}

		const int prim_idx = optixGetPrimitiveIndex();
		const float3 ray_origin = optixGetWorldRayOrigin();
		const float3 ray_dir = optixGetWorldRayDirection();
		const float tmax = optixGetRayTmax();
		const float3 P = ray_origin + tmax * ray_dir;

		float3 N;
		float2 UV;
		SceneObjectPart scene_object_part;
		int scene_object_part_id;

		get_parameters(hit_data, ray_dir, P, prim_idx, N, UV, scene_object_part, scene_object_part_id);

		prd->origin = P;

		OptixAabb& aabb = hit_data->aabb;

		unsigned int seed_for_material = hit_data->seed;
		unsigned int seed_for_ray = prd->seed;
		unsigned int seed_for_material_backup = seed_for_material;
		unsigned int seed_for_object_with_id = static_cast<unsigned int>(scene_object_part_id) * seed_for_material_backup;

		// Material properties.
		SceneObjectType object_type = hit_data->scene_object_type;
		MaterialType material_type;
		bool is_emissive;
		float3 emissive_color = vector3::zero();
		float3 diffuse_color = vector3::one();
		float n_s;
		float k_d;
		float k_s;	
		GlassParameters glass_parameters;

		// Set the material's properties.
		{		
			switch (object_type)
			{
				case SceneObjectType::STREET_LAMP:
				{
					switch (scene_object_part)
					{
						case SceneObjectPart::STREET_LAMP_GLASS_SPHERE:
						{
							// If the ray DO have a direct influence on the pixel, add a small amount of emissive energy 
							// to make the glass more bright.
							if (prd->depth + 1 == prd->recursion_depth)
								emissive_color 
								= OPTIX_PARAMETERS_VAR.street_lamp_emissive_color * OPTIX_PARAMETERS_VAR.street_lamps_on * 0.001f;
							// If the ray doesn't have a direct influence on the pixel, add emissive energy to the glass sphere.
							else
								emissive_color 
								= OPTIX_PARAMETERS_VAR.street_lamp_emissive_color * OPTIX_PARAMETERS_VAR.street_lamps_on * OPTIX_PARAMETERS_VAR.lamp_secondary_emissive_energy_factor;

							diffuse_color = OPTIX_PARAMETERS_VAR.street_lamp_glass.diffuse_color;
							glass_parameters = OPTIX_PARAMETERS_VAR.street_lamp_glass.glass_parameters;
							material_type = MaterialType::GLASS;
						} break;

						case SceneObjectPart::STREET_LAMP_BULB:
						{
							material_type = MaterialType::DIFFUSE;

							// It is supposed that it doesn't have a texture.
							diffuse_color = OPTIX_PARAMETERS_VAR.street_lamp_bulb_material.diffuse_color;
							n_s = OPTIX_PARAMETERS_VAR.street_lamp_bulb_material.diffuse_reflection_parameters.n_s;
							k_s = OPTIX_PARAMETERS_VAR.street_lamp_bulb_material.diffuse_reflection_parameters.k_s;
							k_d = OPTIX_PARAMETERS_VAR.street_lamp_bulb_material.diffuse_reflection_parameters.k_d;
							emissive_color
								= OPTIX_PARAMETERS_VAR.street_lamp_emissive_color * OPTIX_PARAMETERS_VAR.street_lamps_on * OPTIX_PARAMETERS_VAR.street_lamp_emissive_energy_factor;
						} break;

						case SceneObjectPart::STREET_LAMP_PILLAR:
						default:
						{
							material_type = MaterialType::DIFFUSE;
							emissive_color = hit_data->material.emissive_color;
							diffuse_color = hit_data->material.diffuse_color;
							n_s = hit_data->material.diffuse_reflection_parameters.n_s;
							k_s = hit_data->material.diffuse_reflection_parameters.k_s;
							k_d = hit_data->material.diffuse_reflection_parameters.k_d;
						} break;
					}
				} break;

				case SceneObjectType::BUILDING:
				{
					if (OPTIX_PARAMETERS_VAR.procedural_geometry_generation_parameters.render_building_aabbs && hit_data->geometry_type != GeometryType::gt_TRADITIONAL)
						scene_object_part = SceneObjectPart::BUILDING_WALL;	

					switch (scene_object_part)
					{		
						case SceneObjectPart::WINDOW_GLASS:
						{
							emissive_color = OPTIX_PARAMETERS_VAR.window_glass.emissive_color;
							diffuse_color = OPTIX_PARAMETERS_VAR.window_glass.diffuse_color;
							glass_parameters = OPTIX_PARAMETERS_VAR.window_glass.glass_parameters;
							material_type = MaterialType::GLASS;
						} break;

						case SceneObjectPart::BALCONY_GLASS:
						{
							emissive_color = OPTIX_PARAMETERS_VAR.balcony_glass.emissive_color;
							diffuse_color = OPTIX_PARAMETERS_VAR.balcony_glass.diffuse_color;
							glass_parameters = OPTIX_PARAMETERS_VAR.balcony_glass.glass_parameters;
							material_type = MaterialType::GLASS;
						} break;

						case SceneObjectPart::ROOM_WALL:
						case SceneObjectPart::BUILDING_WALL:
						case SceneObjectPart::BALCONY_FENCE:
						case SceneObjectPart::BALCONY_WALL:
						default:
						{
							material_type = MaterialType::DIFFUSE;

							Material material_appearance;

							switch (scene_object_part)
							{
								case SceneObjectPart::WINDOW_IMPOST:
								{
									material_appearance = OPTIX_PARAMETERS_VAR.window_impost_material;
									emissive_color = make_float3(0.0f);
								} break;

								case SceneObjectPart::BALCONY_FENCE:
								{
									material_appearance = params.balcony_fence_material;
									emissive_color = make_float3(0.0f);
								} break;

								case SceneObjectPart::BALCONY_WALL:
								{
									material_appearance = params.inside_room_materials[lcg(seed_for_object_with_id) % params.number_of_inside_room_materials];
									emissive_color = make_float3(0.0f);
								} break;

								case SceneObjectPart::ROOM_BACKGROUND_WALL:
								case SceneObjectPart::ROOM_WALL:
								{			
									material_appearance = params.inside_room_materials[lcg(seed_for_object_with_id) % params.number_of_inside_room_materials];

									float probability_for_emissive = rnd(seed_for_object_with_id);
									// The shop windows have id -1.
									is_emissive
										= scene_object_part_id != -1
										? static_cast<bool>(probability_for_emissive <= OPTIX_PARAMETERS_VAR.window_with_lights_on_percentage)
										: static_cast<bool>(probability_for_emissive <= OPTIX_PARAMETERS_VAR.shop_window_with_lights_on_percentage);

									if (is_emissive)
									{
										emissive_color = material_appearance.emissive_color;
										// This function have bugs...
										float __x = UV.x - 0.5f;
										float __y = UV.y - 0.5f;
										float __z = sqrtf(__x * __x + __y * __y) - 0.5f;
										float noise_value = cudaNoise::repeaterPerlin(
											make_float3(__x, __y, __z),
											OPTIX_PARAMETERS_VAR.perlin_noise_scale,
											seed_for_object_with_id,
											OPTIX_PARAMETERS_VAR.perlin_noise_n,
											OPTIX_PARAMETERS_VAR.perlin_noise_lacunarity,
											OPTIX_PARAMETERS_VAR.perlin_noise_decay
										);
										// If the ray has a direct influence on a pixel, the factor is 1.
										// If the ray does NOT have a direct influence on the pixel, use another factor.
										const float factor 
											= prd->depth + 1 == prd->recursion_depth ? 1.f 
											: scene_object_part_id == -1 ? 
											OPTIX_PARAMETERS_VAR.shop_window_emissive_color_factor : OPTIX_PARAMETERS_VAR.window_emissive_color_factor;
										if (isnan(noise_value))
											noise_value = 1.f;
										noise_value = clamp(noise_value, -1.f, 1.f);
										noise_value = map(noise_value, -1.f, 1.f, OPTIX_PARAMETERS_VAR.perlin_noise_min, OPTIX_PARAMETERS_VAR.perlin_noise_max);
										emissive_color *= noise_value * factor;
									}
								} break;

								default:
								{
									material_appearance = hit_data->material;
									emissive_color = make_float3(0.0f);
								} break;
							}

							n_s = hit_data->material.diffuse_reflection_parameters.n_s;
							k_s = hit_data->material.diffuse_reflection_parameters.k_s;
							k_d = hit_data->material.diffuse_reflection_parameters.k_d;

							if (material_appearance.has_texture)
							{
								float4 color_from_texture = tex2D<float4>(material_appearance.texture.texture_object, UV.x, UV.y);
								diffuse_color = make_float3(color_from_texture.x, color_from_texture.y, color_from_texture.z);
							}
							else
								diffuse_color = material_appearance.diffuse_color;
						} break;
					}					
				} break;

				case SceneObjectType::EDGE_ROAD:
				{
					Material material_appearance = hit_data->material;

					scene_object_part = SceneObjectPart::SCENE_OBJECT_PART_COUNT;
					material_type = MaterialType::DIFFUSE;

					n_s = hit_data->material.diffuse_reflection_parameters.n_s;
					k_s = hit_data->material.diffuse_reflection_parameters.k_s;
					k_d = hit_data->material.diffuse_reflection_parameters.k_d;
					emissive_color = make_float3(0.0f);

					diffuse_color = material_appearance.diffuse_color;
					emissive_color = material_appearance.emissive_color;

					if (!OPTIX_PARAMETERS_VAR.procedural_geometry_generation_parameters.render_road_and_sidewalk_aabbs || hit_data->geometry_type == GeometryType::gt_TRADITIONAL)
					{
						if (dot(N, vector3::up()) > .0f)
						{
							sample_road_markings(P, hit_data, prim_idx, diffuse_color);
						}
					}
				} break;

				case SceneObjectType::LIGHT_SOURCE_COLLIDER:
				{
					float3 new_ray_dir;

					{
						float3 bottom_disc_center 
							= get_lamp_pillar_bottom_disc_center(hit_data->procedural_geometry_hit_group_data.miscellaneous.street_lamp_collider_properties.lamp_aabb);


						float max_y = hit_data->aabb.maxY;
						float min_y = hit_data->aabb.minY;

						float pillar_radius = lamp_pillar_diameter / 2.0f;
						float glass_sphere_radius = lamp_glass_sphere_diameter / 2.0f;
						float bulb_radius = lamp_bulb_diameter / 2.0f;
						float glass_sphere_h = calculate_lamp_glass_sphere_h(glass_sphere_radius, pillar_radius);
						float pillar_max_y = calculate_lamp_pillar_max_y(max_y, glass_sphere_radius, glass_sphere_h);
						float3 bulb_center = calculate_lamp_glass_sphere_center(pillar_max_y, bottom_disc_center, bulb_radius);
						float3 glass_sphere_center = calculate_lamp_glass_sphere_center(max_y, bottom_disc_center, glass_sphere_radius);

						float3 new_ray_target = bulb_center - vector3::up() * bulb_radius * 15.f;

						new_ray_dir = normalize(new_ray_target - P);
					}

					RadiancePRD new_prd;
					new_prd.done = false;
					new_prd.depth = -9999;
					new_prd.recursion_depth = prd->recursion_depth + 1;
					new_prd.ignore_light_source_colliders = true;
					new_prd.previous_object_type = SceneObjectType::LIGHT_SOURCE_COLLIDER;
					new_prd.previous_object_part = SceneObjectPart::SCENE_OBJECT_PART_COUNT;
					new_prd.previous_object_material = MaterialType::MATERIAL_COUNT;

					trace_radiance_ray(&new_prd, P, new_ray_dir);

					prd->origin = new_prd.origin;
					prd->direction = new_prd.direction;
					prd->previous_object_type = new_prd.previous_object_type;
					prd->previous_object_part = new_prd.previous_object_part;
					prd->previous_object_material = new_prd.previous_object_material;
					prd->diffuse_color = new_prd.diffuse_color;
					prd->emissive_color = new_prd.emissive_color;
					prd->done = new_prd.done;

					return;
				};

				case SceneObjectType::NORMAL_GEOMETRY_TEST:
				{
					Material material_appearance = hit_data->material;

					Material m;
					m.has_texture = false;
					m.diffuse_reflection_parameters.k_d = 0.8f;
					m.diffuse_reflection_parameters.k_s = 0.1f;
					m.diffuse_reflection_parameters.n_s = 1000.f;
					m.diffuse_color = dot(ray_dir, N) < 0.f ? material_appearance.diffuse_color : vector3::zero();
					material_appearance = m;

					scene_object_part = SceneObjectPart::SCENE_OBJECT_PART_COUNT;
					material_type = MaterialType::DIFFUSE;

					n_s = material_appearance.diffuse_reflection_parameters.n_s;
					k_s = material_appearance.diffuse_reflection_parameters.k_s;
					k_d = material_appearance.diffuse_reflection_parameters.k_d;
					emissive_color = make_float3(0.0f);

					diffuse_color = material_appearance.diffuse_color;					
				} break;

				case SceneObjectType::OBJECT_TYPE_COUNT:
				{
					Material material_appearance;
					material_appearance.has_texture = false;
					material_appearance.diffuse_reflection_parameters.k_d = 0.8f;
					material_appearance.diffuse_reflection_parameters.k_s = 0.1f;
					material_appearance.diffuse_reflection_parameters.n_s = 1000.f;
					material_appearance.diffuse_color = dot(ray_dir, N) < 0.f ? material_appearance.diffuse_color : vector3::zero();

					scene_object_part = SceneObjectPart::SCENE_OBJECT_PART_COUNT;
					material_type = MaterialType::DIFFUSE;

					n_s = material_appearance.diffuse_reflection_parameters.n_s;
					k_s = material_appearance.diffuse_reflection_parameters.k_s;
					k_d = material_appearance.diffuse_reflection_parameters.k_d;
					emissive_color = make_float3(0.0f);

					diffuse_color = material_appearance.diffuse_color;
				} break;

				default:
				{		
					Material material_appearance = hit_data->material;

					scene_object_part = SceneObjectPart::SCENE_OBJECT_PART_COUNT;
					material_type = MaterialType::DIFFUSE;

					n_s = material_appearance.diffuse_reflection_parameters.n_s;
					k_s = material_appearance.diffuse_reflection_parameters.k_s;
					k_d = material_appearance.diffuse_reflection_parameters.k_d;
					emissive_color = make_float3(0.0f);					

					if (material_appearance.has_texture)
					{
						float4 color_from_texture = tex2D<float4>(material_appearance.texture.texture_object, UV.x, UV.y);

						diffuse_color = make_float3(color_from_texture.x, color_from_texture.y, color_from_texture.z);
					}
					else
					{
						diffuse_color = material_appearance.diffuse_color;
						emissive_color = material_appearance.emissive_color;
					}
				}
			}
		}

		{
			// Russian roulette.
			// http://www.pbr-book.org/3ed-2018/Monte_Carlo_Integration/Russian_Roulette_and_Splitting.html
			bool stop_calculation = false;

			{				
				if (prd->depth >= OPTIX_PARAMETERS_VAR.ray_parameters.russian_roulette_depth)
				{
					float c = 0.0f;

					if (rnd(seed_for_ray) <= OPTIX_PARAMETERS_VAR.ray_parameters.ray_termination_probability)
					{
						diffuse_color = make_float3(c);
						prd->done = true;
						stop_calculation = true;
					}
					else
					{
						diffuse_color = diffuse_color - make_float3(OPTIX_PARAMETERS_VAR.ray_parameters.ray_termination_probability * c);
						diffuse_color = clamp(diffuse_color, vector3::zero(), vector3::one());
						diffuse_color /= (1.0f - OPTIX_PARAMETERS_VAR.ray_parameters.ray_termination_probability);
					}
				}
			}

			// Calculate the direction of the reflected ray.
			// https://www.cs.princeton.edu/courses/archive/fall16/cos526/papers/importance.pdf
			{
				if (!stop_calculation)
				{
					switch (material_type)
					{
						case MaterialType::DIFFUSE:
						{
							const float u = rnd(seed_for_ray);
							const float z1 = rnd(seed_for_ray);
							const float z2 = rnd(seed_for_ray);

							float3 reflected_ray_dir;

							if (u < k_d)
							{
								diffuse_sample_hemisphere(z1, z2, reflected_ray_dir);
							}
							else if (u < k_d + k_s)
							{
								specular_sample_hemisphere(z1, z2, n_s, reflected_ray_dir);
							}
							else
							{
								diffuse_color = vector3::zero();
								emissive_color = vector3::zero();

								prd->done = true;
							}

							transform_to_onb(N, reflected_ray_dir);

							prd->direction = reflected_ray_dir;
						} break;

						case MaterialType::MIRROR:
						{
							prd->direction = reflect(ray_dir, N);
						} break;

						case MaterialType::NON_TRANSPARENT_GLASS:
						{
							const float u = rnd(seed_for_ray);
							const float z1 = rnd(seed_for_ray);
							const float z2 = rnd(seed_for_ray);

							float3 reflected_ray_dir;

							if (u < k_d)
							{
								reflected_ray_dir = reflect(ray_dir, N);
							}
							else
							{
								specular_sample_hemisphere(z1, z2, n_s, reflected_ray_dir);

								transform_to_onb(N, reflected_ray_dir);
							}

							prd->direction = reflected_ray_dir;
						} break;

						case MaterialType::GLASS:
						{						
							float3 refraction_dir;
							float3 reflection_dir = reflect(ray_dir, N);

							float reflection = 1.0f;
							bool refracted = false;
							bool into = dot(ray_dir, N) <= 0.f;
							float3 Nl = into ? N : -N;
							float _refraction_index = into ? glass_parameters.refractive_index : 1.f / glass_parameters.refractive_index;
							if ((refracted = refract(refraction_dir, ray_dir, Nl, _refraction_index)))
							{
								float cos_theta = -dot(ray_dir, Nl);
								reflection = fresnel_schlick(
									cos_theta,
									glass_parameters.fresnel_exponent,
									glass_parameters.fresnel_minimum,
									glass_parameters.fresnel_maximum
								);
							}

							if (prd->depth + 1 == prd->recursion_depth && prd->recursion_depth < max_recursion_depth)
							{
								float3 refraction_result = diffuse_color, reflection_result = diffuse_color;

								{
									if (refracted)
									{
										unsigned int seed_for_refraction = seed_for_ray;
										refraction_result 
											= path_tracing_loop(
												seed_for_refraction, 
												prd->depth + 1, 
												prd->recursion_depth + 1, 
												P, 
												refraction_dir, 
												object_type,
												scene_object_part,
												material_type
											);
									}

									refraction_result *= (1.0f - reflection);
								}

								{
									unsigned int seed_for_reflection = seed_for_ray;
									reflection_result 
										= reflection
										* path_tracing_loop(
											seed_for_reflection, 
											prd->depth + 1, 
											prd->recursion_depth + 1, 
											P, 
											reflection_dir, 
											object_type,
											scene_object_part,
											material_type
										);
								}

								diffuse_color = (refraction_result + reflection_result);

								prd->done = true;
							}
							else
							{
								float probability_of_reflection = .25f + .5f * reflection;
								float probability_of_refraction = 1.f - probability_of_reflection;
								float RP = reflection / probability_of_reflection, TP = (1.f - reflection) / (1.f - probability_of_reflection);
								if (rnd(seed_for_ray) < probability_of_refraction && refracted)
								{
									//diffuse_color = diffuse_color * (1.f - reflection);
									diffuse_color = diffuse_color * TP;
									prd->direction = refraction_dir;
								}
								else
								{
									//diffuse_color = diffuse_color * reflection;
									diffuse_color = diffuse_color * RP;
									prd->direction = reflection_dir;
								}
							}



							//diffuse_color = vector3::zero();
							//emissive_color = vector3::zero();
						} break;

						default:
						{
							printf("Incorrect material.\n");
							prd->direction = ray_dir;
						}
					}
				}
			}

			prd->seed = seed_for_ray;
			prd->diffuse_color = diffuse_color;
			prd->emissive_color = emissive_color;
			prd->previous_object_material = material_type;
			prd->previous_object_type = object_type;
			prd->previous_object_part = scene_object_part;
			prd->miss = false;
			prd->tmax = tmax;
		}
	}

	/// <summary>
	/// No hit. Determine the color of the skydome.
	/// </summary>
	extern "C" __global__ void __miss__radiance()
	{	
		MissData* miss_data = reinterpret_cast<MissData*>(optixGetSbtDataPointer());
		RadiancePRD* prd = get_PRD();					
		float4 color_from_texture;

		{			
			const float3 ray_dir = optixGetWorldRayDirection();

			float u;
			float v;

			u = 0.5f + (atan2f(ray_dir.x, ray_dir.z) / (2 * CUDART_PI_F));

			v = ray_dir.y * 0.5f + 0.5f;
			v = clamp(v, 0.0005f, 0.9995f);

			color_from_texture 
				= tex2D<float4>(miss_data->skydome_parameters.texture_object, u, v) 
				* make_float4(miss_data->skydome_parameters.texture_color_factor, 1.0f);
		}

		{
			// EVERYTHING IS EXPLAINED IN THE REPORT.
			// If the depth is 0, the ray didn't hit any object. In this case, no object will be lighted in the scene by this ray.
			// We want to display only the skydome's color.
			// We have to take only the texture's color to do this.
			if (prd->depth + 1 == prd->recursion_depth)
			{
				prd->diffuse_color = make_float3(color_from_texture.x, color_from_texture.y, color_from_texture.z);
				prd->emissive_color = vector3::zero();
			}
			// Otherwise, the ray is not responsible for displaying the skydome's texture. It is responsible for lighting of the scene.
			else
			{
				prd->diffuse_color 
					= make_float3(color_from_texture.x, color_from_texture.y, color_from_texture.z) 
					* miss_data->skydome_parameters.diffuse_color_factor;
				prd->emissive_color = miss_data->skydome_parameters.emissive_color;
			}

			prd->previous_object_type = SceneObjectType::SKYDOME;
			prd->previous_object_part = SceneObjectPart::SCENE_OBJECT_PART_COUNT;
			prd->previous_object_material = MaterialType::MATERIAL_COUNT;
			prd->done = true;
			prd->miss = true;
			prd->tmax = optixGetRayTmax();
		}
	}

	extern "C" __global__ void __intersection__is()
	{
		const float3 ray_origin = optixGetWorldRayOrigin();
		const float3 ray_dir = optixGetWorldRayDirection();
		HitGroupData* hit_data = reinterpret_cast<HitGroupData*>(optixGetSbtDataPointer());
		RadiancePRD* prd = get_PRD();

		// Determine all the properties once.
		if (hit_data->first_pass)
		{
			set_material_parameters(hit_data);
			hit_data->first_pass = false;
		}			

		switch (hit_data->scene_object_type)
		{
			case SceneObjectType::LIGHT_SOURCE_COLLIDER:
			{
				if (prd->depth == 0 || prd->recursion_depth >= 3 || OPTIX_PARAMETERS_VAR.dont_use_street_lamp_colliders || !OPTIX_PARAMETERS_VAR.street_lamps_on)
					prd->ignore_light_source_colliders = true;

				if (!prd->ignore_light_source_colliders)
					intersection_aabb(ray_origin, ray_dir, hit_data, prd);
			} break;

			case SceneObjectType::BUILDING:
			{
				if (OPTIX_PARAMETERS_VAR.procedural_geometry_generation_parameters.render_building_aabbs)
					intersection_aabb(ray_origin, ray_dir, hit_data, prd);
				else
					intersection_extruded_polygon(ray_origin, ray_dir, hit_data, prd);
			} break;

			case SceneObjectType::STREET_LAMP:
			{
				intersection_street_lamp(ray_origin, ray_dir, hit_data, prd);
			} break;

			case SceneObjectType::JOINT_ROAD:
			case SceneObjectType::EDGE_ROAD:
			case SceneObjectType::SIDEWALK:
			{
				if (OPTIX_PARAMETERS_VAR.procedural_geometry_generation_parameters.render_road_and_sidewalk_aabbs)
					intersection_aabb(ray_origin, ray_dir, hit_data, prd);
				else
					intersection_extruded_polygon(ray_origin, ray_dir, hit_data, prd);
			} break;		

			default:
			{
				intersection_aabb(ray_origin, ray_dir, hit_data, prd);
			} break;
		}
	}	

	/// <summary>
	/// Generates an image by using the developed algorithm.
	/// 
	/// Uses some things from a corresponding function from an Nvidia sample project.
	/// </summary>
	extern "C" __global__ void __raygen__rg()
	{
		const int w = OPTIX_PARAMETERS_VAR.width;
		const int h = OPTIX_PARAMETERS_VAR.height;
		const float3 eye = OPTIX_PARAMETERS_VAR.eye;
		const float3 U = OPTIX_PARAMETERS_VAR.U;
		const float3 V = OPTIX_PARAMETERS_VAR.V;
		const float3 W = OPTIX_PARAMETERS_VAR.W;
		const uint3 idx = optixGetLaunchIndex();
		const int subframe_index = OPTIX_PARAMETERS_VAR.subframe_index;
		const int number_of_samples = OPTIX_PARAMETERS_VAR.ray_parameters.samples_per_launch;

		unsigned int seed = tea<4>(idx.y * w + idx.x, subframe_index);
		float3 result = make_float3(0.0f);
		for (int i = 0; i < number_of_samples; i++)
		{
			float3 ray_direction;
			float3 ray_origin;

			{
				// The center of each pixel is at fraction (0.5, 0.5).
				const float2 subpixel_jitter = make_float2(rnd(seed), rnd(seed));

				// Some randomization.
				const float2 d = 2.0f * make_float2(
					(static_cast<float>(idx.x) + subpixel_jitter.x) / static_cast<float>(w),
					(static_cast<float>(idx.y) + subpixel_jitter.y) / static_cast<float>(h)
				) - 1.0f;

				ray_direction = normalize(d.x * U + d.y * V + W);
				ray_origin = eye;
			}

			result += path_tracing_loop(seed, 0, 1, ray_origin, ray_direction, SceneObjectType::CAMERA, SceneObjectPart::SCENE_OBJECT_PART_COUNT, MaterialType::MATERIAL_COUNT);
		}

		const uint3 launch_index = optixGetLaunchIndex();
		const unsigned int image_index = launch_index.y * OPTIX_PARAMETERS_VAR.width + launch_index.x;
		float3 accum_color = result / static_cast<float>(OPTIX_PARAMETERS_VAR.ray_parameters.samples_per_launch);

		if (subframe_index > 0)
		{
			/*const float a = 1.0f / static_cast<float>(subframe_index + 1);
			const float3 accum_color_prev = make_float3(OPTIX_PARAMETERS_VAR.accum_buffer[image_index]);
			accum_color = lerp(accum_color_prev, accum_color, a);*/

			const float3 accum_color_prev = make_float3(OPTIX_PARAMETERS_VAR.accum_buffer[image_index]);
			accum_color
				+= float(subframe_index)
				* accum_color_prev;
			accum_color /= (float(subframe_index) + 1.f);
		}

		OPTIX_PARAMETERS_VAR.accum_buffer[image_index] = make_float4(accum_color, 1.0f);
		OPTIX_PARAMETERS_VAR.frame_buffer[image_index] = make_color(accum_color);
	}
}
