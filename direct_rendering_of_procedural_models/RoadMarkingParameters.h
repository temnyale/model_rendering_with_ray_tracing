/**
 * Parameters for the road marking lines.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include "MarkingLineParameters.h"

namespace drpm
{
	class RoadMarkingParameters
	{
	public:
		bool calculated;

		float side_gap;
		float end_gap;

		MarkingLineParameters central_line_parameters;

		int number_of_lanes;
		float lane_width;
		MarkingLineParameters lane_separator_parameters;

		__forceinline__ __device__ void calculate_road_markings_parameters(float road_length, float road_width)
		{
			central_line_parameters.number_of_lines = 0;
			lane_separator_parameters.number_of_lines = 0;
			side_gap = drpm::road_markings_side_gap;

			if (road_width < min_road_width_for_markings)
				return;

			end_gap = drpm::road_markings_end_gap;
			lane_width = drpm::road_lane_width;

			number_of_lanes = static_cast<int>(road_width / lane_width);

			if (number_of_lanes % 2 != 0)
				number_of_lanes -= 1;

			if (number_of_lanes < 2)
			{
				number_of_lanes = 0;
				return;
			}

			float remaining_width = road_width - static_cast<float>(number_of_lanes) * lane_width;

			lane_width += remaining_width / static_cast<float>(number_of_lanes);

			central_line_parameters.calculate_line_parameters(
				road_length - 2.f * end_gap,
				road_width,
				MarkingLineType::CENTRAL_LINE
			);

			lane_separator_parameters.calculate_line_parameters(
				road_length - 2.f * end_gap,
				road_width,
				MarkingLineType::LANE_SEPARATOR
			);

			calculated = true;
		}
	};
}