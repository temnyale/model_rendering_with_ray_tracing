/**
 * The entry point into the program.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#include "Program.h"
#include "general_settings.h"
#include "path_settings.h"

/// <summary>
/// If a wrong argument was passed or the user requested to print help, it prints it.
/// </summary>
void print_usage_and_exit();
/// <summary>
/// Prints the current settings for the rendering application.
/// </summary>
void print_current_settings();
/// <summary>
/// It checks if some arguments were passed.
/// </summary>
void process_input_arguments(int argc, char* argv[]);

int main(int argc, char* argv[])
{
	process_input_arguments(argc, argv);
    print_current_settings();

	try
	{
        drpm::Program::initialize();

        drpm::Program* program = drpm::Program::get_instance();

        if (program == nullptr)
        {
            std::cerr << "program == nullptr\n";
            exit(-1);
        }

        program->run();

        delete program;
	}
	catch (std::exception& e)
	{
		std::cerr << "Caught exception: " << e.what() << "\n";
        exit(-1);
	}

	return 0;
}

void print_usage_and_exit()
{
    const std::string geometry_representation_value = "0 - traditional, 1 - procedural.";

    std::cerr << "\n";
    std::cerr << "\n";
    std::cerr << "===================================================================================================================\n";
    std::cerr << "\n";
    std::cerr << "\n";
    std::cerr << "Options:\n";
    std::cerr << "         --seed=<value>                   Sets the seed.\n";
    std::cerr << "         --dim=<width>x<height>           Sets the window dimensions. Values must be greater than 0.\n";
    std::cerr << "         --building-min-h=<value>         Sets the minimum height of buildings. A value must be greater than 0.\n";
    std::cerr << "         --building-max-h=<value>         Sets the maximum height of buildings. A value must be greater than 0 and greater than the minimum height.\n";
    std::cerr << "         --city=<path>                    Sets the name of the scene.\n";
    std::cerr << "         --heightmap=<path>               Sets the path to the heightmap.\n";
    std::cerr << "         --lamp-collider-size=<value>     Sets the of the collider for the street lamps.\n";
    std::cerr << "         --geometry-terrain=<value>       Sets the geometry representation for the terrain. " << geometry_representation_value << "\n";
    std::cerr << "         --geometry-buildings=<value>     Sets the geometry representation for the buildings. " << geometry_representation_value << "\n";
    std::cerr << "         --geometry-edge-roads=<value>    Sets the geometry representation for the edge roads. " << geometry_representation_value << "\n";
    std::cerr << "         --geometry-joint-roads=<value>   Sets the geometry representation for the joint roads. " << geometry_representation_value << "\n";
    std::cerr << "         --geometry-sidewalks=<value>     Sets the geometry representation for the sidewalks. " << geometry_representation_value << "\n";
    std::cerr << "         --print-memory-info              Prints the amount of the total, used and free memory. " << "\n";
    std::cerr << "         --print-scene-info               Prints the number of the buildings, roads, sidewalks and lamps. " << "\n";
    std::cerr << "         --print-scene-geometry-info      Prints an approximation of required memory for the scene. " << "\n";
    std::cerr << "         --cu-out-buf=<BUFFER TYPE>       Chooses the CUDA output buffer type. CUDA_DEVICE, GL_INTEROP, ZERO_COPY or CUDA_P2P." << "\n";
    std::cerr << "         --help | -h                      Prints this usage message.\n";
    std::cerr << "\n";
    std::cerr << "\n";
    std::cerr << "===================================================================================================================\n";
    std::cerr << "\n";
    std::cerr << "\n";
    exit(0);
}

static std::string cuda_output_buffer_type_to_string(sutil::CUDAOutputBufferType cuda_output_buffer_type)
{
    switch (cuda_output_buffer_type)
    {
        case sutil::CUDAOutputBufferType::CUDA_DEVICE:
            return "CUDA_DEVICE";

        case sutil::CUDAOutputBufferType::GL_INTEROP:
            return "GL_INTEROP";

        case sutil::CUDAOutputBufferType::ZERO_COPY:
            return "ZERO_COPY";

        case sutil::CUDAOutputBufferType::CUDA_P2P:
            return "CUDA_P2P";
    }
}

void print_current_settings()
{
    std::cout << "\n";
    std::cout << "\n";
    std::cout << "===================================================================================================================\n";
    std::cout << "\n";
    std::cout << "\n";
    std::cout << "Current settings:\n";
    std::cout << "      Window size:                       " << drpm::application_window_width << "x" << drpm::application_window_height << "\n";
    std::cout << "      Seed:                              " << drpm::global_seed << "\n";
    std::cout << "      Scene:                             " << drpm::path_to_city << "\n";
    std::cout << "      Height map:                        " << (drpm::path_to_heightmap == "" ? "no" : drpm::path_to_heightmap) << "\n";
    std::cout << "      Street lamp collider size:         " << drpm::street_lamp_collider_size << "\n";
    std::cout << "      Building minimum height:           " << drpm::building_min_height << "\n";
    std::cout << "      Building maximum height:           " << drpm::building_max_height << "\n";
    std::cout << "      Procedural terrain:                " << (drpm::procedural_terrain ? "yes" : "no") << "\n";
    std::cout << "      Procedural buildings:              " << (drpm::procedural_buildings ? "yes" : "no") << "\n";
    std::cout << "      Procedural edge roads:             " << (drpm::procedural_edge_roads ? "yes" : "no") << "\n";
    std::cout << "      Procedural joint roads:            " << (drpm::procedural_joint_roads ? "yes" : "no") << "\n";
    std::cout << "      Procedural sidewalks:              " << (drpm::procedural_sidewalks ? "yes" : "no") << "\n";
    std::cout << "      Print memory information:          " << (drpm::print_memory_info ? "yes" : "no") << "\n";
    std::cout << "      Print scene information:           " << (drpm::print_scene_info ? "yes" : "no") << "\n";
    std::cout << "      Print scene geometry information:  " << (drpm::print_scene_geometry_info ? "yes" : "no") << "\n";
    std::cout << "      CUDA output buffer type:           " << cuda_output_buffer_type_to_string(drpm::cuda_output_buffer_type) << "\n";
    std::cout << "\n";
    std::cout << "\n";
    std::cout << "===================================================================================================================\n";
    std::cout << "\n";
    std::cout << "\n";
}

void process_input_arguments(int argc, char* argv[])
{
    for (int i = 1; i < argc; i++)
    {
        const std::string arg_s = argv[i];
        std::string arg_value_s;

        if (arg_s == "--help" || arg_s == "-h")
        {
            print_usage_and_exit();
        }
        else if (arg_s.substr(0, 6) == "--dim=")
        {
            arg_value_s = arg_s.substr(6);

            int w, h;
            sutil::parseDimensions(arg_value_s.c_str(), w, h);
            if (w <= 0 || h <= 0)
                print_usage_and_exit();

            drpm::application_window_width = w;
            drpm::application_window_height = h;
        }
        else if (arg_s.substr(0, 7) == "--seed=")
        {
            arg_value_s = arg_s.substr(7);

            drpm::global_seed = std::stoi(arg_value_s);
        }
        else if (arg_s.substr(0, 7) == "--city=")
        {
            arg_value_s = arg_s.substr(7);

            drpm::path_to_city = arg_value_s;
        }
        else if (arg_s.substr(0, 12) == "--heightmap=")
        {
            arg_value_s = arg_s.substr(12);

            drpm::path_to_heightmap = arg_value_s;
        }
        else if (arg_s.substr(0, 21) == "--lamp-collider-size=") 
        {
            arg_value_s = arg_s.substr(21);

            float arg_value_f = std::stof(arg_value_s);
            if (arg_value_f <= 0)
                print_usage_and_exit();

            drpm::street_lamp_collider_size = arg_value_f;
        }
        else if (arg_s.substr(0, 17) == "--building-min-h=")
        {
            arg_value_s = arg_s.substr(17);

            float arg_value_f = std::stof(arg_value_s);
            if (arg_value_f <= 0)
                print_usage_and_exit();

            drpm::building_min_height = arg_value_f;
        }
        else if (arg_s.substr(0, 17) == "--building-max-h=")
        {
            arg_value_s = arg_s.substr(17);

            float arg_value_f = std::stof(arg_value_s);
            if (arg_value_f <= 0 || arg_value_f < drpm::building_min_height)
                print_usage_and_exit();

            drpm::building_max_height = arg_value_f;
        }
        else if (arg_s.substr(0, 19) == "--geometry-terrain=")
        {
            arg_value_s = arg_s.substr(19);

            drpm::procedural_terrain = std::stoi(arg_value_s);
        }
        else if (arg_s.substr(0, 21) == "--geometry-buildings=")
        {
            arg_value_s = arg_s.substr(21);

            drpm::procedural_buildings = std::stoi(arg_value_s);
        }
        else if (arg_s.substr(0, 22) == "--geometry-edge-roads=")
        {
            arg_value_s = arg_s.substr(22);

            drpm::procedural_edge_roads = std::stoi(arg_value_s);
        }
        else if (arg_s.substr(0, 23) == "--geometry-joint-roads=")
        {
            arg_value_s = arg_s.substr(23);

            drpm::procedural_joint_roads = std::stoi(arg_value_s);
        }
        else if (arg_s.substr(0, 21) == "--geometry-sidewalks=")
        {
            arg_value_s = arg_s.substr(21);

            drpm::procedural_sidewalks = std::stoi(arg_value_s);
        }
        else if (arg_s.substr(0, 19) == "--print-memory-info")
        {
            drpm::print_memory_info = true;
        }
        else if (arg_s.substr(0, 18) == "--print-scene-info")
        {
            drpm::print_scene_info = true;
        }
        else if (arg_s.substr(0, 27) == "--print-scene-geometry-info")
        {
            drpm::print_scene_geometry_info = true;
        }
        else if (arg_s.substr(0, 13) == "--cu-out-buf=")
        {
            arg_value_s = arg_s.substr(13);

            if (arg_value_s == "CUDA_DEVICE")
            {
                drpm::cuda_output_buffer_type = sutil::CUDAOutputBufferType::CUDA_DEVICE;
            }
            else if (arg_value_s == "GL_INTEROP")
            {
                drpm::cuda_output_buffer_type = sutil::CUDAOutputBufferType::GL_INTEROP;
            }
            else if (arg_value_s == "ZERO_COPY")
            {
                drpm::cuda_output_buffer_type = sutil::CUDAOutputBufferType::ZERO_COPY;
            }
            else if (arg_value_s == "CUDA_P2P")
            {
                drpm::cuda_output_buffer_type = sutil::CUDAOutputBufferType::CUDA_P2P;
            }
            else
            {
                std::cerr << "Unknown CUDA output buffer type '" << arg_value_s << "'\n";
                print_usage_and_exit();
            }
        }
        else
        {
            std::cerr << "Unknown option '" << argv[i] << "'\n";
            print_usage_and_exit();
        }
    }
}
