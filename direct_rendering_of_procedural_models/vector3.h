/**
 * Functions that create often used vectors.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <vector_types.h>
#include <helpers.h>

namespace drpm
{
	namespace vector3
	{
		__forceinline__ __device__ float3 up()
		{
			return make_float3(0.0f, 1.0f, 0.0f);
		}

		__forceinline__ __device__ float3 left()
		{
			return make_float3(1.0f, 0.0f, 0.0f);
		}

		__forceinline__ __device__ float3 forward()
		{
			return make_float3(0.0f, 0.0f, 1.0f);
		}

		__forceinline__ __device__ float3 zero()
		{
			return make_float3(0.0f, 0.0f, 0.0f);
		}

		__forceinline__ __device__ float3 one()
		{
			return make_float3(1.0f, 1.0f, 1.0f);
		}
	}
}

