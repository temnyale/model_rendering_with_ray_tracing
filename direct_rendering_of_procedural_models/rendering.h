﻿/**
 * Data structures for the rendering.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#ifndef __RENDERING_H__
#define __RENDERING_H__

#include <vector_types.h>
#include <cuda/helpers.h>
#include "random.h"

#include <optix.h>

#include <GeometryData.h>

#include "general_settings.h"
#include "FacadeParameters.h"
#include "FacadeSlotParameters.h"
#include "RoadMarkingParameters.h"

#define OPTIX_PARAMETERS_VAR params

namespace drpm
{
	enum RayType
	{
		RAY_TYPE_RADIANCE = 0,

		RAY_TYPE_COUNT
	};

	enum MaterialType
	{
		DIFFUSE = 0,
		MIRROR,
		NON_TRANSPARENT_GLASS,
		GLASS,

		MATERIAL_COUNT
	};

	enum SceneObjectType
	{
		TERRAIN = 0,
		EDGE_ROAD,
		JOINT_ROAD,
		SIDEWALK,
		BUILDING,
		STREET_LAMP,
		LIGHT_SOURCE_COLLIDER,		
		SKYDOME,
		CAMERA,
		NORMAL_GEOMETRY_TEST,

		OBJECT_TYPE_COUNT
	};

	enum SceneObjectPart
	{
		BUILDING_WINDOW = 0,
		BUILDING_WALL,
		BUILDING_ROOF,
		BALCONY_FENCE,
		BALCONY_GLASS,
		BALCONY_WALL,
		WINDOW_GLASS,
		WINDOW_IMPOST,
		ROOM_WALL,
		ROOM_BACKGROUND_WALL,
		STREET_LAMP_PILLAR,
		STREET_LAMP_GLASS_SPHERE,
		STREET_LAMP_BULB,

		SCENE_OBJECT_PART_COUNT
	};	

	struct TriangleData
	{
		SceneObjectPart scene_object_part;
		int scene_object_part_id;
		float2 UVs[3];
	};

	struct Texture
	{
		cudaTextureObject_t texture_object;
		float t_h;
		float t_v;
	};

	struct DiffuseReflectionParameters
	{
		float k_d = 0.7f;
		float k_s = 0.3f;
		float n_s = 100.0f;
	};

	struct GlassParameters
	{
		float refractive_index = 1.5f;
		float fresnel_exponent = 5.0f;
		float fresnel_minimum = 0.05f;
		float fresnel_maximum = 1.0f;
	};

	struct Material
	{
		bool has_texture;
		union
		{
			Texture texture;
			float3 diffuse_color;
		};
		float3 emissive_color;
		union
		{
			DiffuseReflectionParameters diffuse_reflection_parameters;
			GlassParameters glass_parameters;
		};

		Material() { }
	};

	struct BuildingProperties
	{
		RegularBuildingFacadeParameters* facade_params;
	};

	struct LightSourceColliderProperties
	{
		OptixAabb lamp_aabb;
	};

	struct ProceduralGeometryGenerationParameters
	{
		bool render_building_aabbs = true;
		bool render_road_and_sidewalk_aabbs = true;
		bool generate_building_geometry = false;
	};

	struct SkydomeParameters
	{
		float3 emissive_color;
		float3 diffuse_color_factor;
		float3 texture_color_factor;
		union
		{
			int texture_number;
			cudaTextureObject_t texture_object;
		};
	};

	struct RayParameters
	{
		int samples_per_launch;
		int russian_roulette_depth;
		float ray_termination_probability;
	};

	struct Parameters
	{
		unsigned int subframe_index;
		float4* accum_buffer;
		uchar4* frame_buffer;
		unsigned int width;
		unsigned int height;

		RayParameters ray_parameters;

		float3 eye;
		float3 U;
		float3 V;
		float3 W;

		OptixTraversableHandle procedural_geometry_traversable;
		OptixTraversableHandle normal_geometry_traversable;

		Material grass_material;
		Material lamp_material;
		Material street_lamp_bulb_material;
		Material road_asphalt_material;
		Material sidewalk_asphalt_material;
		Material balcony_fence_material;
		Material window_impost_material;

		Material* regular_bulding_materials;
		int number_of_regular_building_materials;

		Material* inside_room_materials;
		int number_of_inside_room_materials;

		Material street_lamp_glass;
		Material window_glass;
		Material balcony_glass;

		float window_emissive_color_factor = 0.05f;
		float shop_window_emissive_color_factor = 0.05f;
		float lamp_secondary_emissive_energy_factor = 0.1f;
		float window_with_lights_on_percentage = 0.0f;
		float shop_window_with_lights_on_percentage = 0.0f;
		bool street_lamps_on = false;
		float3 street_lamp_emissive_color = { 1.f, 1.f, 1.f };
		float street_lamp_emissive_energy_factor = 1.0f;
		bool dont_use_street_lamp_colliders = false;

		float perlin_noise_scale = 1.0f;
		int perlin_noise_n = 32;
		float perlin_noise_lacunarity = 3.0f;
		float perlin_noise_decay = 0.6f;
		float perlin_noise_min = 0.15f;
		float perlin_noise_max = 1.5f;

		ProceduralGeometryGenerationParameters procedural_geometry_generation_parameters;
	};	

	struct Polygon
	{
		int number_of_nodes;
		float2* contour_nodes;
	};		

	struct RayGenData
	{

	};

	struct MissData
	{
		SkydomeParameters skydome_parameters;
	};	

	enum GeometryType
	{
		gt_PROCEDURAL,
		gt_TRADITIONAL,

		gt_COUNT
	};

	struct ProceduralGeometryHitGroupData
	{
		union
		{
			Polygon polygon;
			GeometryData::Sphere sphere;
		} geometry;

		union
		{
			BuildingProperties building_properties;
			LightSourceColliderProperties street_lamp_collider_properties;
			RoadMarkingParameters road_marking_properties;
		} miscellaneous;
	};

	struct TraditionalGeometryHitGroupData
	{
		float3* vertices;
		SceneObjectPart* object_parts;
		int* object_part_ids;
		float2* UVs;
	};

	struct HitGroupData
	{
		bool first_pass = true;
		unsigned int seed = 0;
		SceneObjectType scene_object_type;
		OptixAabb aabb;
		Material material;

		GeometryType geometry_type;
		union
		{
			ProceduralGeometryHitGroupData procedural_geometry_hit_group_data;
			TraditionalGeometryHitGroupData traditional_geometry_hit_group_data;
		};
	};


	template <typename T>
	struct Record
	{
		__align__(OPTIX_SBT_RECORD_ALIGNMENT) char header[OPTIX_SBT_RECORD_HEADER_SIZE];
		T data;
	};

	typedef Record<RayGenData> RayGenRecord;
	typedef Record<MissData> MissRecord;
	typedef Record<HitGroupData> HitGroupRecord;
}

#endif