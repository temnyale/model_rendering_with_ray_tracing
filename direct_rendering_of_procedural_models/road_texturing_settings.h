/**
 * Settings for the road texturing.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

namespace drpm
{
	const float road_markings_end_gap = 1.0f;
	const float road_central_line_width = 0.15f;
	const float road_central_line_length = 2.5f;
	const float gap_between_road_central_lines = 1.25f;
	const float road_lane_width = 2.4f;
	const float road_lane_separator_width = 0.1f;
	const float road_lane_separator_length = 1.25f;
	const float gap_between_road_lane_separators = 1.0f;
	const float road_markings_side_gap = 0.2f;
}