#pragma once

#ifndef OPTIX_HELPERS_CUH
#define OPTIX_HELPERS_CUH

// From an Nvidia sample project.
#define float3_as_ints( u ) float_as_int( u.x ), float_as_int( u.y ), float_as_int( u.z )

//// From an Nvidia sample project.
//#define float3_as_args(u) \
//    reinterpret_cast<unsigned int&>((u).x), \
//    reinterpret_cast<unsigned int&>((u).y), \
//    reinterpret_cast<unsigned int&>((u).z)

#include <optix.h>
#include <optix_device.h>
#include <cuda/helpers.h>

namespace drpm
{
	// From an Nvidia sample project.
	__forceinline__ __device__ void* unpack_pointer(unsigned int i0, unsigned int i1)
	{
		const unsigned long long uptr = static_cast<unsigned long long>(i0) << 32 | i1;
		void* ptr = reinterpret_cast<void*>(uptr);
		return ptr;
	}

	// From an Nvidia sample project.
	__forceinline__ __device__ void  pack_pointer(void* ptr, unsigned int& i0, unsigned int& i1)
	{
		const unsigned long long uptr = reinterpret_cast<unsigned long long>(ptr);
		i0 = uptr >> 32;
		i1 = uptr & 0x00000000ffffffff;
	}	

    __forceinline__ __device__ float3 toSRGB(const float3& c)
    {
        float  invGamma = 1.0f / 2.4f;
        float3 powed = make_float3(powf(c.x, invGamma), powf(c.y, invGamma), powf(c.z, invGamma));
        return make_float3(
            c.x < 0.0031308f ? 12.92f * c.x : 1.055f * powed.x - 0.055f,
            c.y < 0.0031308f ? 12.92f * c.y : 1.055f * powed.y - 0.055f,
            c.z < 0.0031308f ? 12.92f * c.z : 1.055f * powed.z - 0.055f);
    }

    //__forceinline__ __device__ unsigned char quantizeUnsigned8Bits(float x)
    //{
    //    x = clamp(x, 0.0f, 1.0f);
    //    enum {
    //        N = (1 << 8) - 1, Np1 = (1 << 8)
    //    };
    //    return (unsigned char)min((unsigned int)(x * (float)Np1), (unsigned int)N);
    //}

    //__forceinline__ __device__ uchar4 make_color(const float3& c)
    //{
    //    // first apply gamma, then convert to unsigned char
    //    float3 srgb = toSRGB(clamp(c, 0.0f, 1.0f));
    //    return make_uchar4(quantizeUnsigned8Bits(srgb.x), quantizeUnsigned8Bits(srgb.y), quantizeUnsigned8Bits(srgb.z), 255u);
    //}

    //__forceinline__ __device__ uchar4 make_color(const float4& c)
    //{
    //    return make_color(make_float3(c.x, c.y, c.z));
    //}
}

#endif // !OPTIX_HELPERS_CUH
