/**
 * Mathematical functions.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <vector_types.h>
#include "rendering.h"
#include "vector3.h"

namespace drpm
{
    struct Triangle
    {
        float3 a, b, c;

        __forceinline__ __device__ float3 get_normal() const
        {
            return normalize(cross(b - a, c - a));
        }

        __forceinline__ __device__ Triangle get_reversed() const
        {
            Triangle reversed;
            reversed.a = c;
            reversed.b = b;
            reversed.c = a;
            return reversed;
        }

        __forceinline__ __device__ Triangle operator+(const float3& direction) const
        {
            Triangle ret;
            ret.a = this->a + direction;
            ret.b = this->b + direction;
            ret.c = this->c + direction;
            return ret;
        }

        __forceinline__ __device__ Triangle operator-(const float3& direction) const
        {
            return (*this + (-direction));
        }
    };

    /// <summary>
    /// To define a wall,
    /// a is the left lower point,
    /// b is the right lower point,
    /// c is the right upper point,
    /// d is the left upper point.
    /// </summary>
    struct Quadrangle
    {
        float3 a, b, c, d;

        __forceinline__ __device__ float3 get_normal() const
        {
            return normalize(cross(d - a, b - a));
        }

        __forceinline__ __device__ float3 get_vertical_direction() const
        {
            return normalize(d - a);
        }

        __forceinline__ __device__ float3 get_horizontal_direction() const
        {
            return normalize(b - a);
        }

        __forceinline__ __device__ float get_width() const
        {
            return length(b - a);
        }

        __forceinline__ __device__ float get_height() const
        {
            return length(d - a);
        }

        __forceinline__ __device__ Quadrangle get_left(float width) const
        {
            Quadrangle left;

            left.b = a;
            left.c = d;
            left.a = left.b - get_normal() * width;
            left.d = left.c - get_normal() * width;

            return left;
        }

        __forceinline__ __device__ Quadrangle get_right(float width) const
        {
            Quadrangle right;

            right.a = b;
            right.d = c;
            right.b = right.a - get_normal() * width;
            right.c = right.d - get_normal() * width;

            return right;
        }

        __forceinline__ __device__ Quadrangle get_top(float height) const
        {
            Quadrangle top;

            top.a = d;
            top.b = c;
            top.d = top.a - get_normal() * height;
            top.c = top.b - get_normal() * height;

            return top;
        }

        __forceinline__ __device__ Quadrangle get_bottom(float height) const
        {
            Quadrangle bottom;

            bottom.d = a;
            bottom.c = b;
            bottom.a = bottom.d - get_normal() * height;
            bottom.b = bottom.c - get_normal() * height;

            return bottom;
        }

        __forceinline__ __device__ Quadrangle get_opposite(float distance) const
        {
            return get_reversed() - get_normal() * distance;
        }

        __forceinline__ __device__ Quadrangle get_reversed() const
        {
            Quadrangle reversed;
            reversed.a = b;
            reversed.b = a;
            reversed.c = d;
            reversed.d = c;
            return reversed;
        }

        __forceinline__ __device__ static Quadrangle create_from_bottom_vertices(const float3& bottom_left, const float3& bottom_right, const float3& up, const float height)
        {
            Quadrangle ret;
            ret.a = bottom_left;
            ret.b = bottom_right;
            ret.d = ret.a + up * height;
            ret.c = ret.b + up * height;
            return ret;
        }

        __forceinline__ __device__ Quadrangle operator+(const float3& direction) const
        {
            Quadrangle ret;
            ret.a = this->a + direction;
            ret.b = this->b + direction;
            ret.c = this->c + direction;
            ret.d = this->d + direction;
            return ret;
        }

        __forceinline__ __device__ Quadrangle operator-(const float3& direction) const
        {
            return (*this + (-direction));
        }

        __forceinline__ __device__ void to_triangles(Triangle triangles[2]) const
        {
            triangles[0] = { a, b, c };
            triangles[1] = { c, d, a };
        }

        __forceinline__ __device__ void split_vertically_at_distance_from_a(const float dist_from_a, Quadrangle quadrangles[2]) const
        {
            quadrangles[0].a = a;
            quadrangles[0].d = d;
            quadrangles[0].b = quadrangles[0].a + get_horizontal_direction() * dist_from_a;
            quadrangles[0].c = quadrangles[0].d + get_horizontal_direction() * dist_from_a;

            quadrangles[1].b = b;
            quadrangles[1].c = c;
            quadrangles[1].b = quadrangles[0].b;
            quadrangles[1].c = quadrangles[0].c;
        }
    };


    /// <summary>
    /// Orthonormal basis.
    /// For example, if we have the vector of a reflected ray in the standard base, we want to transform to ONB.
    /// </summary>
    __forceinline__ __device__ void transform_to_onb(const float3 normal, float3& point_to_transform)
    {
        float3 tangent;
        float3 binormal;

        {
            float3 w = fabsf(normal.x) > 0.99 ? make_float3(0, 0, 1) : make_float3(1, 0, 0);
            tangent = normalize(cross(normal, w));
            binormal = cross(tangent, normal);
        }

        {
            point_to_transform = point_to_transform.x * tangent + point_to_transform.y * normal + point_to_transform.z * binormal;
        }
    }

    // From optixWhitted.
    __forceinline__ __device__ bool refract(float3& r, const float3& i, const float3& n, const float ior) 
    {
        float3 nn = n;
        float negNdotV = dot(i, nn);
        float eta;

        if (negNdotV > 0.0f)
        {
            eta = ior;
            nn = -n;
            negNdotV = -negNdotV;
        }
        else
        {
            eta = 1.f / ior;
        }

        const float k = 1.f - eta * eta * (1.f - negNdotV * negNdotV);

        if (k < 0.0f)
        {
            // Initialize this value, so that r always leaves this function initialized.
            r = make_float3(0.f);
            return false;
        }
        else
        {
            r = normalize(eta * i - (eta * negNdotV + sqrtf(k)) * nn);
            return true;
        }
    }

    // From optixWhitted.
    __forceinline__ __device__ float fresnel_schlick(const float cos_theta, const float exponent = 5.0f,
        const float minimum = 0.0f, const float maximum = 1.0f)
    {
        /**
          Clamp the result of the arithmetic due to floating point precision:
          the result should lie strictly within [minimum, maximum]
          return clamp(minimum + (maximum - minimum) * powf(1.0f - cos_theta, exponent),
                       minimum, maximum);
        */

        /** The max doesn't seem like it should be necessary, but without it you get
            annoying broken pixels at the center of reflective spheres where cos_theta ~ 1.
        */
        return clamp(minimum + (maximum - minimum) * powf(fmaxf(0.0f, 1.0f - cos_theta), exponent),
            minimum, maximum);
    }

    __forceinline__ __device__ float deg2rad(const float deg)
    {
        return deg * 0.0174533f;
    }

    /// <summary>
    /// Maps a value from the source interval to the destination interval.
    /// </summary>
    __forceinline__ __device__ float map(const float value, const float src_range_min, const float src_range_max, const float dst_range_min, const float dst_range_max)
    {
        return dst_range_min + ((dst_range_max - dst_range_min) / (src_range_max - src_range_min)) * (value - src_range_min);
    }

    __forceinline__ __device__ float2 float3_to_float2(const float3& vec)
    {
        return make_float2(vec.x, vec.z);
    }

    __forceinline__ __device__ float3 float2_to_float3(const float2& vec, const float y)
    {
        return make_float3(vec.x, y, vec.y);
    }    

    __forceinline__ __device__ float get_projection(float3 line_to_project, float3 axis)
    {
        return fabsf(dot(line_to_project, axis));
    }

    __forceinline__ __device__ float get_projection(float2 line_to_project, float2 axis)
    {
        float3 line_to_project_3d = make_float3(line_to_project.x, 0.f, line_to_project.y);
        float3 axis_3d = normalize(make_float3(axis.x, 0.f, axis.y));
        return get_projection(line_to_project_3d, axis_3d);
    }

    __forceinline__ __device__ bool is_point_between_planes(float3 plane_1_point, float3 plane_1_normal, float3 plane_2_point, float3 plane_2_normal, float3 point)
    {
        float3 P_P1 = point - plane_1_point;
        float3 P_P2 = point - plane_2_point;

        return dot(plane_1_normal, P_P1) >= 0.f && dot(plane_2_normal, P_P2) >= 0.f;
    }

    __forceinline__ __device__ float distance_between_plane_and_point(float3 plane_point, float3 plane_normal, float3 point)
    {
        return dot((plane_point - point), plane_normal);
    }

    __forceinline__ __device__ float distance_between_line_and_point(float3 line_point, float3 line_dir, float3 point)
    {
        float3& AB = line_dir;
        float3 AC = point - line_point;
        //float area = Vector(AB * AC).magnitude();
        float area = length(cross(AB, AC));
        float CD = area / length(AB);
        return CD;
    }

    __forceinline__ __device__ float distance_between_lines(float3 line1_point, float3 line1_dir, float3 line2_point, float3 line2_dir)
    {
        if (fabsf(dot(line1_dir, line2_dir)) > 0.999f)
        {
            return distance_between_line_and_point(line1_point, line1_dir, line2_point);
        }
        float3 N = cross(line1_dir, line2_dir);       
        float3 PQ = line1_point - line2_point;

        return fabsf(dot(PQ, N) / length(N));
    }

    __forceinline__ __device__ bool is_point_inside_cube(
        const float3& a, const float3& b, const float3& c, const float3& d, 
        const float3& e, const float3& f, const float3& g, const float3& h,
        const float3& p,
        const float eps = 1e-4
    )
    {
        const float x_size = length(b - a);
        const float z_size = length(c - b);
        const float y_size = length(e - a);

        const float3 x_dir = normalize(b - a);
        const float3 z_dir = normalize(c - b);
        const float3 y_dir = normalize(e - a);

        float3 i = (a + g) / 2.0f;

        const float3 pi = p - i;

        const float x_proj = fabsf(dot(pi, x_dir));
        const float y_proj = fabsf(dot(pi, y_dir));
        const float z_proj = fabsf(dot(pi, z_dir));

        return 2.0f * x_proj <= x_size + eps && 2.0f * y_proj <= y_size + eps && 2.0f * z_proj <= z_size + eps;
    }

    __forceinline__ __device__ bool solve_quadratic_equation(float A, float B, float C, float& t0, float& t1, bool& one_root)
    {
        float eps = 1e-4f;

        one_root = false;

        float discriminant = (B * B) - (4.0f * A * C);

        if (discriminant < 0.0f)
            return false;
        else if (discriminant <= eps)
        {
            t0 = -B / (2.0f * A);
            t1 = -1e16;
            one_root = true;
            return true;
        }
        else
        {
            float root_from_discriminant = sqrtf(discriminant);

            t0 = (-B + root_from_discriminant) / (2.0f * A);
            t1 = (-B - root_from_discriminant) / (2.0f * A);

            return true;
        }
    }

    // https://en.wikipedia.org/wiki/M�ller�Trumbore_intersection_algorithm
    __forceinline__ __device__ bool does_ray_intersect_triangle(
        const float3& rayOrigin, const float3& rayVector,
        const float3& vertex0, const float3& vertex1, const float3& vertex2,
        float& t
    )
    {
        const float EPSILON_ = 0.0000001f;
        float3 edge1, edge2, h, s, q;
        float a, f, u, v;
        edge1 = vertex1 - vertex0;
        edge2 = vertex2 - vertex0;
        h = cross(rayVector, edge2);
        a = dot(edge1, h);
        if (a > -EPSILON_ && a < EPSILON_)
            return false;    // This ray is parallel to this triangle.
        f = 1.0f / a;
        s = rayOrigin - vertex0;
        u = f * dot(s, h);
        if (u < 0.0f || u > 1.0f)
            return false;
        q = cross(s, edge1);
        v = f * dot(rayVector, q);
        if (v < 0.0f || u + v > 1.0f)
            return false;
        // At this stage we can compute t to find out where the intersection point is on the line.
        t = f * dot(edge2, q);
        if ((t) > EPSILON_) // ray intersection
        {
            return true;
        }
        else // This means that there is a line intersection but not a ray intersection.
            return false;
    }

    __forceinline__ __device__ double calculate_area_of_triangle(const float3& AB, const float3& CB, const float& angle)
    {
        return length(AB) * length(CB) * sinf(angle) / 2.0f;
    }      

    __forceinline__ __device__ bool is_point_inside_wall(const Quadrangle& wall, const float3& M, const float& eps, const float& threshold)
    {
        // The area of a triangle ABC can be calculated as (length(AB) * length(CB) * sin(angle(AB, CB)) / 2).
        float3 AB = wall.a - wall.b;
        float3 CB = wall.c - wall.b;
        float angleAB_CB = acosf(dot(normalize(AB), normalize(CB)));
        float areaABC = calculate_area_of_triangle(AB, CB, angleAB_CB);

        float3 AD = wall.a - wall.d;
        float3 CD = wall.c - wall.d;
        float angleAD_CD = acosf(dot(normalize(AD), normalize(CD)));
        float areaADC = calculate_area_of_triangle(AD, CD, angleAD_CD);

        float correctAreaOfADC = areaABC + areaADC;

        float3 MB = M - wall.b;
        float3 MC = M - wall.c;
        float3 MA = M - wall.a;
        float3 MD = M - wall.d;

        float angleMB_MC = acosf(dot(normalize(MB), normalize(MC)));
        float areaMBC = calculate_area_of_triangle(MB, MC, angleMB_MC);

        float angleMB_MA = acosf(dot(normalize(MB), normalize(MA)));
        float areaMBA = calculate_area_of_triangle(MB, MA, angleMB_MA);

        float angleMA_MD = acosf(dot(normalize(MA), normalize(MD)));
        float areaMAD = calculate_area_of_triangle(MA, MD, angleMA_MD);

        float angleMC_MD = acosf(dot(normalize(MC), normalize(MD)));
        float areaMCD = calculate_area_of_triangle(MC, MD, angleMC_MD);

        float calculatedAreaOfABCD = areaMBC + areaMBA + areaMAD + areaMCD;

        if (fabsf(correctAreaOfADC - calculatedAreaOfABCD) <= eps * correctAreaOfADC)
            return true;

        return false;
    }

    // https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-plane-and-ray-disk-intersection
    __forceinline__ __device__ bool does_ray_intersect_plane(const float3& plane_normal, const float3& point_on_plane, const float3& ray_origin, const float3& ray_dir, float& t)
    {
        // assuming vectors are all normalized
        float denom = dot(plane_normal, ray_dir);
        if (denom != 0.0f)
        {
            float3 p0l0 = point_on_plane - ray_origin;
            t = dot(p0l0, plane_normal) / denom;
            return (t >= 0);
        }

        return false;
    }

    __forceinline__ __device__ bool is_point_inside_aabb(const float3& point, OptixAabb aabb, float eps = 0.0f)
    {
        if ((point.x >= aabb.minX - eps && point.x <= aabb.maxX + eps)
            && (point.y >= aabb.minY - eps && point.y <= aabb.maxY + eps)
            && (point.z >= aabb.minZ - eps && point.z <= aabb.maxZ + eps))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Returns 0 if no intersection.
    /// Returns 1 if there is an intersection and the ray's direction and the normal has the dot less than 0.
    /// Returns 2 if there is an intersection and the ray's direction and the normal has the dot equal to 0 or greater.
    /// </summary>
    /// <param name="wall"></param>
    /// <param name="ray_origin"></param>
    /// <param name="ray_dir"></param>
    /// <param name="t"></param>
    /// <param name="eps"></param>
    /// <param name="threshold"></param>
    /// <returns></returns>
    __forceinline__ __device__ int does_ray_intersect_wall_from_any_side(
        const Quadrangle& wall, const float3& ray_origin, const float3& ray_dir, float& t)
    {
        if (does_ray_intersect_plane(wall.get_normal(), wall.a, ray_origin, ray_dir, t))
        {
            float3 intersection_point = ray_origin + ray_dir * t;

            if (does_ray_intersect_triangle(ray_origin, ray_dir, wall.a, wall.b, wall.c, t) || does_ray_intersect_triangle(ray_origin, ray_dir, wall.a, wall.c, wall.d, t))
            {
                return dot(wall.get_normal(), ray_dir) < 0 ? 1 : 2;
            }
        }

        return 0;
    }

    __forceinline__ __device__ bool does_ray_intersect_wall(
        const Quadrangle& wall, const float3& ray_origin, const float3& ray_dir, float& t)
    {
        float dot_with_normal = dot(wall.get_normal(), ray_dir);

        if (dot_with_normal < 0.0f && does_ray_intersect_wall_from_any_side(wall, ray_origin, ray_dir, t))
        {           
            float3 intersection_point = ray_origin + ray_dir * t;      

            return true;
        }

        return false;
    }    

    __forceinline__ __device__ bool is_point_inside_sphere(const float3& point, const float3& center, const float& radius)
    {
        //float eps = 1e-4f;
        return length(point - center) <= radius/* + eps*/;
    }

    // It's taken from an Nvidia sample project named "optixWhitted".
    static __forceinline__ __device__ bool does_ray_intersect_sphere(
        const float3& ray_origin, const float3& ray_dir,
        const float3& center, const float& radius,
        bool& t1_suitable, float& t1, float3& intersection1, float3& normal1,
        bool& t2_suitable, float& t2, float3& intersection2, float3& normal2
    )
    {
        t1_suitable = false;
        t2_suitable = false;

        float d = distance_between_line_and_point(ray_origin, ray_dir, center);
        if (d <= radius)
        {
            float dist_Ro_cylinder = length(ray_origin - center);
            float __h = sqrtf(radius * radius - d * d);
            float __l = sqrtf(dist_Ro_cylinder * dist_Ro_cylinder - d * d);
            float __t3 = __l - __h;

            t1 = __t3;

            if (t1 >= 0.0f)
            {
                intersection1 = ray_origin + t1 * ray_dir;

                if (length(intersection1 - center) <= radius + 1e-4)
                {
                    t1_suitable = true;
                    normal1 = normalize(intersection1 - center);
                }
            }

            float __t2 = __l + __h;

            t2 = __t2;

            if (t2 >= 0.0f)
            {
                intersection2 = ray_origin + t2 * ray_dir;

                if (length(intersection2 - center) <= radius + 1e-4)
                {
                    t2_suitable = true;
                    normal2 = normalize(intersection2 - center);
                }
            }
        }     

        return t1_suitable || t2_suitable;
    }

    __forceinline__ __device__ bool does_ray_intersect_axis_aligned_disk(
        const float3& ray_origin, const float3& ray_dir,
        const float3& disk_normal, const float3& disk_center, const float& radius, float& t
    )
    {
        if (does_ray_intersect_plane(disk_normal, disk_center, ray_origin, ray_dir, t))
        {
            float3 p = ray_origin + ray_dir * t;
            float3 v = p - disk_center;
            //float d2 = dot(v, v);
            //return (sqrtf(d2) <= radius);
            return length(v) < radius;
        }

        return false;
    }

    static __forceinline__ __device__ bool check_ray_axis_aligned_cylinder_body_intersection(
        const float3& ray_origin, const float3& ray_dir, 
        const float& ray_tmin, const float& ray_tmax, const float& t,
        const float3& cylidner_bottom_disk_center, const float& radius, const float& height,
        float3& intersection, float3& normal
    )
    {
        const float max_y = cylidner_bottom_disk_center.y + height;
        const float min_y = cylidner_bottom_disk_center.y;

        intersection = ray_origin + t * ray_dir;
        if (intersection.y >= min_y && intersection.y <= max_y)
        {
            float3 _intersection = intersection;
            _intersection.y = cylidner_bottom_disk_center.y;

            normal = normalize(_intersection - cylidner_bottom_disk_center);

            return true;
        }
        else
        {
            return false;
        }
    }

    __forceinline__ __device__ bool does_ray_intersect_axis_aligned_cylinder(
        const float3& ray_origin, const float3& ray_dir,
        const float& ray_tmin, const float& ray_tmax,
        const float3& cylidner_bottom_disk_center, const float& radius, const float& height,
        bool& t1_suitable, float& t1, float3& intersection1, float3& normal1,
        bool& t2_suitable, float& t2, float3& intersection2, float3& normal2,
        bool& t3_suitable, float& t3, float3& intersection3, float3& normal3,
        bool& t4_suitable, float& t4, float3& intersection4, float3& normal4
    )
    {
        t1_suitable = false;
        t2_suitable = false;
        t3_suitable = false;
        t4_suitable = false;

        {
            normal1 = vector3::up();
            normal2 = -vector3::up();
            float3 upper_disk = cylidner_bottom_disk_center + vector3::up() * height;
            t1_suitable = does_ray_intersect_axis_aligned_disk(ray_origin, ray_dir, normal1, upper_disk, radius, t1) && dot(ray_dir, normal1) <= 0.0f;
            if (t1 < 0.0f)
                t1_suitable = false;
            t2_suitable = does_ray_intersect_axis_aligned_disk(ray_origin, ray_dir, normal2, cylidner_bottom_disk_center, radius, t2) && dot(ray_dir, normal2) <= 0.0f;
            if (t2 < 0.0f)
                t2_suitable = false;
        }

        if (!t1_suitable && !t2_suitable)
        {
            float d = distance_between_lines(ray_origin, ray_dir, cylidner_bottom_disk_center, vector3::up());
            if (d <= radius)
            {            
                // cos of the angle between the ray direction and the horizontal plane.
                float3 __ray_dir = normalize(make_float3(ray_dir.x, 0.0f, ray_dir.z));
                float cos_with_plane = dot(ray_dir, __ray_dir);
                float dist_Ro_cylinder = distance_between_line_and_point(cylidner_bottom_disk_center, vector3::up(), ray_origin);
                float __d = distance_between_lines(ray_origin, __ray_dir, cylidner_bottom_disk_center, vector3::up());
                float __h = sqrtf(radius * radius - __d * __d);
                float __l = sqrtf(dist_Ro_cylinder * dist_Ro_cylinder - __d * __d);
                float __t3 = __l - __h;

                t3 = __t3 / cos_with_plane;

                if (t3 >= 0.0f)
                {
                    intersection3 = ray_origin + t3 * ray_dir;

                    if (intersection3.y > cylidner_bottom_disk_center.y && intersection3.y < cylidner_bottom_disk_center.y + height)
                    {
                        t3_suitable = true;
                        float3 __intersection3 = make_float3(intersection3.x, cylidner_bottom_disk_center.y, intersection3.z);
                        normal3 = normalize(__intersection3 - cylidner_bottom_disk_center);

                        if (length(__intersection3 - cylidner_bottom_disk_center) > radius + 1e-4)
                            t3_suitable = false;
                            //printf("%f\n", length(__intersection3 - cylidner_bottom_disk_center) - radius);
                    }
                }

                float __t4 = __l + __h;

                t4 = __t4 / cos_with_plane;

                if (t4 >= 0.0f)
                {
                    intersection4 = ray_origin + t4 * ray_dir;

                    if (intersection4.y > cylidner_bottom_disk_center.y && intersection4.y < cylidner_bottom_disk_center.y + height)
                    {
                        t4_suitable = true;
                        float3 __intersection4 = make_float3(intersection4.x, cylidner_bottom_disk_center.y, intersection4.z);
                        normal4 = normalize(__intersection4 - cylidner_bottom_disk_center);

                        if (length(__intersection4 - cylidner_bottom_disk_center) > radius + 1e-4)
                            t4_suitable = false;
                    }
                }
            }
        }

        return t1_suitable || t2_suitable || t3_suitable || t4_suitable;
    }

    __forceinline__ __device__ float max3f(float a, float b, float c)
    {
        return fmaxf(fmaxf(a, b), c);
    }

    __forceinline__ __device__ float min3f(float a, float b, float c)
    {
        return fminf(fminf(a, b), c);
    }

    __forceinline__ __device__ bool is_point_between(const float3& line_point_1, const float3& line_point_2, const float3& point_to_check, const float eps = 0.1f)
    {
        float3 dir_1 = normalize(point_to_check - line_point_1);
        float3 dir_2 = normalize(point_to_check - line_point_2);
        float dot_product = dot(dir_1, dir_2);

        return dot_product > (-1.0f - eps) && dot_product < (-1.0f + eps);
    }

    /// <summary>
    /// Calculate the u/v coordinate for a tile texture.
    /// It's explained in the report.
    /// </summary>
    __forceinline__ __device__ float UV_map(float value, float tiling = 1e16f)
    {
        value = fmodf(value, tiling);

        return map(value, 0.0f, tiling, 0.0f, 1.0f);
    }

    __forceinline__ __device__ void wall_UV_map(float& u, float& v, const Quadrangle& wall, const float3& intersection_point, const float t_h = 1e16f, const float t_v = 1e16f)
    {
        u = UV_map(distance_between_line_and_point(wall.a, normalize(wall.d - wall.a), intersection_point), t_h);
        v = UV_map(distance_between_line_and_point(wall.a, normalize(wall.b - wall.a), intersection_point), t_v);
    }

    __forceinline__ __device__ void from_spherical(const float phi, const float theta, float3& result)
    {
        // (x, y, z) = (sin(alpha)*cos(beta), sin(alpha)*sin(beta), cos(alpha))
        result.x = cosf(phi) * sinf(theta);
        result.y = cosf(theta);
        result.z = sinf(phi) * sinf(theta);
    }

    __forceinline__ __device__ void diffuse_sample_hemisphere(const float u1, const float u2, float3& result)
    {
        float theta = acosf(sqrtf(u1));
        float phi = 2.0f * M_PIf * u2;

        from_spherical(phi, theta, result);
    }

    __forceinline__ __device__ void specular_sample_hemisphere(const float u1, const float u2, const float n, float3& result)
    {
        float theta = acosf(pow(1.0f - u1, 1.0f / (1.0f + n)));
        float phi = 2.0f * M_PIf * u2;

        from_spherical(phi, theta, result);
    }       

    struct BoundingQuadrangles
    {
        Quadrangle foreground;
        Quadrangle background;

        __forceinline__ __device__ void initialize_bounding_quadrangles(Quadrangle mid, float max_extrusion, float max_intrusion)
        {
            float3 extrusion_dir = mid.get_normal();

            this->background = mid - (extrusion_dir * max_intrusion);
            Quadrangle background_backup = this->background;
            this->background.a = background_backup.b;
            this->background.b = background_backup.a;
            this->background.c = background_backup.d;
            this->background.d = background_backup.c;

            this->foreground = mid + (extrusion_dir * max_extrusion);
        }

        __forceinline__ __device__ Quadrangle get_left_quadrangle()
        {
            Quadrangle ret;
            ret.a = background.b;
            ret.b = foreground.a;
            ret.c = foreground.d;
            ret.d = background.c;
            return ret;
        }

        __forceinline__ __device__ Quadrangle get_right_quadrangle()
        {
            Quadrangle ret;
            ret.a = foreground.b;
            ret.b = background.a;
            ret.c = background.d;
            ret.d = foreground.c;
            return ret;
        }

        __forceinline__ __device__ Quadrangle get_top_quadrangle()
        {
            Quadrangle ret;
            ret.a = foreground.d;
            ret.b = foreground.c;
            ret.c = background.d;
            ret.d = background.c;
            return ret;
        }

        __forceinline__ __device__ Quadrangle get_bottom_quadrangle()
        {
            Quadrangle ret;
            ret.a = background.b;
            ret.b = background.a;
            ret.c = foreground.a;
            ret.d = foreground.b;
            return ret;
        }

        __forceinline__ __device__ bool does_ray_intersect_bounding_quadrangles(const float3& ray_origin, const float3& ray_dir)
        {
            Quadrangle array_of_bounding_quadrangles[6] =
            {
                foreground,
                get_left_quadrangle(),
                get_right_quadrangle(),
                get_top_quadrangle(),
                get_bottom_quadrangle(),
                background
            };


            /*for (auto& q : array_of_bounding_quadrangles)
            {
                float t;
                if (does_ray_intersect_wall_from_any_side(q, ray_origin, ray_dir, t) != 0)
                    return true;
            }*/

            if (is_point_inside_cube(
                foreground.a, foreground.b, background.a, background.b,
                foreground.d, foreground.c, background.d, background.c,
                ray_origin, 2e-0f
            ))
                return true;

            for (auto& q : array_of_bounding_quadrangles)
            {
                float t;
                if (does_ray_intersect_wall(q, ray_origin, ray_dir, t) != 0)
                    return true;
            }

            return false;
        }
    };

    // Copyright 2000 softSurfer, 2012 Dan Sunday
    // This code may be freely used and modified for any purpose
    // providing that this copyright notice is included with it.
    // SoftSurfer makes no warranty for this code, and cannot be held
    // liable for any real or imagined damage resulting from its use.
    // Users of this code must verify correctness for their application.
    //
    // http://geomalgorithms.com/a03-_inclusion.html
    //===================================================================

    // isLeft(): tests if a point is Left|On|Right of an infinite line.
    //    Input:  three points P0, P1, and P2
    //    Return: >0 for P2 left of the line through P0 and P1
    //            =0 for P2  on the line
    //            <0 for P2  right of the line
    //    See: Algorithm 1 "Area of Triangles and Polygons"
    __forceinline__ __device__ int isLeft(float2 P0, float2 P1, float2 P2)
    {
        return ((P1.x - P0.x) * (P2.y - P0.y)
            - (P2.x - P0.x) * (P1.y - P0.y));
    }
    //===================================================================

    // cn_PnPoly(): crossing number test for a point in a polygon
    //      Input:   P = a point,
    //               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
    //      Return:  0 = outside, 1 = inside
    // This code is patterned after [Franklin, 2000]
    __forceinline__ __device__ int cn_PnPoly(float2 P, float2* V, int n)
    {
        int    cn = 0;    // the  crossing number counter

        // loop through all edges of the polygon
        for (int i = 0; i < n; i++) {    // edge from V[i]  to V[i+1]
            if (((V[i].y <= P.y) && (V[i + 1].y > P.y))     // an upward crossing
                || ((V[i].y > P.y) && (V[i + 1].y <= P.y))) { // a downward crossing
                    // compute  the actual edge-ray intersect x-coordinate
                float vt = (float)(P.y - V[i].y) / (V[i + 1].y - V[i].y);
                if (P.x < V[i].x + vt * (V[i + 1].x - V[i].x)) // P.x < intersect
                    ++cn;   // a valid crossing of y=P.y right of P.x
            }
        }
        return (cn & 1);    // 0 if even (out), and 1 if  odd (in)

    }
    //===================================================================

    // wn_PnPoly(): winding number test for a point in a polygon
    //      Input:   P = a point,
    //               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
    //      Return:  wn = the winding number (=0 only when P is outside)
    __forceinline__ __device__ int wn_PnPoly(float2 P, float2* V, int n)
    {
        int    wn = 0;    // the  winding number counter

        // loop through all edges of the polygon
        for (int i = 0; i < n; i++) {   // edge from V[i] to  V[i+1]
            if (V[i].y <= P.y) {          // start y <= P.y
                if (V[i + 1].y > P.y)      // an upward crossing
                    if (isLeft(V[i], V[i + 1], P) > 0)  // P left of  edge
                        ++wn;            // have  a valid up intersect
            }
            else {                        // start y > P.y (no test needed)
                if (V[i + 1].y <= P.y)     // a downward crossing
                    if (isLeft(V[i], V[i + 1], P) < 0)  // P right of  edge
                        --wn;            // have  a valid down intersect
            }
        }
        return wn;
    }
    //===================================================================
}