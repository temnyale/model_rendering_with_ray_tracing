/**
 * Functions that create the geometry to store in the memory.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include <vector>
#include <iostream>

#include "common_geometry.h"
#include "math_utils.h"
#include "rendering.h"

namespace drpm
{
	static void get_default_UVs(float2 UV1[3], float2 UV2[3])
	{
		UV1[0] = { 0.f, 0.f };
		UV1[1] = { 1.f, 0.f };
		UV1[2] = { 1.f, 1.f };

		UV2[0] = { 1.f, 1.f };
		UV2[1] = { 0.f, 1.f };
		UV2[2] = { 0.f, 0.f };
	}

	void push_triangles_to_vector(
		const Quadrangle& q, 
		TriangleData triangle1_data,
		TriangleData triangle2_data,
		std::vector<std::pair<Triangle, TriangleData>>& vec,
		bool set_default_UVs = false
	)
	{
		if (set_default_UVs)
		{
			float2 UV1[3];
			float2 UV2[3];

			get_default_UVs(UV1, UV2);

			triangle1_data.UVs[0] = UV1[0];
			triangle1_data.UVs[1] = UV1[1];
			triangle1_data.UVs[2] = UV1[2];

			triangle2_data.UVs[0] = UV2[0];
			triangle2_data.UVs[1] = UV2[1];
			triangle2_data.UVs[2] = UV2[2];
		}

		Triangle t[2];
		q.to_triangles(t);
		vec.push_back({ t[0], triangle1_data });
		vec.push_back({ t[1], triangle2_data });
	}

	void push_triangles_to_vector(
		const Quadrangle* q, 
		const TriangleData* triangle_data,
		const int N, 
		std::vector<std::pair<Triangle, TriangleData>>& vec,
		bool set_default_UVs = false
	)
	{
		for (int i = 0; i < N; i++)
		{
			push_triangles_to_vector(q[i], triangle_data[i * 2 + 0], triangle_data[i * 2 + 1], vec, set_default_UVs);
		}
	}

	void push_triangles_to_vector(
		const Quadrangle* q,
		TriangleData triangle1_data,
		TriangleData triangle2_data,
		const int N,
		std::vector<std::pair<Triangle, TriangleData>>& vec,
		bool set_default_UVs = false
	)
	{
		for (int i = 0; i < N; i++)
		{
			push_triangles_to_vector(q[i], triangle1_data, triangle1_data, vec, set_default_UVs);
		}
	}

	void push_triangles_to_vector_with_UVs_according_to_wall(
		const Quadrangle* q,
		SceneObjectPart scene_object_part,
		int scene_object_part_id,
		const int N,
		const Quadrangle& wall,
		std::vector<std::pair<Triangle, TriangleData>>& vec
	)
	{
		TriangleData* triangle_data = new TriangleData[N * 2];
		for (int i = 0; i < N; i++)
		{
			const Quadrangle& cur_q = q[i];
			Triangle triangles[2];
			cur_q.to_triangles(triangles);
			for (int j = 0; j < 2; j++)
			{
				auto& cur_triangle_data = triangle_data[i * 2 + j];
				auto& cur_triangle = triangles[j];
				float a_u = distance_between_line_and_point(wall.a, wall.d - wall.a, cur_triangle.a);
				float a_v = distance_between_line_and_point(wall.a, wall.b - wall.a, cur_triangle.a);
				float b_u = distance_between_line_and_point(wall.a, wall.d - wall.a, cur_triangle.b);
				float b_v = distance_between_line_and_point(wall.a, wall.b - wall.a, cur_triangle.b);
				float c_u = distance_between_line_and_point(wall.a, wall.d - wall.a, cur_triangle.c);
				float c_v = distance_between_line_and_point(wall.a, wall.b - wall.a, cur_triangle.c);
				cur_triangle_data.UVs[0] = { a_u, a_v };
				cur_triangle_data.UVs[1] = { b_u, b_v };
				cur_triangle_data.UVs[2] = { c_u, c_v };
				cur_triangle_data.scene_object_part = scene_object_part;
				cur_triangle_data.scene_object_part_id = scene_object_part_id;
			}
		}

		push_triangles_to_vector(q, triangle_data, N, vec, false);
	}

	void get_window_triangles(
		const Quadrangle& window_quadrangle,
		const WindowParameters& window_parameters,
		const int window_id,
		std::vector<std::pair<Triangle, TriangleData>>& triangles
	)
	{
		float2 UV1[3];
		float2 UV2[3];

		get_default_UVs(UV1, UV2);

		{
			Quadrangle left, top, right, bottom;

			left = (window_quadrangle.get_left(window_parameters.outside_intrusion)).get_reversed();
			top = (window_quadrangle.get_top(window_parameters.outside_intrusion)).get_reversed();
			right = (window_quadrangle.get_right(window_parameters.outside_intrusion)).get_reversed();
			bottom = (window_quadrangle.get_bottom(window_parameters.outside_intrusion)).get_reversed();

			push_triangles_to_vector(
				left, 
				{ SceneObjectPart::BUILDING_WALL, window_id }, 
				{ SceneObjectPart::BUILDING_WALL, window_id },
				triangles,
				true
			);
			push_triangles_to_vector(
				top,
				{ SceneObjectPart::BUILDING_WALL, window_id, },
				{ SceneObjectPart::BUILDING_WALL, window_id, },
				triangles,
				true
			);
			push_triangles_to_vector(
				right,
				{ SceneObjectPart::BUILDING_WALL, window_id, },
				{ SceneObjectPart::BUILDING_WALL, window_id, },
				triangles,
				true
			);
			push_triangles_to_vector(
				bottom,
				{ SceneObjectPart::BUILDING_WALL, window_id, },
				{ SceneObjectPart::BUILDING_WALL, window_id, },
				triangles,
				true
			);

			Quadrangle _window_quadrangle = window_quadrangle - window_quadrangle.get_normal() * window_parameters.outside_intrusion;
			left = (_window_quadrangle.get_left(window_parameters.inside_intrusion)).get_reversed();
			top = (_window_quadrangle.get_top(window_parameters.inside_intrusion)).get_reversed();
			right = (_window_quadrangle.get_right(window_parameters.inside_intrusion)).get_reversed();
			bottom = (_window_quadrangle.get_bottom(window_parameters.inside_intrusion)).get_reversed();

			push_triangles_to_vector(
				left,
				{ SceneObjectPart::ROOM_WALL, window_id },
				{ SceneObjectPart::ROOM_WALL, window_id },
				triangles,
				true
			);
			push_triangles_to_vector(
				top,
				{ SceneObjectPart::ROOM_WALL, window_id, },
				{ SceneObjectPart::ROOM_WALL, window_id, },
				triangles,
				true
			);
			push_triangles_to_vector(
				right,
				{ SceneObjectPart::ROOM_WALL, window_id, },
				{ SceneObjectPart::ROOM_WALL, window_id, },
				triangles,
				true
			);
			push_triangles_to_vector(
				bottom,
				{ SceneObjectPart::ROOM_WALL, window_id, },
				{ SceneObjectPart::ROOM_WALL, window_id, },
				triangles,
				true
			);
		}

		Quadrangle window_glass_quadrangle = window_quadrangle - window_quadrangle.get_normal() * window_parameters.outside_intrusion;
		Quadrangle room_background_quadrangle = window_glass_quadrangle - window_glass_quadrangle.get_normal() * window_parameters.inside_intrusion;

		{
			if (!window_parameters.imposts)
			{
				push_triangles_to_vector(
					window_glass_quadrangle,
					{ SceneObjectPart::WINDOW_GLASS, window_id, },
					{ SceneObjectPart::WINDOW_GLASS, window_id, },
					triangles,
					true
				);
			}
			else
			{
				Quadrangle qs[16];
				//printf("impost width = %f\n", window_parameters.impost_width);
				get_quadrangles_for_window_glass_and_imposts(window_glass_quadrangle, window_parameters.impost_position, window_parameters.impost_width, window_parameters.impost_thickness, qs);

				push_triangles_to_vector_with_UVs_according_to_wall(qs, SceneObjectPart::BALCONY_GLASS, window_id, 4, window_glass_quadrangle, triangles);
				push_triangles_to_vector_with_UVs_according_to_wall(qs + 4, SceneObjectPart::WINDOW_IMPOST, window_id, 12, window_glass_quadrangle, triangles);

				/*push_triangles_to_vector(
					qs,
					{ SceneObjectPart::WINDOW_GLASS, window_id, },
					{ SceneObjectPart::WINDOW_GLASS, window_id, },
					triangles,
					true
				);

				push_triangles_to_vector(
					qs[1],
					{ SceneObjectPart::WINDOW_IMPOST, window_id, },
					{ SceneObjectPart::WINDOW_IMPOST, window_id, },
					triangles,
					true
				);*/
			}
		}

		push_triangles_to_vector(
			room_background_quadrangle,
			{ SceneObjectPart::ROOM_BACKGROUND_WALL, window_id, },
			{ SceneObjectPart::ROOM_BACKGROUND_WALL, window_id, },
			triangles,
			true
		);
	}

	void get_balcony_triangles(
		Quadrangle balcony_background_wall, 
		const RegularBuildingFacadeParameters& facade_parameters,
		const int balcony_id,
		std::vector<std::pair<Triangle, TriangleData>>& triangles
	)
	{
		float intrusion = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_intrusion;

		Quadrangle balcony_original_quadrangle_backup = balcony_background_wall;

		{
			Quadrangle left, top, right, bottom;

			left = (balcony_background_wall.get_left(intrusion)).get_reversed();
			top = (balcony_background_wall.get_top(intrusion)).get_reversed();
			right = (balcony_background_wall.get_right(intrusion)).get_reversed();
			bottom = (balcony_background_wall.get_bottom(intrusion)).get_reversed();

			push_triangles_to_vector(left, { SceneObjectPart::BALCONY_WALL, balcony_id }, { SceneObjectPart::BALCONY_WALL, balcony_id }, triangles, true);
			push_triangles_to_vector(top, { SceneObjectPart::BALCONY_WALL, balcony_id }, { SceneObjectPart::BALCONY_WALL, balcony_id }, triangles, true);
			push_triangles_to_vector(right, { SceneObjectPart::BALCONY_WALL, balcony_id }, { SceneObjectPart::BALCONY_WALL, balcony_id }, triangles, true);
			push_triangles_to_vector(bottom, { SceneObjectPart::BALCONY_WALL, balcony_id }, { SceneObjectPart::BALCONY_WALL, balcony_id }, triangles, true);
		}

		float3 balcony_quadrangle_n = balcony_background_wall.get_normal();

		balcony_background_wall = balcony_background_wall - balcony_quadrangle_n * intrusion;

		// Fence.
		{
			Quadrangle foreground, top, background;

			foreground.a = balcony_original_quadrangle_backup.a;
			foreground.b = balcony_original_quadrangle_backup.b;
			foreground.c
				= foreground.b + balcony_original_quadrangle_backup.get_vertical_direction() * facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_wall_height;
			foreground.d
				= foreground.a + balcony_original_quadrangle_backup.get_vertical_direction() * facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_wall_height;

			float fence_intrusion = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_wall_thickness;

			top = foreground.get_top(fence_intrusion);
			background = foreground.get_opposite(fence_intrusion);

			push_triangles_to_vector(foreground, { SceneObjectPart::BALCONY_FENCE, balcony_id }, { SceneObjectPart::BALCONY_FENCE, balcony_id }, triangles, true);
			push_triangles_to_vector(top, { SceneObjectPart::BALCONY_FENCE, balcony_id }, { SceneObjectPart::BALCONY_FENCE, balcony_id }, triangles, true);
			push_triangles_to_vector(background, { SceneObjectPart::BALCONY_FENCE, balcony_id }, { SceneObjectPart::BALCONY_FENCE, balcony_id }, triangles, true);
		}

		{
			// Glass.
			{
				Quadrangle glass_quadrangle;

				float fence_height = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_wall_height;
				float balcony_height = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_sizes.y;
				float glass_height = balcony_height - fence_height;

				glass_quadrangle.d = balcony_original_quadrangle_backup.d;
				glass_quadrangle.c = balcony_original_quadrangle_backup.c;
				glass_quadrangle.a = glass_quadrangle.d - balcony_original_quadrangle_backup.get_vertical_direction() * glass_height;
				glass_quadrangle.b = glass_quadrangle.c - balcony_original_quadrangle_backup.get_vertical_direction() * glass_height;
				push_triangles_to_vector(glass_quadrangle, { SceneObjectPart::BALCONY_GLASS, balcony_id }, { SceneObjectPart::BALCONY_GLASS, balcony_id }, triangles, true);
			}

			push_triangles_to_vector(balcony_background_wall, { SceneObjectPart::BALCONY_WALL, balcony_id }, { SceneObjectPart::BALCONY_WALL, balcony_id }, triangles, true);
		}
	}

	void get_triangles_of_floor_with_window_2_balconies_window(
		const Quadrangle& wall, const int floor, const RegularBuildingFacadeParameters& facade_parameters,
		std::vector<std::pair<Triangle, TriangleData>>& triangles
	)
	{
		const int common_id_factor = 4321;

		float3 vertical_dir = wall.get_vertical_direction();
		float3 horizontal_dir = wall.get_horizontal_direction();

		float first_floor_above_shop_height
			= facade_parameters.bottom_gap + facade_parameters.shop_floor_height * facade_parameters.shop_at_first_floor;

		float y = wall.a.y + first_floor_above_shop_height + facade_parameters.floor_height * floor;		

		if (facade_parameters.number_of_slots_horizontally == 0)
		{
			Quadrangle q = get_wall_part_for_floor(
				vertical_dir,
				make_float3(wall.a.x, y, wall.a.z),
				make_float3(wall.b.x, y, wall.b.z),
				facade_parameters.floor_height
			);
			//push_triangles_to_vector(q, { SceneObjectPart::BUILDING_WALL, -1 }, { SceneObjectPart::BUILDING_WALL, -1 }, triangles, true);
			push_triangles_to_vector_with_UVs_according_to_wall(&q, SceneObjectPart::BUILDING_WALL, -1, 1, wall, triangles);
		}
		else
		{
			{
				Quadrangle edge_quadrangles[2];
				get_floor_edge_quadrangles(wall, y, facade_parameters.floor_height, facade_parameters.facade_slots.window_2_balconies_window_parameters.edge_gap, edge_quadrangles);
				push_triangles_to_vector_with_UVs_according_to_wall(edge_quadrangles, SceneObjectPart::BUILDING_WALL, -1, 2, wall, triangles);
			}

			for (int j = 0; j < facade_parameters.number_of_slots_horizontally; j++)
			{
				float dist_between_floor_bottom_and_window =
					(facade_parameters.floor_height
						- facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.y)
					/ 2.0f;

				float dist_between_floor_bottom_and_balcony =
					(facade_parameters.floor_height
						- facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_sizes.y)
					/ 2.0f;

				float3 current_slot = get_facade_slot_start_point(
					horizontal_dir,
					make_float3(wall.a.x, y, wall.a.z),
					facade_parameters.facade_slots.window_2_balconies_window_parameters.get_slot(),
					facade_parameters.facade_slots.window_2_balconies_window_parameters.edge_gap,
					j
				);

				Quadrangle slot_quadrangle = get_slot_quadrangle(
					horizontal_dir,
					vertical_dir,
					current_slot,
					facade_parameters.facade_slots.window_2_balconies_window_parameters.get_slot(),
					facade_parameters.floor_height
				);
				//push_triangles_to_vector(slot_quadrangle, SceneObjectPart::BUILDING_WALL, -1, triangles);
				float3 current_slot_for_windows = current_slot + vertical_dir * dist_between_floor_bottom_and_window;
				float3 current_slot_for_balcony = current_slot + vertical_dir * dist_between_floor_bottom_and_balcony;

				// Windows. -----------------------------------------------------------------------------------------------------------------
				Quadrangle window_quadrangles[2];
				{
					{
						Quadrangle q = get_quadrangle_in_slot(
							horizontal_dir,
							vertical_dir,
							current_slot_for_windows,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.x,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.y,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_windows / 2.f
						);

						window_quadrangles[0].a = q.a;
						window_quadrangles[0].b = q.b;
						window_quadrangles[0].c = q.c;
						window_quadrangles[0].d = q.d;
					}
					{
						float3 _current_slot_for_windows = current_slot_for_windows + horizontal_dir * facade_parameters.facade_slots.window_2_balconies_window_parameters.get_slot();

						Quadrangle q = get_quadrangle_in_slot(
							-horizontal_dir,
							vertical_dir,
							_current_slot_for_windows,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.x,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.y,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_windows / 2.f
						);
						q = q.get_reversed();

						window_quadrangles[1].a = q.a;
						window_quadrangles[1].b = q.b;
						window_quadrangles[1].c = q.c;
						window_quadrangles[1].d = q.d;
					}

					int window_in_slot = common_id_factor;
					for (auto& window_quadrangle : window_quadrangles)
					{
						int window_id = (floor * facade_parameters.number_of_slots_horizontally + j) * 1234 * floor * window_in_slot;
						get_window_triangles(
							window_quadrangle,
							facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters,
							window_id,
							triangles
						);

						window_in_slot *= window_in_slot;
					}
				}

				// Balconies. ------------------------------------------------------------------------------------------------------------------------
				Quadrangle balconies_quadrangles[2];
				{
					{
						const float& gap_between_windows = facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_windows;
						const float& window_u = facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.x;
						const float& gap_between_window_and_balcony = facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_window_and_balcony;
						const float& balcony_u = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_sizes.x;
						const float& balcony_v = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_sizes.y;

						Quadrangle q = get_quadrangle_in_slot(
							horizontal_dir,
							vertical_dir,
							current_slot_for_balcony,
							balcony_u,
							balcony_v,
							(facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_windows / 2.f) + window_u + gap_between_window_and_balcony
						);

						balconies_quadrangles[0].a = q.a;
						balconies_quadrangles[0].b = q.b;
						balconies_quadrangles[0].c = q.c;
						balconies_quadrangles[0].d = q.d;
					}
					{
						const float& slot_size = facade_parameters.facade_slots.window_2_balconies_window_parameters.get_slot();
						const float& gap_between_windows = facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_windows;
						const float& window_u = facade_parameters.facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.x;
						const float& gap_between_window_and_balcony = facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_window_and_balcony;
						const float& balcony_u = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_sizes.x;
						const float& balcony_v = facade_parameters.facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_sizes.y;

						Quadrangle q = get_quadrangle_in_slot(
							-horizontal_dir,
							vertical_dir,
							current_slot_for_balcony + horizontal_dir * slot_size,
							balcony_u,
							balcony_v,
							(facade_parameters.facade_slots.window_2_balconies_window_parameters.gap_between_windows / 2.f) + window_u + gap_between_window_and_balcony
						);
						q = q.get_reversed();

						balconies_quadrangles[1].a = q.a;
						balconies_quadrangles[1].b = q.b;
						balconies_quadrangles[1].c = q.c;
						balconies_quadrangles[1].d = q.d;
					}

					int balcony_in_slot = common_id_factor;
					for (auto& balcony_quadrangle : balconies_quadrangles)
					{
						int balcony_id = (floor * facade_parameters.number_of_slots_horizontally + j) * 1234 * floor * balcony_in_slot;
						get_balcony_triangles(
							balcony_quadrangle,
							facade_parameters,
							balcony_id,
							triangles
						);

						balcony_in_slot *= balcony_in_slot;
					}
				}

				{
					Quadrangle rest_quadrangles[11];
					get_window_2_balconies_window_slot_rest_quadrangles(slot_quadrangle, window_quadrangles, balconies_quadrangles, rest_quadrangles);
					//push_triangles_to_vector(rest_quadrangles, { SceneObjectPart::BUILDING_WALL, -1 }, { SceneObjectPart::BUILDING_WALL, -1 }, 11, triangles, true);
					push_triangles_to_vector_with_UVs_according_to_wall(rest_quadrangles, SceneObjectPart::BUILDING_WALL, -1, 11, wall, triangles);
				}
			}
		}
	}

	void get_triangles_of_floor_with_simple_windows(
		const Quadrangle& wall, const int floor, const RegularBuildingFacadeParameters& facade_parameters,
		std::vector<std::pair<Triangle, TriangleData>>& triangles
	)
	{
		//if (facade_parameters.facade_slots.simple_window_parameters.window_parameters.imposts)
		//	printf("ZDE 2 %f\n", facade_parameters.facade_slots.simple_window_parameters.window_parameters.impost_width);

		float3 vertical_dir = wall.get_vertical_direction();
		float3 horizontal_dir = wall.get_horizontal_direction();

		float first_floor_above_shop_height
			= facade_parameters.bottom_gap + facade_parameters.shop_floor_height * facade_parameters.shop_at_first_floor;

		float y = wall.a.y + first_floor_above_shop_height + facade_parameters.floor_height * floor;		

		if (facade_parameters.number_of_slots_horizontally == 0)
		{
			Quadrangle q = get_wall_part_for_floor(
				vertical_dir, 
				make_float3(wall.a.x, y, wall.a.z),
				make_float3(wall.b.x, y, wall.b.z),
				facade_parameters.floor_height
			);
			//push_triangles_to_vector(q, { SceneObjectPart::BUILDING_WALL, -1 }, { SceneObjectPart::BUILDING_WALL, -1 }, triangles, true);
			push_triangles_to_vector_with_UVs_according_to_wall(&q, SceneObjectPart::BUILDING_WALL, -1, 1, wall, triangles);
		}
		else
		{
			{
				Quadrangle edge_quadrangles[2];
				get_floor_edge_quadrangles(wall, y, facade_parameters.floor_height, facade_parameters.facade_slots.simple_window_parameters.edge_gap, edge_quadrangles);
				push_triangles_to_vector_with_UVs_according_to_wall(edge_quadrangles, SceneObjectPart::BUILDING_WALL, -1, 2, wall, triangles);
			}

			for (int j = 0; j < facade_parameters.number_of_slots_horizontally; j++)
			{
				float dist_between_floor_bottom_and_window =
					(facade_parameters.floor_height - facade_parameters.facade_slots.simple_window_parameters.window_parameters.sizes.y) / 2.0f;

				float3 window_slot = get_facade_slot_start_point(
					horizontal_dir,
					make_float3(wall.a.x, y + dist_between_floor_bottom_and_window, wall.a.z),
					facade_parameters.facade_slots.simple_window_parameters.get_slot(),
					facade_parameters.facade_slots.simple_window_parameters.edge_gap,
					j
				);

				float3 floor_slot = window_slot - vector3::up() * dist_between_floor_bottom_and_window;

				Quadrangle window_quadrangle = get_quadrangle_in_slot(
					horizontal_dir,
					vertical_dir,
					window_slot,
					facade_parameters.facade_slots.simple_window_parameters.window_parameters.sizes.x,
					facade_parameters.facade_slots.simple_window_parameters.window_parameters.sizes.y,
					facade_parameters.facade_slots.simple_window_parameters.gap_between_windows / 2.f
				);

				{
					Quadrangle q = get_slot_quadrangle(
						horizontal_dir,
						vertical_dir,
						floor_slot,
						facade_parameters.facade_slots.simple_window_parameters.get_slot(),
						facade_parameters.floor_height
					);

					Quadrangle rest_quadrangles[4];
					get_simple_window_slot_rest_quadrangles(q, window_quadrangle, rest_quadrangles);
					//push_triangles_to_vector(rest_quadrangles, { SceneObjectPart::BUILDING_WALL, -1 }, { SceneObjectPart::BUILDING_WALL, -1 }, 4, triangles, true);
					push_triangles_to_vector_with_UVs_according_to_wall(rest_quadrangles, SceneObjectPart::BUILDING_WALL, -1, 4, wall, triangles);
				}

				{

					int window_id = (floor * facade_parameters.number_of_slots_horizontally + j) * 1234 * floor;
					get_window_triangles(
						window_quadrangle,
						facade_parameters.facade_slots.simple_window_parameters.window_parameters,
						window_id,
						triangles
					);
				}
			}
		}
	}

	void get_facade_triangles(
		const Quadrangle& wall, const RegularBuildingFacadeParameters& facade_parameters,
		std::vector<std::pair<Triangle, TriangleData>>& triangles
	)
	{
		float wall_width = length(wall.a - wall.b);
		float wall_height = length(wall.d - wall.a);
		float3 vertical_dir = wall.get_vertical_direction();
		float3 horizontal_dir = wall.get_horizontal_direction();

		float3 intersection_point;

		{
			Quadrangle top_and_bottom_quadrangles[2];
			int n = get_wall_top_and_bottom_quadrangles(wall, facade_parameters.bottom_gap, facade_parameters.top_gap, top_and_bottom_quadrangles);
			//push_triangles_to_vector(top_and_bottom_quadrangles, { SceneObjectPart::BUILDING_WALL, -1 }, { SceneObjectPart::BUILDING_WALL, -1 }, n, triangles, true);
			push_triangles_to_vector_with_UVs_according_to_wall(top_and_bottom_quadrangles, SceneObjectPart::BUILDING_WALL, -1, 2, wall, triangles);
		}

		if (facade_parameters.shop_at_first_floor)
		{		
			if (facade_parameters.number_of_shop_windows_horizontally == 0)
			{
				Quadrangle q = get_wall_part_for_floor(vertical_dir, wall.a, wall.b, facade_parameters.shop_floor_height);
				//push_triangles_to_vector(q, { SceneObjectPart::BUILDING_WALL, -1 }, { SceneObjectPart::BUILDING_WALL, -1 }, triangles, true);
				push_triangles_to_vector_with_UVs_according_to_wall(&q, SceneObjectPart::BUILDING_WALL, -1, 1, wall, triangles);
			}
			else
			{
				{
					Quadrangle edge_quadrangles[2];
					get_floor_edge_quadrangles(wall, wall.a.y, facade_parameters.shop_floor_height, facade_parameters.shop_window_parameters.edge_gap, edge_quadrangles);
					//push_triangles_to_vector(edge_quadrangles, { SceneObjectPart::BUILDING_WALL, -1 }, { SceneObjectPart::BUILDING_WALL, -1 }, 2, triangles, true);
					push_triangles_to_vector_with_UVs_according_to_wall(edge_quadrangles, SceneObjectPart::BUILDING_WALL, -1, 2, wall, triangles);
				}

				for (int i = 0; i < facade_parameters.number_of_shop_windows_horizontally; i++)
				{
					float3 shop_window_slot = get_facade_slot_start_point(
						horizontal_dir,
						make_float3(wall.a.x, wall.a.y, wall.a.z),
						facade_parameters.shop_window_parameters.get_slot(),
						facade_parameters.shop_window_parameters.edge_gap,
						i
					);

					Quadrangle shop_window_quad = get_quadrangle_in_slot(
						horizontal_dir,
						vertical_dir,
						shop_window_slot,
						facade_parameters.shop_window_parameters.window_parameters.sizes.x,
						facade_parameters.shop_window_parameters.window_parameters.sizes.y,
						facade_parameters.shop_window_parameters.gap_between_windows / 2.f
					);

					get_window_triangles(
						shop_window_quad,
						facade_parameters.shop_window_parameters.window_parameters,
						-1,
						triangles
					);

					Quadrangle q = get_slot_quadrangle(
						horizontal_dir,
						vertical_dir,
						shop_window_slot,
						facade_parameters.shop_window_parameters.get_slot(),
						facade_parameters.shop_floor_height
					);

					Quadrangle rest_quadrangles[4];
					get_simple_window_slot_rest_quadrangles(q, shop_window_quad, rest_quadrangles);
					//push_triangles_to_vector(rest_quadrangles, { SceneObjectPart::BUILDING_WALL, -1 }, { SceneObjectPart::BUILDING_WALL, -1 }, 4, triangles, true);
					push_triangles_to_vector_with_UVs_according_to_wall(rest_quadrangles, SceneObjectPart::BUILDING_WALL, -1, 4, wall, triangles);
				}
			}
		}

		// Check if the point is inside a window.
		{
			int start_i = 0, end_i = facade_parameters.number_of_floors;
			
			for (int i = start_i; i < end_i; i++)
			{
				switch (facade_parameters.facade_slot_type)
				{
					case FacadeSlotType::SIMPLE_WINDOWS:
					{
						//printf("edge gap %d\n", facade_parameters.facade_slots.simple_window_parameters.edge_gap);
						get_triangles_of_floor_with_simple_windows(wall, i, facade_parameters, triangles);
						//if (facade_parameters.facade_slots.simple_window_parameters.window_parameters.imposts)
						//	printf("ZDE %f\n", facade_parameters.facade_slots.simple_window_parameters.window_parameters.impost_width);
					} break;

					case FacadeSlotType::WINDOW_BALCONY_BALCONY_WINDOW:
					{
						get_triangles_of_floor_with_window_2_balconies_window(wall, i, facade_parameters, triangles);
					} break;
				}
			}
		}

		//printf("Number of windows=%d\n", counter);
	}
}