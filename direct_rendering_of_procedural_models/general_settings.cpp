/**
 * General settings for the application, such as the seed, the window size, etc..
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#include "general_settings.h"

namespace drpm
{
	unsigned int global_seed = 0;
	bool print_memory_info = false;
	bool print_scene_info = false;
	bool print_scene_geometry_info = false;
	int application_window_width = 1024;
	int application_window_height = 768;	

#ifndef __CUDACC__
	sutil::CUDAOutputBufferType cuda_output_buffer_type = sutil::CUDAOutputBufferType::GL_INTEROP;
#endif
}