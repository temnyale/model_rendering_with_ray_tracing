/**
 * Data structures for facades.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#pragma once

#include "geometry_settings.h"
#include "FacadeSlotParameters.h"

namespace drpm
{
	class RegularBuildingFacadeParameters
	{
	private:
		const float eps = 0.1f;

		__forceinline__ __device__ void calculate_window_2_balconies_window_vertical_sizes(
			const float wall_height, float& space_for_slots,
			float& floor_height, int& number_of_floors,
			float& remaining_size
		)
		{
			space_for_slots = wall_height - (top_gap + bottom_gap);
			number_of_floors = static_cast<int>(floorf((space_for_slots / floor_height) + eps));
			remaining_size = space_for_slots - number_of_floors * floor_height;
		}

		__forceinline__ __device__ void calculate_window_2_balconies_window_horizontal_sizes(
			const float wall_width_arg, Window2BalconiesWindowSlotParameters& parameters_arg,
			float& space_for_slots_arg, float& slot_size_arg,
			int& number_of_slots_arg, float& remaining_size_arg
		)
		{
			space_for_slots_arg = wall_width_arg - 2.0f * parameters_arg.edge_gap;
			slot_size_arg = parameters_arg.get_slot();
			number_of_slots_arg = (int)floorf((space_for_slots_arg / slot_size_arg) + eps);
			remaining_size_arg = space_for_slots_arg - number_of_slots_arg * slot_size_arg;
		}

		__forceinline__ __device__ void try_min_window_2_balconies_window_widths(
			const float& wall_width, float& space_for_slots,
			float& slot_size, int& number_of_slots,
			float& remaining_size
		)
		{
			facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.x = regular_building_window_u_min;
			facade_slots.window_2_balconies_window_parameters.gap_between_windows = regular_building_gap_around_window_min;
			facade_slots.window_2_balconies_window_parameters.edge_gap = 0;

			calculate_window_2_balconies_window_horizontal_sizes(
				wall_width,
				facade_slots.window_2_balconies_window_parameters,
				space_for_slots,
				slot_size,
				number_of_slots,
				remaining_size
			);
		}

		__forceinline__ __device__ void try_one_window_2_balconies_window_horizontally_with_min_widths(
			const float& wall_width, float& space_for_slots,
			float& slot_size, int& number_of_slots,
			float& remaining_size
		)
		{
			facade_slots.window_2_balconies_window_parameters.window_parameters.sizes.x = regular_building_window_u_min;
			facade_slots.window_2_balconies_window_parameters.gap_between_windows = 0;
			if ((wall_width - facade_slots.window_2_balconies_window_parameters.get_slot()) / 2.0f >= regular_building_edge_gap_min)
			{
				facade_slots.window_2_balconies_window_parameters.edge_gap = (wall_width - facade_slots.window_2_balconies_window_parameters.get_slot()) / 2.0f;

				calculate_window_2_balconies_window_horizontal_sizes(
					wall_width,
					facade_slots.window_2_balconies_window_parameters,
					space_for_slots,
					slot_size,
					number_of_slots,
					remaining_size
				);

				number_of_slots = facade_slots.window_2_balconies_window_parameters.get_slot() <= space_for_slots ? 1 : 0;
				remaining_size = 0;

				float excent_edge_gap = facade_slots.window_2_balconies_window_parameters.edge_gap - regular_building_edge_gap_max;

				if (excent_edge_gap > 0.0f)
				{
					/*facade_slots.window_2_balconies_window_parameters.gap_between_balconies += excent_edge_gap / 3.0f;
					facade_slots.window_2_balconies_window_parameters.gap_between_window_and_balcony += (excent_edge_gap / 3.0f) / 2.0f;
					facade_slots.window_2_balconies_window_parameters.edge_gap -= excent_edge_gap;*/

					float excent_size = excent_edge_gap * 2;

					facade_slots.window_2_balconies_window_parameters.gap_between_balconies += excent_size / 2.f;
					facade_slots.window_2_balconies_window_parameters.gap_between_window_and_balcony += excent_size / 4.f;
					facade_slots.window_2_balconies_window_parameters.edge_gap -= excent_edge_gap;
				}
			}
			else number_of_slots = 0;
		}

		__forceinline__ __device__ void calculate_simple_window_vertical_sizes(
			const float wall_height, float& space_for_windows,
			float& floor_height, int& number_of_floors,
			float& remaining_size
		)
		{
			space_for_windows = wall_height - (top_gap + bottom_gap);
			number_of_floors = static_cast<int>(floorf((space_for_windows / floor_height) + eps));
			remaining_size = space_for_windows - number_of_floors * floor_height;
		}

		__forceinline__ __device__ void calculate_simple_window_horizontal_sizes(
			const float wall_width_arg, SimpleWindowSlotParameters& window_parameters_arg,
			float& space_for_windows_arg, float& window_slot_size_arg,
			int& number_of_windows_arg, float& remaining_size_arg
		)
		{
			space_for_windows_arg = wall_width_arg - 2.0f * window_parameters_arg.edge_gap;
			window_slot_size_arg = window_parameters_arg.get_slot();
			number_of_windows_arg = (int)floorf((space_for_windows_arg / window_slot_size_arg) + eps);
			remaining_size_arg = space_for_windows_arg - number_of_windows_arg * window_slot_size_arg;
		}

		__forceinline__ __device__ void try_min_simple_window_widths(
			const float& wall_width, float& space_for_windows,
			float& window_slot_size, int& number_of_windows,
			float& remaining_size
		)
		{
			facade_slots.simple_window_parameters.window_parameters.sizes.x = regular_building_window_u_min;
			facade_slots.simple_window_parameters.gap_between_windows = regular_building_gap_around_window_min;
			facade_slots.simple_window_parameters.edge_gap = 0;

			calculate_simple_window_horizontal_sizes(
				wall_width,
				facade_slots.simple_window_parameters,
				space_for_windows,
				window_slot_size,
				number_of_windows,
				remaining_size
			);
		}

		__forceinline__ __device__ void try_one_simple_window_horizontally(
			const float& wall_width, float& space_for_windows,
			float& window_slot_size, int& number_of_windows,
			float& remaining_size
		)
		{
			facade_slots.simple_window_parameters.window_parameters.sizes.x = regular_building_window_u_min;
			facade_slots.simple_window_parameters.gap_between_windows = 0;

			if ((wall_width - facade_slots.simple_window_parameters.window_parameters.sizes.x) / 2.0f >= regular_building_edge_gap_min)
			{
				facade_slots.simple_window_parameters.edge_gap = (wall_width - facade_slots.simple_window_parameters.window_parameters.sizes.x) / 2.0f;

				calculate_simple_window_horizontal_sizes(
					wall_width,
					facade_slots.simple_window_parameters,
					space_for_windows,
					window_slot_size,
					number_of_windows,
					remaining_size
				);

				number_of_windows = facade_slots.simple_window_parameters.get_slot() <= space_for_windows ? 1 : 0;
				remaining_size = 0;
			}
			else number_of_windows = 0;
		}

		__forceinline__ __device__ bool try_shop_windows(const float wall_width, const float wall_height, unsigned int seed)
		{
			shop_at_first_floor = rnd(seed) < shop_probability;

			if (shop_at_first_floor)
			{
				int idx = lcg(seed);
				SimpleWindowSlotParameters selected_shop_window_initial_parameters = shop_window_initial_parameters[idx % number_of_shop_window_initial_parameters];
				shop_window_parameters.window_parameters.sizes = selected_shop_window_initial_parameters.window_parameters.sizes;
				shop_window_parameters.window_parameters.outside_intrusion = selected_shop_window_initial_parameters.window_parameters.outside_intrusion;
				shop_window_parameters.window_parameters.inside_intrusion = selected_shop_window_initial_parameters.window_parameters.inside_intrusion;
				shop_window_parameters.gap_between_windows = selected_shop_window_initial_parameters.gap_between_windows;
				shop_window_parameters.edge_gap = fmaxf(0, selected_shop_window_initial_parameters.edge_gap - shop_window_parameters.gap_between_windows);
				shop_window_parameters.window_parameters.imposts = selected_shop_window_initial_parameters.window_parameters.imposts;
				shop_window_parameters.window_parameters.impost_position = selected_shop_window_initial_parameters.window_parameters.impost_position;
				shop_window_parameters.window_parameters.impost_width = selected_shop_window_initial_parameters.window_parameters.impost_width;
				shop_window_parameters.window_parameters.impost_thickness = selected_shop_window_initial_parameters.window_parameters.impost_thickness;

				float space_for_shop_windows, shop_window_slot_size, remaining_size;
				calculate_simple_window_horizontal_sizes(wall_width, shop_window_parameters, space_for_shop_windows, shop_window_slot_size, number_of_shop_windows_horizontally, remaining_size);

				if (wall_width < selected_shop_window_initial_parameters.edge_gap * 2.05f)
				{
					number_of_shop_windows_horizontally = 0;
				}
				else if (number_of_shop_windows_horizontally <= 1)
				{
					shop_window_parameters.edge_gap += shop_window_parameters.gap_between_windows / 2.0f;
					space_for_shop_windows = wall_width - shop_window_parameters.edge_gap * 2.0f;
					shop_window_parameters.window_parameters.sizes.x = space_for_shop_windows;
					shop_window_parameters.gap_between_windows = 0.0f;
					number_of_shop_windows_horizontally = 1;
				}
				else
				{
					shop_window_parameters.window_parameters.sizes.x += remaining_size / number_of_shop_windows_horizontally;
				}

				//return number_of_shop_windows_horizontally > 0;
				return true;
			}
			else return false;
		}

		__forceinline__ __device__ bool try_simple_windows(const float wall_width, const float wall_height, unsigned int seed)
		{
			facade_slot_type = FacadeSlotType::SIMPLE_WINDOWS;

			int parameters_index = lcg(seed) % number_of_facade_slot_initial_parameters;
			SimpleWindowSlotParameters selected_window_initial_parameters = simple_window_initial_parameters[parameters_index];
			float selected_floor_height = simple_window_initial_floor_heights[parameters_index];
			facade_slots.simple_window_parameters.window_parameters.sizes = selected_window_initial_parameters.window_parameters.sizes;

			// Horizontally. ---------------------------------------------------------------------------------------------------------
			facade_slots.simple_window_parameters.gap_between_windows = selected_window_initial_parameters.gap_between_windows;
			facade_slots.simple_window_parameters.edge_gap = fmaxf(0, selected_window_initial_parameters.edge_gap - facade_slots.simple_window_parameters.gap_between_windows);
			facade_slots.simple_window_parameters.window_parameters.outside_intrusion = selected_window_initial_parameters.window_parameters.outside_intrusion;
			facade_slots.simple_window_parameters.window_parameters.inside_intrusion = selected_window_initial_parameters.window_parameters.inside_intrusion;
			facade_slots.simple_window_parameters.window_parameters.imposts = selected_window_initial_parameters.window_parameters.imposts;
			facade_slots.simple_window_parameters.window_parameters.impost_position = selected_window_initial_parameters.window_parameters.impost_position;
			facade_slots.simple_window_parameters.window_parameters.impost_width = selected_window_initial_parameters.window_parameters.impost_width;
			facade_slots.simple_window_parameters.window_parameters.impost_thickness = selected_window_initial_parameters.window_parameters.impost_thickness;

			float space_for_windows_horizontally, window_slot_size, remaining_size_horizontally;
			calculate_simple_window_horizontal_sizes(
				wall_width,
				facade_slots.simple_window_parameters,
				space_for_windows_horizontally,
				window_slot_size,
				number_of_slots_horizontally,
				remaining_size_horizontally
			);

			if (number_of_slots_horizontally < 1)
			{
				try_min_simple_window_widths(wall_width, space_for_windows_horizontally, window_slot_size, number_of_slots_horizontally, remaining_size_horizontally);

				if (number_of_slots_horizontally <= 1)
				{
					try_one_simple_window_horizontally(wall_width, space_for_windows_horizontally, window_slot_size, number_of_slots_horizontally, remaining_size_horizontally);

					if (number_of_slots_horizontally < 1)
					{
						number_of_slots_horizontally = 0;
					}
				}
			}
			else if (number_of_slots_horizontally == 1)
			{
				facade_slots.simple_window_parameters.gap_between_windows = 0;
				facade_slots.simple_window_parameters.edge_gap = (wall_width - facade_slots.simple_window_parameters.get_slot()) / 2.0f;
			}
			else
			{
				if (space_for_windows_horizontally < facade_slots.simple_window_parameters.window_parameters.sizes.x)
					number_of_slots_horizontally = 0;
				else
					facade_slots.simple_window_parameters.gap_between_windows
					= facade_slots.simple_window_parameters.gap_between_windows + (remaining_size_horizontally / number_of_slots_horizontally);
			}

			// Vertically. ---------------------------------------------------------------------------------------------------------
			floor_height = selected_floor_height;

			float space_for_windows_vertically, remaining_size_vertically;
			calculate_simple_window_vertical_sizes(
				wall_height,
				space_for_windows_vertically,
				floor_height,
				number_of_floors,
				remaining_size_vertically
			);

			if (space_for_windows_vertically < facade_slots.simple_window_parameters.window_parameters.sizes.y)
			{
				number_of_floors = 0;
				shop_at_first_floor = false;
			}
			else
			{
				if (shop_at_first_floor)
				{
					bottom_gap = 0.0f;
					shop_floor_height = floor_height + remaining_size_vertically;
					shop_window_parameters.window_parameters.sizes.y = fminf(shop_window_parameters.window_parameters.sizes.y, shop_floor_height);
				}
				else
				{
					bottom_gap += remaining_size_vertically;
					if (remaining_size_vertically > floor_height)
					{
						number_of_floors += 1;
						bottom_gap -= floor_height;
					}
				}
			}

			return number_of_slots_horizontally > 0 && number_of_floors > (shop_at_first_floor ? 1 : 0);
		}

		__forceinline__ __device__ bool try_window_2_balconies_window(const float wall_width, const float wall_height, unsigned int seed)
		{
			facade_slot_type = FacadeSlotType::WINDOW_BALCONY_BALCONY_WINDOW;

			int parameters_index = lcg(seed) % number_of_window_2_balconies_window_initial_parameters;
			Window2BalconiesWindowSlotParameters selected_initial_parameters
				= window_2_balconies_window_initial_parameters[parameters_index];
			float selected_floor_height = window_2_balconies_window_initial_floor_heights[parameters_index];
			facade_slots.window_2_balconies_window_parameters.window_parameters.sizes = selected_initial_parameters.window_parameters.sizes;
			facade_slots.window_2_balconies_window_parameters.window_parameters.outside_intrusion = selected_initial_parameters.window_parameters.outside_intrusion;
			facade_slots.window_2_balconies_window_parameters.window_parameters.inside_intrusion = selected_initial_parameters.window_parameters.inside_intrusion;
			facade_slots.window_2_balconies_window_parameters.window_parameters.imposts = selected_initial_parameters.window_parameters.imposts;
			facade_slots.window_2_balconies_window_parameters.window_parameters.impost_position = selected_initial_parameters.window_parameters.impost_position;
			facade_slots.window_2_balconies_window_parameters.window_parameters.impost_width = selected_initial_parameters.window_parameters.impost_width;
			facade_slots.window_2_balconies_window_parameters.window_parameters.impost_thickness = selected_initial_parameters.window_parameters.impost_thickness;
			facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_sizes = selected_initial_parameters.balcony_parameters.balcony_sizes;
			facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_wall_height = selected_initial_parameters.balcony_parameters.balcony_wall_height;
			facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_wall_thickness = selected_initial_parameters.balcony_parameters.balcony_wall_thickness;
			facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_intrusion = selected_initial_parameters.balcony_parameters.balcony_intrusion;

			// Horizontally. ---------------------------------------------------------------------------------------------------------
			facade_slots.window_2_balconies_window_parameters.gap_between_windows = selected_initial_parameters.gap_between_windows;
			facade_slots.window_2_balconies_window_parameters.gap_between_window_and_balcony = selected_initial_parameters.gap_between_window_and_balcony;
			facade_slots.window_2_balconies_window_parameters.gap_between_balconies = selected_initial_parameters.gap_between_balconies;
			facade_slots.window_2_balconies_window_parameters.edge_gap = fmaxf(0, selected_initial_parameters.edge_gap - facade_slots.window_2_balconies_window_parameters.gap_between_windows);

			float space_for_slots_horizontally, slot_size, remaining_size_horizontally;
			calculate_window_2_balconies_window_horizontal_sizes(
				wall_width,
				facade_slots.window_2_balconies_window_parameters,
				space_for_slots_horizontally,
				slot_size,
				number_of_slots_horizontally,
				remaining_size_horizontally
			);

			if (number_of_slots_horizontally < 1)
			{
				try_min_window_2_balconies_window_widths(
					wall_width,
					space_for_slots_horizontally,
					slot_size,
					number_of_slots_horizontally,
					remaining_size_horizontally
				);

				if (number_of_slots_horizontally <= 1)
				{
					try_one_window_2_balconies_window_horizontally_with_min_widths(
						wall_width,
						space_for_slots_horizontally,
						slot_size,
						number_of_slots_horizontally,
						remaining_size_horizontally
					);
				}
			}
			else if (number_of_slots_horizontally == 1)
			{
				if ((wall_width - facade_slots.window_2_balconies_window_parameters.get_slot()) / 2.0f >= regular_building_edge_gap_min)
				{
					facade_slots.window_2_balconies_window_parameters.gap_between_windows = 0;
					facade_slots.window_2_balconies_window_parameters.edge_gap = (wall_width - facade_slots.window_2_balconies_window_parameters.get_slot()) / 2.0f;

					float excent_edge_gap = facade_slots.window_2_balconies_window_parameters.edge_gap - regular_building_edge_gap_max;

					if (excent_edge_gap > 0.f)
					{
						/*facade_slots.window_2_balconies_window_parameters.gap_between_balconies += excent_edge_gap / 3.0f;
						facade_slots.window_2_balconies_window_parameters.gap_between_window_and_balcony += (excent_edge_gap / 3.0f) / 2.0f;
						facade_slots.window_2_balconies_window_parameters.edge_gap -= excent_edge_gap;*/
						float excent_size = excent_edge_gap * 2;

						facade_slots.window_2_balconies_window_parameters.gap_between_balconies += excent_size / 2.f;
						facade_slots.window_2_balconies_window_parameters.gap_between_window_and_balcony += excent_size / 4.f;
						facade_slots.window_2_balconies_window_parameters.edge_gap -= excent_edge_gap;
					}
					else if (excent_edge_gap < 0.f)
					{
						try_one_window_2_balconies_window_horizontally_with_min_widths(
							wall_width,
							space_for_slots_horizontally,
							slot_size,
							number_of_slots_horizontally,
							remaining_size_horizontally
						);
					}
				}
				else number_of_slots_horizontally = 0;
			}
			else facade_slots.window_2_balconies_window_parameters.gap_between_windows += remaining_size_horizontally / number_of_slots_horizontally;

			// Vertically. ---------------------------------------------------------------------------------------------------------
			floor_height = selected_floor_height;

			float space_for_slots_vertically, remaining_size_vertically;
			calculate_window_2_balconies_window_vertical_sizes(
				wall_height,
				space_for_slots_vertically,
				floor_height,
				number_of_floors,
				remaining_size_vertically
			);

			if (space_for_slots_vertically < facade_slots.window_2_balconies_window_parameters.get_max_vertical_size())
			{
				number_of_floors = 0;
				shop_at_first_floor = false;
			}
			else
			{
				if (shop_at_first_floor)
				{
					bottom_gap = 0.0f;
					shop_floor_height = floor_height + remaining_size_vertically;
					shop_window_parameters.window_parameters.sizes.y = fminf(shop_window_parameters.window_parameters.sizes.y, shop_floor_height);
				}
				else
				{
					bottom_gap += remaining_size_vertically;
					if (remaining_size_vertically > floor_height)
					{
						number_of_floors += 1;
						bottom_gap -= floor_height;
					}
				}
			}

			return number_of_slots_horizontally > 0 && number_of_floors > (shop_at_first_floor ? 1 : 0);
		}

	public:
		bool calculated;

		float max_intrusion;
		float max_extrusion;

		int number_of_floors;
		float floor_height;

		float top_gap;
		float bottom_gap;

		FacadeSlotType facade_slot_type;
		int number_of_slots_horizontally;
		union
		{
			SimpleWindowSlotParameters simple_window_parameters;
			Window2BalconiesWindowSlotParameters window_2_balconies_window_parameters;
		} facade_slots;

		bool shop_at_first_floor;
		float shop_floor_height;
		int number_of_shop_windows_horizontally;
		SimpleWindowSlotParameters shop_window_parameters;

		RegularBuildingFacadeParameters()
		{

		}

		__forceinline__ __device__ void calculate_window_parameters(const float wall_width, const float wall_height, unsigned int seed)
		{
			calculated = true;

			number_of_shop_windows_horizontally = 0;
			number_of_slots_horizontally = 0;

			top_gap = (regular_building_top_gap_max - regular_building_top_gap_min) * rnd(seed) + regular_building_top_gap_min;
			bottom_gap = (regular_building_bottom_gap_max - regular_building_bottom_gap_min) * rnd(seed) + regular_building_bottom_gap_min;

			// Shop windows (must be calculated before).
			{
				try_shop_windows(wall_width, wall_height, seed);
			}

			if (shop_at_first_floor)
			{
				bottom_gap = 0;
			}
			else
			{
				if (bottom_gap > regular_building_bottom_gap_max)
				{
					float excent_bottom_gap = bottom_gap - regular_building_bottom_gap_max;
					floor_height += excent_bottom_gap / number_of_floors;
				}
			}

			// Windows.
			{
				if (!try_window_2_balconies_window(wall_width, wall_height, seed))
				{
					try_simple_windows(wall_width, wall_height, seed);
				}
			}

			if (shop_at_first_floor)
			{
				number_of_floors -= 1;
			}

			max_extrusion = 0;
			max_intrusion = 0;

			if (shop_at_first_floor)
			{
				max_intrusion = fmaxf(
					shop_window_parameters.window_parameters.outside_intrusion + shop_window_parameters.window_parameters.inside_intrusion,
					max_intrusion
				);
			}

			if (number_of_floors > 0 && number_of_slots_horizontally > 0)
			{
				switch (facade_slot_type)
				{
					case FacadeSlotType::SIMPLE_WINDOWS:
					{
						max_intrusion = fmaxf(
							facade_slots.simple_window_parameters.window_parameters.outside_intrusion
							+ facade_slots.simple_window_parameters.window_parameters.inside_intrusion,
							max_intrusion
						);
						//if (facade_slots.simple_window_parameters.window_parameters.imposts)
						//	printf("%f\n", facade_slots.simple_window_parameters.window_parameters.impost_width);
					} break;

					case FacadeSlotType::WINDOW_BALCONY_BALCONY_WINDOW:
					{
						max_intrusion = fmaxf(
							facade_slots.window_2_balconies_window_parameters.window_parameters.outside_intrusion
							+ facade_slots.window_2_balconies_window_parameters.window_parameters.inside_intrusion,
							max_intrusion
						);
						max_intrusion = fmaxf(facade_slots.window_2_balconies_window_parameters.balcony_parameters.balcony_intrusion, max_intrusion);
					} break;
				}
			}
		}
	};
}