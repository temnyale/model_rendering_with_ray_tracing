/**
 * Data structure to create textures.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#include "TextureCreator.h"

#include <helpers.h>
#include <random>

void drpm::TextureCreator::create_black_texture(std::vector<float4>& buffer, unsigned int width, unsigned int height)
{
    buffer.resize(width * height);

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            buffer[y * width + x].x = 0.0f;
            buffer[y * width + x].y = 0.0f;
            buffer[y * width + x].z = 0.0f;
            buffer[y * width + x].w = 0.0f;
        }
    }
}

void drpm::TextureCreator::create_white_texture(std::vector<float4>& buffer, unsigned int width, unsigned int height)
{
    buffer.resize(width * height);

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            buffer[y * width + x].x = 1.0f;
            buffer[y * width + x].y = 1.0f;
            buffer[y * width + x].z = 1.0f;
            buffer[y * width + x].w = 0.0f;
        }
    }
}

void drpm::TextureCreator::create_pseudo_night_sky_texture(std::vector<float4>& buffer, unsigned int width, unsigned int height, float star_density, int seed)
{
    buffer.resize(width * height);

    std::mt19937 gen(seed);
    std::uniform_real_distribution<float> dis(0.0f, 1.0f);

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            float4 color;

            if (dis(gen) < star_density)
            {
                color = make_float4(1.0f, 1.0f, 1.0f, 0.0f);
            }
            else
            {
                color = make_float4(0.00007450980392156862745098039215686f, 0.00009411764705882352941176470588235f, 0.00038431372549019607843137254901961f, 0.0f);
            }

            buffer[y * width + x] = color;
        }
    }
}

void drpm::TextureCreator::create_black_white_texture(std::vector<float4>& buffer, unsigned int width, unsigned int height, float white_density, int seed)
{
	buffer.resize(width * height);

	std::mt19937 gen(seed);
	std::uniform_real_distribution<float> dis(0.0f, 1.0f);

	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			float4 rgba_color;

			if (dis(gen) < white_density)
			{
				rgba_color = make_float4(1.0f, 1.0f, 1.0f, 0.0f);
			}
			else
			{
				rgba_color = make_float4(0, 0, 0, 0.0f);
			}

			buffer[y * width + x] = rgba_color;
		}
	}
}

void drpm::TextureCreator::float4_to_float(std::vector<float>& new_texture_buffer, std::vector<float4> texture_buffer, unsigned int width, unsigned int height)
{
    new_texture_buffer.resize(width * 4 * height);

    for (int y = 0; y < height; y++)
    {
        for (int x = 0, _x = 0; x < width * 4; x += 4, _x++)
        {
            new_texture_buffer[y * width * 4 + x + 0] = texture_buffer[y * width + _x].x;
            new_texture_buffer[y * width * 4 + x + 1] = texture_buffer[y * width + _x].y;
            new_texture_buffer[y * width * 4 + x + 2] = texture_buffer[y * width + _x].z;
            new_texture_buffer[y * width * 4 + x + 3] = texture_buffer[y * width + _x].w;
        }
    }
}
