#pragma once

#include <iostream>
#include <chrono>

namespace drpm
{
	class TimeCounter
	{
	private:
		int iteration;
		std::chrono::duration<double> total_time;

	public:
		TimeCounter()
		{

		}

		void start()
		{
			total_time = std::chrono::milliseconds::zero();
			iteration = 0;
		}

		void iterate(std::chrono::duration<double>& passed_time)
		{
			total_time =+ passed_time;
			iteration++;
		}

		void print()
		{
			if (iteration == 0)
				throw "No iteration was done.";

			std::chrono::duration<double> average_time = total_time / static_cast<double>(iteration);

			std::cout << "Average time: " << average_time.count() << "\n";
		}
	};
}