/**
 * Data structure that simplifies the process of image rendering.
 *
 * Author: Alexander Temnyakov.
 * Contact: temnyale@fel.cvut.cz, alex.temnyakoff@yandex.ru.
 *
 */

#include "image_handling.h"

namespace drpm
{
	void save_image(sutil::CUDAOutputBuffer<uchar4>& output_buffer)
	{
		auto t = std::time(nullptr);
		auto tm = *std::localtime(&t);

		std::ostringstream oss;
		oss << std::put_time(&tm, "%d-%m-%Y %H-%M-%S.png");
		std::string filename = oss.str();
		sutil::ImageBuffer buffer;
		buffer.data = output_buffer.getHostPointer();
		buffer.width = output_buffer.width();
		buffer.height = output_buffer.height();
		buffer.pixel_format = sutil::BufferImageFormat::UNSIGNED_BYTE4;
		sutil::saveImage(filename.c_str(), buffer, false);
	}
}