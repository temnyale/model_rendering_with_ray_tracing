# Direct Rendering of Procedural Models (DRPM)

Proceed to the wiki: https://gitlab.fel.cvut.cz/temnyale/model_rendering_with_ray_tracing/-/wikis/home.

![18](https://gitlab.fel.cvut.cz/temnyale/model_rendering_with_ray_tracing/-/wikis/uploads/8d00415fab01a40ba76c862d589f8e56/18.png)

![19](https://gitlab.fel.cvut.cz/temnyale/model_rendering_with_ray_tracing/-/wikis/uploads/8c650c911571c6138b9b0afac0d6ac3c/19.png)

![20](https://gitlab.fel.cvut.cz/temnyale/model_rendering_with_ray_tracing/-/wikis/uploads/14ebc8b033c977578e9d9bf15c7c2844/20.png)